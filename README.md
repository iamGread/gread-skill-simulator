# Gread Skill Simulator #
## ##
## * 현재 이 프로젝트는 기여자가 입대함으로 인해 중지중입니다. * ##

라이센스는 FreeBSD 를 사용합니다. iamGread라는 원 저작자에게 감사만 표하신다면 뭐에 어따 써먹든 신경쓰지 않습니다.
 
example 폴더에 스킬 예제가 있습니다.

 
## 실험영상 ##

** [스킬 실험영상 - 구버전](https://www.youtube.com/watch?v=_Pahj5iYqgo) **

** [스킬 실험영상 1](https://www.youtube.com/watch?v=_Pahj5iYqgo) **
 
** [스킬 실험영상 2](https://www.youtube.com/watch?v=5SVAwi7q-Y4) **
 
** [스킬 실험영상 3](https://www.youtube.com/watch?v=dRnadR9rnW0) **
 
** [스킬 실험영상 4](https://www.youtube.com/watch?v=zrxJjoLZOCA) **
 
** [스킬 제작영상](https://www.youtube.com/watch?v=RsUrTYxGu5Q) **
 
구버전 영상은 내부적으로는 큰 차이가 있지만 현재 버전하고 겉보기적 측면에서는 차이가 없습니다.

유용하게 쓰셨다면 ksh9345@gmail.com 으로 감사메일정도만 보내주세요 감사합니다. ㅎㅎ 

 
## 다운로드 ##
** [Gread SKill Simulator](https://www.dropbox.com/sh/xa9qol65fo5pmwg/AABIoW8gwZ57hPlGrQQYuZk2a?dl=0) **
 
** [GSS Builder](https://www.dropbox.com/sh/n7wqu6d99evd92n/AABCf23cK_yIGaqdnJf4qUnTa?dl=0) **
 
 
 
*Author By iamGread*