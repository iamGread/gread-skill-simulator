﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using LibGit2Sharp;

namespace GSS_Builder
{
    public partial class Form1 : Form
    {
        string path = "git";
        public Form1()
        {
            InitializeComponent();
        }
        public static void RecursiveDelete(DirectoryInfo baseDir)
        {
            if (!baseDir.Exists)
                return;

            foreach (var dir in baseDir.EnumerateDirectories())
            {
                RecursiveDelete(dir);
            }
            baseDir.Delete(true);
        }

        private void setText(string text)
        {
            if (this.label2.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(()=> {
                    this.label2.Text = text;
                }));
            }
            else
            {
                this.label2.Text = text;
            }
        }

        private void button1_Click_run()
        {

            try
            {
                string loc = "";
                this.Invoke(new MethodInvoker(()=>loc = textBox1.Text));
                var copt = new CloneOptions();
                copt.OnTransferProgress = (TransferProgress progress) =>
                {                    
                    setText(String.Format("{0}/{1}", progress.IndexedObjects, progress.TotalObjects));
                    this.Invoke(new MethodInvoker(() => progressBar1.Value = (int)(((double)progress.IndexedObjects) / ((double)progress.TotalObjects) * 100)));
                    return true;
                };
                setText("Cloning start : " + textBox1.Text);
                string repopath;
                if (!Directory.Exists(path))
                {
                    repopath = Repository.Clone(
                        loc,
                        path,
                        copt);
                }
                else
                {
                    repopath = path;
                }
                using (var rep = new Repository(repopath))
                {
                    Commit com = rep.Commits.First<Commit>();
                    ProcStat.dtoffset = com.Committer.When;
                    setText(String.Format("Author : {0} Last Commit {1}", com.Author, com.Committer.When.ToString("yyyy-MM-dd")));
                    //
                    var dirloc = @"src/gmail/ksh9345/gss/core/frames/";
                    GSSFrame.frames.Clear();
                    foreach (var ft in GSSFrameType.All())
                    {
                        foreach (TreeEntry treeentry in com.Tree[dirloc + ft.name.ToLower()].Target as Tree)
                        {
                            if (treeentry.TargetType == TreeEntryTargetType.Blob)
                            {
                                string classname = treeentry.Name.Split('.')[0];
                                Console.WriteLine(classname);
                                //
                                Blob b = treeentry.Target as Blob;
                                string text = b.GetContentText();
                                int start = text.IndexOf("{");
                                int end = text.LastIndexOf("}");
                                text = text.Substring(start, end - start + 1);
                                text = text.Trim();
                                // frameenum
                                var mas = Regex.Matches(text, @"\s+public\s+enum\s+[a-zA-Z0-9_]+\s*{[\s\n\w,;]+");
                                List<GSSFrameEnum> fes = new List<GSSFrameEnum>();
                                foreach (Match ma in mas)
                                {
                                    string[] splited = ma.Value.Split('{');
                                    var tempsplited = Regex.Split(splited[0], @"[\n\s]+");
                                    fes.Add(new GSSFrameEnum(
                                        tempsplited[3],
                                        splited[1].Split(',').Select(each => each.Trim()).Where(each => each.Length > 0).ToArray()));
                                }
                                // constructor
                                var cstrstr = Regex.Matches(text, @"\s+public\s+" + classname + @"\s*\([\s\n\w,\[\]]+");
                                List<GSSFrameConstructor> fcs = new List<GSSFrameConstructor>();
                                foreach (Match cstr in cstrstr)
                                {
                                    string[] splited = cstr.Value.Split('(');
                                    var tempsplited = Regex.Split(splited[0], @"\s+");
                                    List<GSSSupportType> cstr_types = new List<GSSSupportType>();
                                    List<string> cstr_names = new List<string>();
                                    var v = Regex.Split(splited[1], @"\s*,\s*").Select(each => each.Trim()).ToArray();
                                    
                                    foreach (var val in v)
                                    {
                                        var tempvalsplit = Regex.Split(val, @"\s+");
                                        var tp = tempvalsplit[0].Trim();
                                        var name = tempvalsplit[1].Trim();
                                        var stp = GSSSupportType.getByName(tp);
                                        if (stp == null)
                                        {
                                            var stp_fe = fes.Find(temp => temp.enumname.Equals(tp));
                                            if (stp_fe == null)
                                            {
                                                stp = GSSSupportType.String;
                                            }
                                            else
                                            {
                                                stp = new GSSSupportType(stp_fe.enumname, stp_fe.values);
                                            }
                                        }
                                        cstr_types.Add(stp);
                                        cstr_names.Add(name);
                                    }
                                    fcs.Add(new GSSFrameConstructor(cstr_types.ToArray(), cstr_names.ToArray()));
                                }
                                if (fcs.Count == 0)
                                {
                                    fcs.Add(new GSSFrameConstructor(new GSSSupportType[] { }, new string[] { }));
                                }
                                GSSFrame.frames.Add(new GSSFrame(classname, ft, fcs.ToArray(), fes.ToArray()));
                            }
                        }
                    }




                    Thread.Sleep(300);

                    this.Invoke(new MethodInvoker(() =>
                    {
                        next();
                    }));
                }
            }
            catch (RepositoryNotFoundException ex)
            {
                setText("Repository not found exception");
                this.Invoke(new MethodInvoker(() => {
                    button1.Enabled = true;
                }));
            }
            catch (Exception ex2)
            {
                setText("Exception");
                this.Invoke(new MethodInvoker(() => {
                    button1.Enabled = true;
                }));
                MessageBox.Show(ex2.ToString());
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            Thread thd = new Thread(new ThreadStart(button1_Click_run));
            thd.Start();
        }
        private void next() {
            this.Close();
            ProcStat.loadsuc = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {

            Process delproc = new Process();
            delproc.StartInfo.FileName = "cmd.exe";
            delproc.StartInfo.CreateNoWindow = false;
            delproc.StartInfo.UseShellExecute = false;

            delproc.StartInfo.Arguments = string.Format("/C rmdir /s /q \"{0}\"", path);
            delproc.Start();
            

        }
    }
}
