﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace GSS_Builder
{
    public static class ProcStat
    {
        public static bool loadsuc = false;
        public static DateTimeOffset dtoffset;
        public static string Version = "0.0.2";
        private static string BaseDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..");
        public static string getBasedir()
        {
            if(Directory.Exists(Path.Combine(BaseDir, "gss")))
            {
                return Path.GetFullPath(Path.Combine(BaseDir, "gss"));
            }
            return Path.GetFullPath(BaseDir);
        }
    }
    static class Program
    {
        
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            if (ProcStat.loadsuc)
            {
                Application.Run(new Editor());
            }
        }
    }
}
