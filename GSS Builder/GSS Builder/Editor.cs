﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
namespace GSS_Builder
{
    public partial class Editor : Form
    {
        Guid tempname = Guid.NewGuid();
        Dictionary<GSSSkillOption, string> opt = new Dictionary<GSSSkillOption, string>();
        //
        GSSFrame selected;
        string[] selectedArgs;
        //
        public Editor()
        {
            InitializeComponent();
        }
        void SelectFrame()
        {
            this.selected = null;
            this.selectedArgs = null;
            this.argKey.Items.Clear();
            this.argKey.SelectedItem = null;
            this.argValue.Items.Clear();
            this.argValue.SelectedItem = null;
            this.argValue.Text = null;
            this.frameLabel.Text = null;
            //
            constructors.Items.Clear();
            constructors.Text = null;

            if (selected == null)
            {
                frameName.Text = "None";
                return;
            }
            frameName.Text = selected.framename;
            constructors.Items.AddRange(selected.constructors);
            constructors.SelectedIndex = 0;
        }
        void SelectFrame(GSSFrame gssf)
        {
            this.selected = gssf;
            this.selectedArgs = null;
            this.argKey.Items.Clear();
            this.argKey.SelectedItem = null;
            this.argValue.Items.Clear();
            this.argValue.SelectedItem = null;
            this.argValue.Text = null;
            this.frameLabel.Text = null;
            //
            constructors.Items.Clear();
            constructors.Text = null;
            
            if (selected == null)
            {
                frameName.Text = "None";
                return;
            }
            frameName.Text = selected.framename;
            constructors.Items.AddRange(selected.constructors);
            constructors.SelectedIndex = 0;
        }
        void SelectFrame(GSSFrameInstance gssfi) {
            if (gssfi == null)
            {
                return;
            }
            SelectFrame(gssfi.frameType);
            //
            this.constructors.SelectedItem = gssfi.constructor;
            selectedArgs = new string[gssfi.arguaments.Length];
            gssfi.arguaments.CopyTo(selectedArgs, 0);
            if (argKey.Items.Count > 0)
            {
                argKey.SelectedIndex = 0;
                argKey_SelectedIndexChanged(null, null);
            }
        }
        //
        private void options_SelectedIndexChanged(object sender, EventArgs e)
        {
            GSSSkillOption gsssopt = options.SelectedItem as GSSSkillOption;
            optionValue.Items.Clear();
            if (gsssopt.type.kind.Length > 0)
            {
                optionValue.Items.Add("");
                optionValue.Items.AddRange(gsssopt.type.kind);
                optionValue.DropDownStyle = ComboBoxStyle.DropDownList;
            }
            else
            {
                
                optionValue.DropDownStyle = ComboBoxStyle.DropDown;
            }
            try
            {
                var temp = opt[gsssopt];
                optionValue.Text = temp;
            }
            catch (Exception)
            {
                optionValue.Text = "";
            }
        }
        private void optionValue_Leave(object sender, EventArgs e)
        {
            GSSSkillOption gsssopt = options.SelectedItem as GSSSkillOption;
            if (optionValue.Text.Length == 0)
            {
                opt.Remove(gsssopt);
            }
            else if (!gsssopt.type.valid(optionValue.Text))
            {

                if (gsssopt.type.kind.Length > 0)
                {
                    optionValue.Text = optionValue.Items[0].ToString();
                }
                else
                {
                    optionValue.Text = "";
                }
                MessageBox.Show("This is not valid type value for " + gsssopt);
                optionValue.Focus();
            }
            else
            {
                opt[gsssopt] = optionValue.Text;
            }
        }
        //
        private void actList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(actList.SelectedIndex == -1) return;
            SelectFrame(actList.SelectedItem as GSSFrame);
            actList.SelectedIndex = -1;
        }
        private void controlList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (controlList.SelectedIndex == -1) return;
            SelectFrame(controlList.SelectedItem as GSSFrame);
            controlList.SelectedIndex = -1;
        }
        private void etcList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (etcList.SelectedIndex == -1) return;
            SelectFrame(etcList.SelectedItem as GSSFrame);
            etcList.SelectedIndex = -1;
        }
        private void findList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (findList.SelectedIndex == -1) return;
            SelectFrame(findList.SelectedItem as GSSFrame);
            findList.SelectedIndex = -1;
        }
        private void listenList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listenList.SelectedIndex == -1) return;
            SelectFrame(listenList.SelectedItem as GSSFrame);
            listenList.SelectedIndex = -1;
        }
        private void manageList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (manageList.SelectedIndex == -1) return;
            SelectFrame(manageList.SelectedItem as GSSFrame);
            manageList.SelectedIndex = -1;
        }
        private void movementList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (movementList.SelectedIndex == -1) return;
            SelectFrame(movementList.SelectedItem as GSSFrame);
            movementList.SelectedIndex = -1;
        }
        private void selectList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (selectList.SelectedIndex == -1) return;
            SelectFrame(selectList.SelectedItem as GSSFrame);
            selectList.SelectedIndex = -1;
        }
        private void showList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (showList.SelectedIndex == -1) return;
            SelectFrame(showList.SelectedItem as GSSFrame);
            showList.SelectedIndex = -1;
        }
        private void sortList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sortList.SelectedIndex == -1) return;
            SelectFrame(sortList.SelectedItem as GSSFrame);
            sortList.SelectedIndex = -1;
        }
        //
        private void constructors_SelectedIndexChanged(object sender, EventArgs e)
        {
            GSSFrameConstructor cstr = constructors.SelectedItem as GSSFrameConstructor;
            selectedArgs = new string[cstr.argstype.Length];
            argKey.Items.Clear();
            argKey.Text = null;
            argKey.Items.AddRange(cstr.argstype);
            if(argKey.Items.Count > 0)
            {
                argKey.SelectedIndex = 0;
                argKey_SelectedIndexChanged(null, null);
            }
            
        }
        
        private void argKey_SelectedIndexChanged(object sender, EventArgs e)
        {
            GSSFrameConstructor cstr = constructors.SelectedItem as GSSFrameConstructor;
            //
            argValue.Items.Clear();
            argValue.Text = null;
            unsetAVTU();
            //
            GSSSupportType st = argKey.SelectedItem as GSSSupportType;
            if(st.kind.Length > 0)
            {
                argValue.DropDownStyle = ComboBoxStyle.DropDownList;
                argValue.Items.AddRange(st.kind);
            }
            else
            {
                if(st == GSSSupportType.Vector3d)
                {
                    argValue.DropDownStyle = ComboBoxStyle.DropDown;
                    setAVTU();
                    if (selectedArgs[argKey.SelectedIndex] == null)
                    {
                        selectedArgs[argKey.SelectedIndex] = "[0, 0, 0]";
                        argValue.Text = "[0, 0, 0]";
                        prevArgValue = "[0, 0, 0]";
                    }
                }
                else {
                    argValue.DropDownStyle = ComboBoxStyle.DropDown;
                }
            }
            Keyname.Text = cstr.argsname[argKey.SelectedIndex];




            if (selectedArgs[argKey.SelectedIndex] != null)
            {
                argValue.Text = selectedArgs[argKey.SelectedIndex];
            }
        }

        private void argValue_Leave(object sender, EventArgs e)
        {
            GSSSupportType st = argKey.SelectedItem as GSSSupportType;
            try
            {
                if (st == GSSSupportType.Integer)
                {
                    if (argValue.Text.StartsWith("#"))
                    {
                        selectedArgs[argKey.SelectedIndex] = argValue.Text;
                        return;
                    }
                }
                if (st.valid(argValue.Text))
                {
                    selectedArgs[argKey.SelectedIndex] = argValue.Text;
                }
                else
                {
                    MessageBox.Show("Invalid value");
                    argValue.Text = null;
                }
            }
            catch (Exception)
            {

                return;
            }
        }
        private void setAVTU()
        {
            argValue.TextUpdate += new EventHandler(argValue_TextUpdate);
            prevArgcursor = 0;
        }
        private void unsetAVTU()
        {
            argValue.TextUpdate -= new EventHandler(argValue_TextUpdate);
        }
        private string prevArgValue = null;
        private int prevArgcursor = 0;
        private void argValue_TextUpdate(object sender, EventArgs e)
        {
            GSSSupportType st = argKey.SelectedItem as GSSSupportType;
            
            if (Regex.IsMatch(argValue.Text, @"^\s*\[\s*,\s*[-+]?([0-9]*\.[0-9]+|[0-9]+|[0-9]+\.)\s*,\s*[-+]?([0-9]*\.[0-9]+|[0-9]+|[0-9]+\.)\s*\]\s*$"))
            {
                
                prevArgcursor = argValue.SelectionStart + 1;
                argValue.Text = argValue.Text.Insert(argValue.SelectionStart, "0");
                argValue.SelectionStart = prevArgcursor;
            }
            else if (Regex.IsMatch(argValue.Text, @"^\s*\[\s*[-+]?([0-9]*\.[0-9]+|[0-9]+|[0-9]+\.)\s*,\s*,\s*[-+]?([0-9]*\.[0-9]+|[0-9]+|[0-9]+\.)\s*\]\s*$"))
            {
                
                prevArgcursor = argValue.SelectionStart + 1;
                argValue.Text = argValue.Text.Insert(argValue.SelectionStart, "0");
                argValue.SelectionStart = prevArgcursor;
            }
            else if (Regex.IsMatch(argValue.Text, @"^\s*\[\s*[-+]?([0-9]*\.[0-9]+|[0-9]+|[0-9]+\.)\s*,\s*[-+]?([0-9]*\.[0-9]+|[0-9]+|[0-9]+\.)\s*\]\s*,\s*$"))
            {
                
                prevArgcursor = argValue.SelectionStart + 1;
                argValue.Text = argValue.Text.Insert(argValue.SelectionStart, "0");
                argValue.SelectionStart = prevArgcursor;
            }
            else
            {
                if (!st.valid(argValue.Text))
                {
                    argValue.Text = prevArgValue;
                    argValue.SelectionStart = prevArgcursor;
                }
                else
                {
                    prevArgValue = argValue.Text;
                    prevArgcursor = argValue.SelectionStart;
                }
            }
        }

        

        private void Clean_Click(object sender, EventArgs e)
        {
            SelectFrame();
        }
        private void frameinstances_DoubleClick(object sender, EventArgs e)
        {
            GSSFrameInstance gssfi = frameinstances.SelectedItem as GSSFrameInstance;
            SelectFrame(gssfi);
        }

        private bool existLabel(string label)
        {
            if(label == null)
            {
                return false;
            }
            foreach(GSSFrameInstance t in frameinstances.Items)
            {
                if (t.label != null && t.label.Equals(label))
                {
                    return true;
                }
            }
            return false;
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if (existLabel(frameLabel.Text))
            {
                frameLabel.Focus();
                toolTip1.Show("Exist Label", frameLabel, new Point(4, 24), 1000);
                return;
            }
            var tempcstr = constructors.SelectedItem as GSSFrameConstructor;
            var tempargs = new string[selectedArgs.Length];
            selectedArgs.CopyTo(tempargs, 0);
            if (!tempcstr.validArgs(selectedArgs))
            {
                MessageBox.Show("Invalid Constructor Arguments");
                return;
            }

            if (frameLabel.Text.Length > 0)
            {
                frameinstances.Items.Insert(
                    frameinstances.SelectedIndex + 1, new GSSFrameInstance(selected, frameLabel.Text, tempcstr, tempargs));
            }
            else
            {
                frameinstances.Items.Insert(
                    frameinstances.SelectedIndex + 1, new GSSFrameInstance(selected, tempcstr, tempargs));
            }
            frameinstances.SelectedIndex += 1;
        }
        private void Replace_Click(object sender, EventArgs e)
        {
            var tempcstr = constructors.SelectedItem as GSSFrameConstructor;
            var tempargs = new string[selectedArgs.Length];
            selectedArgs.CopyTo(tempargs, 0);
            if (!tempcstr.validArgs(selectedArgs))
            {
                MessageBox.Show("Invalid Constructor Arguments");
                return;
            }
            //
            var tempmem = frameinstances.SelectedIndex;
            frameinstances.Items.RemoveAt(frameinstances.SelectedIndex);
            if(tempmem >= frameinstances.Items.Count)
            {
                if (frameLabel.Text.Length > 0)
                {
                    frameinstances.Items.Add(
                        new GSSFrameInstance(selected, frameLabel.Text, tempcstr, tempargs));
                }
                else
                {
                    frameinstances.Items.Add(
                        new GSSFrameInstance(selected, tempcstr, tempargs));
                }
                frameinstances.SelectedIndex = frameinstances.Items.Count - 1;
            }
            else
            {
                if (frameLabel.Text.Length > 0)
                {
                    frameinstances.Items.Insert(
                        tempmem, new GSSFrameInstance(selected, frameLabel.Text, tempcstr, tempargs));
                }
                else
                {
                    frameinstances.Items.Insert(
                        tempmem, new GSSFrameInstance(selected, tempcstr, tempargs));
                }
                frameinstances.SelectedIndex = tempmem;
            }
            //
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frameinstances.Items.Clear();
        }
        
        private void Save_Click(object sender, EventArgs e)
        {
            if( !(skillName.Text != null && skillName.Text.Length > 0))
            {
                skillName.Focus();
                toolTip1.Show("Name required", skillName, new Point(4, 24), 1000);
                return;
            }
            string skname = skillName.Text;
            string description = skillDescrption.Text;
            Dictionary<string, object> tempopt = new Dictionary<string, object>();
            foreach(var v in opt){
                tempopt[v.Key.name] = v.Key.type.toObject(v.Value);
            }
            GSSFrameInstance[] frs = new GSSFrameInstance[frameinstances.Items.Count];
            for (int i = 0; i < frameinstances.Items.Count; i++)
            {
                frs[i] = frameinstances.Items[i] as GSSFrameInstance;
            }

            GSSSkill comp = new GSSSkill(skname, description, tempopt, frs);
            var temp = JsonConvert.SerializeObject(comp, Formatting.Indented);


            var memtemp = new MemoryStream(Encoding.UTF8.GetBytes(
                @"//////////////////////////////////////////////////////////////////////////////////" + "\n" +
                @"//   _____    _____    _____      ____            _   _       _                 //" + "\n" +
                @"//  / ____|  / ____|  / ____|    |  _ \          (_) | |     | |                //" + "\n" +
                @"// | |  __  | (___   | (___      | |_) |  _   _   _  | |   __| |   ___   _ __   //" + "\n" +
                @"// | | |_ |  \___ \   \___ \     |  _ <  | | | | | | | |  / _` |  / _ \ | '__|  //" + "\n" +
                @"// | |__| |  ____) |  ____) |    | |_) | | |_| | | | | | | (_| | |  __/ | |     //" + "\n" +
                @"//  \_____| |_____/  |_____/     |____/   \__,_| |_| |_|  \__,_|  \___| |_|     //" + "\n" +
                @"//                                                                              //" + "\n" +
                @"//////////////////////////////////////////////////////////////////////////////////" + "\n" +
                @"// GSS Builder Compiled v" + ProcStat.Version + "\n" +
                @"// " + "\n" +
                @"// Compiled Date : " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\n" +
                @"// GSS Frame Commit Date : " + ProcStat.dtoffset.ToString("yyyy-MM-dd") + "\n" +
                @"// " + "\n" +
                @"// Author By iamGread" + "\n" +
                @"// " + "\n" +
                temp));
            var memtemp2 = new MemoryStream();
            new BinaryFormatter().Serialize(memtemp2, comp);
            memtemp2.Seek(0, SeekOrigin.Begin);
            //

            saveFileDialog1.InitialDirectory = ProcStat.getBasedir();
            saveFileDialog1.FileName = skillName.Text;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                
                Stream jsonStream = null;
                try
                {
                    if ((jsonStream = saveFileDialog1.OpenFile()) != null)
                    {
                        memtemp.CopyTo(jsonStream);
                        // Code to write the stream goes here.
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.ToString());
                    return;
                }
                finally
                {
                    if(jsonStream != null)
                    {
                        jsonStream.Close();
                    }
                }
                Stream gsbStream = null;
                try
                {
                    string temppath = Path.ChangeExtension(saveFileDialog1.FileName, "gsb");
                    if ((gsbStream = new FileStream(temppath, FileMode.OpenOrCreate)) != null)
                    {
                        memtemp2.CopyTo(gsbStream);
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.ToString());
                    return;
                }
                finally
                {
                    if (gsbStream != null)
                    {
                        gsbStream.Close();
                    }
                }
                MessageBox.Show("Save Complete");
            }
        }
        private void ForceClose()
        {
            forceclose = true;
            this.Close();
        }
        bool forceclose = false;
        private void Editor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!forceclose)
            {
                if( MessageBox.Show("Are you want to close?", "GSS Builder", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        private void Remove_Click(object sender, EventArgs e)
        {
            if(frameinstances.SelectedIndex > -1)
            {
                var temp = frameinstances.SelectedIndex;
                frameinstances.Items.RemoveAt(frameinstances.SelectedIndex);
                if(temp < frameinstances.Items.Count)
                {
                    frameinstances.SelectedIndex = temp;
                }
                else
                {
                    frameinstances.SelectedIndex = frameinstances.Items.Count - 1;
                }
            }
        }

        private void frameinstances_SelectedIndexChanged(object sender, EventArgs e)
        {
            GSSFrameInstance fi = frameinstances.SelectedItem as GSSFrameInstance;
            frameInfo.Text = JsonConvert.SerializeObject(fi, Formatting.Indented);
        }

        private void Load_Click(object sender, EventArgs e) {
            GSSSkill temp = null;
            var memtemp = new MemoryStream();
            openFileDialog1.InitialDirectory = ProcStat.getBasedir();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Stream myStream = null;
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        myStream.CopyTo(memtemp);
                        memtemp.Seek(0, SeekOrigin.Begin);
                        temp = new BinaryFormatter().Deserialize(memtemp) as GSSSkill;
                        if (!temp.version.Equals(ProcStat.Version))
                        {
                            throw new Exception("Invalid version (skill : v" + temp.version + ", GSS Builder v" + ProcStat.Version + ")");
                        }
                        skillName.Text = temp.skillname;
                        skillDescrption.Text = temp.description;
                        foreach (var item in temp.options)
                        {
                            GSSSkillOption gsssopt = GSSSkillOption.getByName(item.Key);
                            if (gsssopt != null)
                                opt[gsssopt] = JsonConvert.SerializeObject(item.Value);
                        }
                        frameinstances.Items.Clear();
                        frameinstances.Items.AddRange(temp.frames);
                    }
                    MessageBox.Show("Load Complete");
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.ToString());
                    return;
                }
                finally
                {
                    if (myStream != null)
                    {
                        myStream.Close();
                        
                    }
                }
            }
            
        }

        private void Editor_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + "  " + ProcStat.dtoffset.ToString("yyyy-MM-dd");
            // 프레임 선택
            SelectFrame();
            // 옵션
            foreach (GSSSkillOption sopt in GSSSkillOption.All())
            {
                options.Items.Add(sopt);
            }
            options.SelectedIndex = 0;
            // 프레임탭
            foreach (GSSFrame f in GSSFrame.frames)
            {
                if (f.frametype == GSSFrameType.Act)
                {
                    actList.Items.Add(f);
                }
                else if (f.frametype == GSSFrameType.Control)
                {
                    controlList.Items.Add(f);
                }
                else if (f.frametype == GSSFrameType.ETC)
                {
                    etcList.Items.Add(f);
                }
                else if (f.frametype == GSSFrameType.Find)
                {
                    findList.Items.Add(f);
                }
                else if (f.frametype == GSSFrameType.Listen)
                {
                    listenList.Items.Add(f);
                }
                else if (f.frametype == GSSFrameType.Manage)
                {
                    manageList.Items.Add(f);
                }
                else if (f.frametype == GSSFrameType.Movement)
                {
                    movementList.Items.Add(f);
                }
                else if (f.frametype == GSSFrameType.Select)
                {
                    selectList.Items.Add(f);
                }
                else if (f.frametype == GSSFrameType.Show)
                {
                    showList.Items.Add(f);
                }
                else if (f.frametype == GSSFrameType.Sort)
                {
                    sortList.Items.Add(f);
                }
            }
        }
    }
}
