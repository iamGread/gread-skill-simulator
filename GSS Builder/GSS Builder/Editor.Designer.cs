﻿namespace GSS_Builder
{
    partial class Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.frameinstances = new System.Windows.Forms.ListBox();
            this.skillName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.options = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.frameTab = new System.Windows.Forms.TabControl();
            this.Act = new System.Windows.Forms.TabPage();
            this.actList = new System.Windows.Forms.ListBox();
            this.Control = new System.Windows.Forms.TabPage();
            this.controlList = new System.Windows.Forms.ListBox();
            this.ETC = new System.Windows.Forms.TabPage();
            this.etcList = new System.Windows.Forms.ListBox();
            this.Find = new System.Windows.Forms.TabPage();
            this.findList = new System.Windows.Forms.ListBox();
            this.Listen = new System.Windows.Forms.TabPage();
            this.listenList = new System.Windows.Forms.ListBox();
            this.Manage = new System.Windows.Forms.TabPage();
            this.manageList = new System.Windows.Forms.ListBox();
            this.Movement = new System.Windows.Forms.TabPage();
            this.movementList = new System.Windows.Forms.ListBox();
            this.Select = new System.Windows.Forms.TabPage();
            this.selectList = new System.Windows.Forms.ListBox();
            this.Show = new System.Windows.Forms.TabPage();
            this.showList = new System.Windows.Forms.ListBox();
            this.Sort = new System.Windows.Forms.TabPage();
            this.sortList = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.skillDescrption = new System.Windows.Forms.RichTextBox();
            this.Save = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.optionValue = new System.Windows.Forms.ComboBox();
            this.Append = new System.Windows.Forms.Button();
            this.constructors = new System.Windows.Forms.ComboBox();
            this.argKey = new System.Windows.Forms.ComboBox();
            this.argValue = new System.Windows.Forms.ComboBox();
            this.frameName = new System.Windows.Forms.Label();
            this.Clean = new System.Windows.Forms.Button();
            this.Replace = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.Remove = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.Keyname = new System.Windows.Forms.Label();
            this.frameInfo = new System.Windows.Forms.RichTextBox();
            this.frameLabel = new System.Windows.Forms.TextBox();
            this.btn_Load = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.frameTab.SuspendLayout();
            this.Act.SuspendLayout();
            this.Control.SuspendLayout();
            this.ETC.SuspendLayout();
            this.Find.SuspendLayout();
            this.Listen.SuspendLayout();
            this.Manage.SuspendLayout();
            this.Movement.SuspendLayout();
            this.Select.SuspendLayout();
            this.Show.SuspendLayout();
            this.Sort.SuspendLayout();
            this.SuspendLayout();
            // 
            // frameinstances
            // 
            this.frameinstances.FormattingEnabled = true;
            this.frameinstances.ItemHeight = 12;
            this.frameinstances.Location = new System.Drawing.Point(12, 12);
            this.frameinstances.Name = "frameinstances";
            this.frameinstances.Size = new System.Drawing.Size(599, 472);
            this.frameinstances.TabIndex = 0;
            this.frameinstances.SelectedIndexChanged += new System.EventHandler(this.frameinstances_SelectedIndexChanged);
            this.frameinstances.DoubleClick += new System.EventHandler(this.frameinstances_DoubleClick);
            // 
            // skillName
            // 
            this.skillName.Location = new System.Drawing.Point(696, 12);
            this.skillName.Name = "skillName";
            this.skillName.Size = new System.Drawing.Size(752, 21);
            this.skillName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 11F);
            this.label1.Location = new System.Drawing.Point(617, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Skill name";
            // 
            // options
            // 
            this.options.FormattingEnabled = true;
            this.options.ItemHeight = 12;
            this.options.Location = new System.Drawing.Point(620, 62);
            this.options.Name = "options";
            this.options.Size = new System.Drawing.Size(223, 76);
            this.options.TabIndex = 3;
            this.options.SelectedIndexChanged += new System.EventHandler(this.options_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 11F);
            this.label2.Location = new System.Drawing.Point(617, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Options";
            // 
            // frameTab
            // 
            this.frameTab.Controls.Add(this.Act);
            this.frameTab.Controls.Add(this.Control);
            this.frameTab.Controls.Add(this.ETC);
            this.frameTab.Controls.Add(this.Find);
            this.frameTab.Controls.Add(this.Listen);
            this.frameTab.Controls.Add(this.Manage);
            this.frameTab.Controls.Add(this.Movement);
            this.frameTab.Controls.Add(this.Select);
            this.frameTab.Controls.Add(this.Show);
            this.frameTab.Controls.Add(this.Sort);
            this.frameTab.Location = new System.Drawing.Point(863, 188);
            this.frameTab.Name = "frameTab";
            this.frameTab.SelectedIndex = 0;
            this.frameTab.Size = new System.Drawing.Size(585, 491);
            this.frameTab.TabIndex = 6;
            // 
            // Act
            // 
            this.Act.Controls.Add(this.actList);
            this.Act.Location = new System.Drawing.Point(4, 22);
            this.Act.Name = "Act";
            this.Act.Padding = new System.Windows.Forms.Padding(3);
            this.Act.Size = new System.Drawing.Size(577, 465);
            this.Act.TabIndex = 0;
            this.Act.Text = "Act";
            this.Act.UseVisualStyleBackColor = true;
            // 
            // actList
            // 
            this.actList.FormattingEnabled = true;
            this.actList.ItemHeight = 12;
            this.actList.Location = new System.Drawing.Point(0, 0);
            this.actList.Name = "actList";
            this.actList.Size = new System.Drawing.Size(577, 460);
            this.actList.TabIndex = 0;
            this.actList.SelectedIndexChanged += new System.EventHandler(this.actList_SelectedIndexChanged);
            // 
            // Control
            // 
            this.Control.Controls.Add(this.controlList);
            this.Control.Location = new System.Drawing.Point(4, 22);
            this.Control.Name = "Control";
            this.Control.Padding = new System.Windows.Forms.Padding(3);
            this.Control.Size = new System.Drawing.Size(577, 465);
            this.Control.TabIndex = 1;
            this.Control.Text = "Control";
            this.Control.UseVisualStyleBackColor = true;
            // 
            // controlList
            // 
            this.controlList.FormattingEnabled = true;
            this.controlList.ItemHeight = 12;
            this.controlList.Location = new System.Drawing.Point(0, 2);
            this.controlList.Name = "controlList";
            this.controlList.Size = new System.Drawing.Size(577, 460);
            this.controlList.TabIndex = 1;
            this.controlList.SelectedIndexChanged += new System.EventHandler(this.controlList_SelectedIndexChanged);
            // 
            // ETC
            // 
            this.ETC.Controls.Add(this.etcList);
            this.ETC.Location = new System.Drawing.Point(4, 22);
            this.ETC.Name = "ETC";
            this.ETC.Padding = new System.Windows.Forms.Padding(3);
            this.ETC.Size = new System.Drawing.Size(577, 465);
            this.ETC.TabIndex = 2;
            this.ETC.Text = "ETC";
            this.ETC.UseVisualStyleBackColor = true;
            // 
            // etcList
            // 
            this.etcList.FormattingEnabled = true;
            this.etcList.ItemHeight = 12;
            this.etcList.Location = new System.Drawing.Point(0, 2);
            this.etcList.Name = "etcList";
            this.etcList.Size = new System.Drawing.Size(577, 460);
            this.etcList.TabIndex = 2;
            this.etcList.SelectedIndexChanged += new System.EventHandler(this.etcList_SelectedIndexChanged);
            // 
            // Find
            // 
            this.Find.Controls.Add(this.findList);
            this.Find.Location = new System.Drawing.Point(4, 22);
            this.Find.Name = "Find";
            this.Find.Size = new System.Drawing.Size(577, 465);
            this.Find.TabIndex = 3;
            this.Find.Text = "Find";
            this.Find.UseVisualStyleBackColor = true;
            // 
            // findList
            // 
            this.findList.FormattingEnabled = true;
            this.findList.ItemHeight = 12;
            this.findList.Location = new System.Drawing.Point(0, 2);
            this.findList.Name = "findList";
            this.findList.Size = new System.Drawing.Size(577, 460);
            this.findList.TabIndex = 2;
            this.findList.SelectedIndexChanged += new System.EventHandler(this.findList_SelectedIndexChanged);
            // 
            // Listen
            // 
            this.Listen.Controls.Add(this.listenList);
            this.Listen.Location = new System.Drawing.Point(4, 22);
            this.Listen.Name = "Listen";
            this.Listen.Size = new System.Drawing.Size(577, 465);
            this.Listen.TabIndex = 4;
            this.Listen.Text = "Listen";
            this.Listen.UseVisualStyleBackColor = true;
            // 
            // listenList
            // 
            this.listenList.FormattingEnabled = true;
            this.listenList.ItemHeight = 12;
            this.listenList.Location = new System.Drawing.Point(0, 2);
            this.listenList.Name = "listenList";
            this.listenList.Size = new System.Drawing.Size(577, 460);
            this.listenList.TabIndex = 2;
            this.listenList.SelectedIndexChanged += new System.EventHandler(this.listenList_SelectedIndexChanged);
            // 
            // Manage
            // 
            this.Manage.Controls.Add(this.manageList);
            this.Manage.Location = new System.Drawing.Point(4, 22);
            this.Manage.Name = "Manage";
            this.Manage.Size = new System.Drawing.Size(577, 465);
            this.Manage.TabIndex = 5;
            this.Manage.Text = "Manage";
            this.Manage.UseVisualStyleBackColor = true;
            // 
            // manageList
            // 
            this.manageList.FormattingEnabled = true;
            this.manageList.ItemHeight = 12;
            this.manageList.Location = new System.Drawing.Point(0, 2);
            this.manageList.Name = "manageList";
            this.manageList.Size = new System.Drawing.Size(577, 460);
            this.manageList.TabIndex = 2;
            this.manageList.SelectedIndexChanged += new System.EventHandler(this.manageList_SelectedIndexChanged);
            // 
            // Movement
            // 
            this.Movement.Controls.Add(this.movementList);
            this.Movement.Location = new System.Drawing.Point(4, 22);
            this.Movement.Name = "Movement";
            this.Movement.Size = new System.Drawing.Size(577, 465);
            this.Movement.TabIndex = 6;
            this.Movement.Text = "Movement";
            this.Movement.UseVisualStyleBackColor = true;
            // 
            // movementList
            // 
            this.movementList.FormattingEnabled = true;
            this.movementList.ItemHeight = 12;
            this.movementList.Location = new System.Drawing.Point(0, 2);
            this.movementList.Name = "movementList";
            this.movementList.Size = new System.Drawing.Size(577, 460);
            this.movementList.TabIndex = 2;
            this.movementList.SelectedIndexChanged += new System.EventHandler(this.movementList_SelectedIndexChanged);
            // 
            // Select
            // 
            this.Select.Controls.Add(this.selectList);
            this.Select.Location = new System.Drawing.Point(4, 22);
            this.Select.Name = "Select";
            this.Select.Size = new System.Drawing.Size(577, 465);
            this.Select.TabIndex = 7;
            this.Select.Text = "Select";
            this.Select.UseVisualStyleBackColor = true;
            // 
            // selectList
            // 
            this.selectList.FormattingEnabled = true;
            this.selectList.ItemHeight = 12;
            this.selectList.Location = new System.Drawing.Point(0, 2);
            this.selectList.Name = "selectList";
            this.selectList.Size = new System.Drawing.Size(577, 460);
            this.selectList.TabIndex = 2;
            this.selectList.SelectedIndexChanged += new System.EventHandler(this.selectList_SelectedIndexChanged);
            // 
            // Show
            // 
            this.Show.Controls.Add(this.showList);
            this.Show.Location = new System.Drawing.Point(4, 22);
            this.Show.Name = "Show";
            this.Show.Size = new System.Drawing.Size(577, 465);
            this.Show.TabIndex = 8;
            this.Show.Text = "Show";
            this.Show.UseVisualStyleBackColor = true;
            // 
            // showList
            // 
            this.showList.FormattingEnabled = true;
            this.showList.ItemHeight = 12;
            this.showList.Location = new System.Drawing.Point(0, 2);
            this.showList.Name = "showList";
            this.showList.Size = new System.Drawing.Size(577, 460);
            this.showList.TabIndex = 2;
            this.showList.SelectedIndexChanged += new System.EventHandler(this.showList_SelectedIndexChanged);
            // 
            // Sort
            // 
            this.Sort.Controls.Add(this.sortList);
            this.Sort.Location = new System.Drawing.Point(4, 22);
            this.Sort.Name = "Sort";
            this.Sort.Size = new System.Drawing.Size(577, 465);
            this.Sort.TabIndex = 9;
            this.Sort.Text = "Sort";
            this.Sort.UseVisualStyleBackColor = true;
            // 
            // sortList
            // 
            this.sortList.FormattingEnabled = true;
            this.sortList.ItemHeight = 12;
            this.sortList.Location = new System.Drawing.Point(0, 2);
            this.sortList.Name = "sortList";
            this.sortList.Size = new System.Drawing.Size(577, 460);
            this.sortList.TabIndex = 2;
            this.sortList.SelectedIndexChanged += new System.EventHandler(this.sortList_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 11F);
            this.label3.Location = new System.Drawing.Point(851, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Description";
            // 
            // skillDescrption
            // 
            this.skillDescrption.Location = new System.Drawing.Point(854, 62);
            this.skillDescrption.Name = "skillDescrption";
            this.skillDescrption.Size = new System.Drawing.Size(594, 103);
            this.skillDescrption.TabIndex = 9;
            this.skillDescrption.Text = "";
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(1373, 790);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(75, 23);
            this.Save.TabIndex = 11;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(536, 790);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 12;
            this.button3.Text = "Clear";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // optionValue
            // 
            this.optionValue.FormattingEnabled = true;
            this.optionValue.Location = new System.Drawing.Point(620, 145);
            this.optionValue.Name = "optionValue";
            this.optionValue.Size = new System.Drawing.Size(223, 20);
            this.optionValue.TabIndex = 13;
            this.optionValue.Leave += new System.EventHandler(this.optionValue_Leave);
            // 
            // Append
            // 
            this.Append.Location = new System.Drawing.Point(1292, 740);
            this.Append.Name = "Append";
            this.Append.Size = new System.Drawing.Size(75, 23);
            this.Append.TabIndex = 14;
            this.Append.Text = "Append";
            this.Append.UseVisualStyleBackColor = true;
            this.Append.Click += new System.EventHandler(this.button4_Click);
            // 
            // constructors
            // 
            this.constructors.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.constructors.FormattingEnabled = true;
            this.constructors.Location = new System.Drawing.Point(782, 685);
            this.constructors.Name = "constructors";
            this.constructors.Size = new System.Drawing.Size(666, 20);
            this.constructors.TabIndex = 15;
            this.constructors.SelectedIndexChanged += new System.EventHandler(this.constructors_SelectedIndexChanged);
            // 
            // argKey
            // 
            this.argKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.argKey.FormattingEnabled = true;
            this.argKey.Location = new System.Drawing.Point(624, 711);
            this.argKey.Name = "argKey";
            this.argKey.Size = new System.Drawing.Size(152, 20);
            this.argKey.TabIndex = 16;
            this.argKey.SelectedIndexChanged += new System.EventHandler(this.argKey_SelectedIndexChanged);
            // 
            // argValue
            // 
            this.argValue.FormattingEnabled = true;
            this.argValue.Location = new System.Drawing.Point(968, 711);
            this.argValue.Name = "argValue";
            this.argValue.Size = new System.Drawing.Size(480, 20);
            this.argValue.TabIndex = 17;
            this.argValue.Leave += new System.EventHandler(this.argValue_Leave);
            // 
            // frameName
            // 
            this.frameName.AutoSize = true;
            this.frameName.Font = new System.Drawing.Font("굴림", 11F);
            this.frameName.Location = new System.Drawing.Point(621, 685);
            this.frameName.Name = "frameName";
            this.frameName.Size = new System.Drawing.Size(41, 15);
            this.frameName.TabIndex = 18;
            this.frameName.Text = "None";
            // 
            // Clean
            // 
            this.Clean.Location = new System.Drawing.Point(1373, 740);
            this.Clean.Name = "Clean";
            this.Clean.Size = new System.Drawing.Size(75, 23);
            this.Clean.TabIndex = 20;
            this.Clean.Text = "Clean";
            this.Clean.UseVisualStyleBackColor = true;
            this.Clean.Click += new System.EventHandler(this.Clean_Click);
            // 
            // Replace
            // 
            this.Replace.Location = new System.Drawing.Point(1211, 740);
            this.Replace.Name = "Replace";
            this.Replace.Size = new System.Drawing.Size(75, 23);
            this.Replace.TabIndex = 21;
            this.Replace.Text = "Replace";
            this.Replace.UseVisualStyleBackColor = true;
            this.Replace.Click += new System.EventHandler(this.Replace_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "json";
            this.saveFileDialog1.Filter = "Json (*.json)|*.json";
            this.saveFileDialog1.InitialDirectory = "\".\\..\"";
            this.saveFileDialog1.Title = "GSS Skill Save";
            // 
            // Remove
            // 
            this.Remove.Location = new System.Drawing.Point(455, 790);
            this.Remove.Name = "Remove";
            this.Remove.Size = new System.Drawing.Size(75, 23);
            this.Remove.TabIndex = 22;
            this.Remove.Text = "Remove";
            this.Remove.UseVisualStyleBackColor = true;
            this.Remove.Click += new System.EventHandler(this.Remove_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("굴림", 11F);
            this.label4.Location = new System.Drawing.Point(621, 745);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 15);
            this.label4.TabIndex = 23;
            this.label4.Text = "Label";
            // 
            // Keyname
            // 
            this.Keyname.AutoSize = true;
            this.Keyname.Font = new System.Drawing.Font("굴림", 11F);
            this.Keyname.Location = new System.Drawing.Point(782, 714);
            this.Keyname.Name = "Keyname";
            this.Keyname.Size = new System.Drawing.Size(41, 15);
            this.Keyname.TabIndex = 24;
            this.Keyname.Text = "None";
            // 
            // frameInfo
            // 
            this.frameInfo.Location = new System.Drawing.Point(12, 490);
            this.frameInfo.Name = "frameInfo";
            this.frameInfo.ReadOnly = true;
            this.frameInfo.Size = new System.Drawing.Size(599, 268);
            this.frameInfo.TabIndex = 25;
            this.frameInfo.Text = "";
            // 
            // frameLabel
            // 
            this.frameLabel.Location = new System.Drawing.Point(782, 741);
            this.frameLabel.Name = "frameLabel";
            this.frameLabel.Size = new System.Drawing.Size(423, 21);
            this.frameLabel.TabIndex = 27;
            // 
            // btn_Load
            // 
            this.btn_Load.Location = new System.Drawing.Point(1292, 790);
            this.btn_Load.Name = "btn_Load";
            this.btn_Load.Size = new System.Drawing.Size(75, 23);
            this.btn_Load.TabIndex = 28;
            this.btn_Load.Text = "Load";
            this.btn_Load.UseVisualStyleBackColor = true;
            this.btn_Load.Click += new System.EventHandler(this.Load_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "GSS Builder (*.gsb)|*gsb";
            this.openFileDialog1.InitialDirectory = ".\\..";
            // 
            // Editor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1456, 817);
            this.Controls.Add(this.btn_Load);
            this.Controls.Add(this.frameLabel);
            this.Controls.Add(this.frameInfo);
            this.Controls.Add(this.Keyname);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Remove);
            this.Controls.Add(this.Replace);
            this.Controls.Add(this.Clean);
            this.Controls.Add(this.frameName);
            this.Controls.Add(this.argValue);
            this.Controls.Add(this.argKey);
            this.Controls.Add(this.constructors);
            this.Controls.Add(this.Append);
            this.Controls.Add(this.optionValue);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.skillDescrption);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.frameTab);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.options);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.skillName);
            this.Controls.Add(this.frameinstances);
            this.Name = "Editor";
            this.Text = "Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Editor_FormClosing);
            this.Load += new System.EventHandler(this.Editor_Load);
            this.frameTab.ResumeLayout(false);
            this.Act.ResumeLayout(false);
            this.Control.ResumeLayout(false);
            this.ETC.ResumeLayout(false);
            this.Find.ResumeLayout(false);
            this.Listen.ResumeLayout(false);
            this.Manage.ResumeLayout(false);
            this.Movement.ResumeLayout(false);
            this.Select.ResumeLayout(false);
            this.Show.ResumeLayout(false);
            this.Sort.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox frameinstances;
        private System.Windows.Forms.TextBox skillName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox options;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl frameTab;
        private System.Windows.Forms.TabPage Act;
        public System.Windows.Forms.TabPage Control;
        private System.Windows.Forms.TabPage ETC;
        private System.Windows.Forms.TabPage Find;
        private System.Windows.Forms.TabPage Listen;
        private System.Windows.Forms.TabPage Manage;
        private System.Windows.Forms.TabPage Movement;
        private System.Windows.Forms.TabPage Select;
        private System.Windows.Forms.TabPage Show;
        private System.Windows.Forms.TabPage Sort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox skillDescrption;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox optionValue;
        private System.Windows.Forms.ListBox actList;
        private System.Windows.Forms.ListBox controlList;
        private System.Windows.Forms.ListBox etcList;
        private System.Windows.Forms.ListBox findList;
        private System.Windows.Forms.ListBox listenList;
        private System.Windows.Forms.ListBox manageList;
        private System.Windows.Forms.ListBox movementList;
        private System.Windows.Forms.ListBox selectList;
        private System.Windows.Forms.ListBox showList;
        private System.Windows.Forms.ListBox sortList;
        private System.Windows.Forms.Button Append;
        private System.Windows.Forms.ComboBox constructors;
        private System.Windows.Forms.ComboBox argKey;
        private System.Windows.Forms.ComboBox argValue;
        private System.Windows.Forms.Label frameName;
        private System.Windows.Forms.Button Clean;
        private System.Windows.Forms.Button Replace;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button Remove;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Keyname;
        private System.Windows.Forms.RichTextBox frameInfo;
        private System.Windows.Forms.TextBox frameLabel;
        private System.Windows.Forms.Button btn_Load;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}