﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace GSS_Builder
{
    [Serializable]
    class GSSSkill
    {
        [JsonIgnore]
        public string version;
        [JsonProperty(Required = Required.Always)]
        public string skillname;
        [JsonProperty(Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string description;
        [JsonProperty(Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, object> options;
        [JsonProperty(Required = Required.Always)]
        public GSSFrameInstance[] frames;

        public GSSSkill(string skillname, string description, Dictionary<string, object> options, GSSFrameInstance[] frames)
        {
            version = ProcStat.Version;
            this.skillname = skillname;
            this.description = description;
            this.options = options;
            this.frames = frames;
        }
    }
    [Serializable]
    class GSSFrameInstanceBinary
    {
        [JsonProperty(Required = Required.Always)]
        byte[] bytes;

        public GSSFrameInstanceBinary(GSSFrameInstance gssfi)
        {
            var temp = new MemoryStream();
            new BinaryFormatter().Serialize(temp, gssfi);
            this.bytes = temp.ToArray();
        }
        public GSSFrameInstance getInstance()
        {
            var temp = new MemoryStream(bytes);

            object obj = new BinaryFormatter().Deserialize(temp);
            if (temp is GSSFrameInstance)
            {
                return obj as GSSFrameInstance;
            }
            return null;
        }
    }
    [Serializable]
    class GSSFrameInstance
    {
        [JsonProperty(Required = Required.Always)]
        public readonly string frame;
        [JsonIgnore]
        public readonly GSSFrame frameType;
        [JsonIgnore]
        public readonly GSSFrameConstructor constructor;
        [JsonProperty(Required = Required.AllowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string label;
        [JsonIgnore]
        public readonly string[] arguaments;
        [JsonProperty(Required = Required.Always)]
        public readonly object[] args;

        public GSSFrameInstance(GSSFrame frame, string label, GSSFrameConstructor constructor, string[] args)
        {
            this.frame = frame.framename;
            this.frameType = frame;
            this.constructor = constructor;
            this.label = label;
            this.arguaments = args;

            this.args = new object[constructor.argstype.Length];
            for (int i = 0; i < constructor.argstype.Length; i++)
            {
                this.args[i] = constructor.argstype[i].toObject(this.arguaments[i]);
            }
        }
        public GSSFrameInstance(GSSFrame frame, GSSFrameConstructor constructor, string[] args)
        {
            this.frame = frame.framename;
            this.frameType = frame;
            this.constructor = constructor;
            this.label = null;
            this.arguaments = args;

            this.args = new object[constructor.argstype.Length];
            for (int i = 0; i < constructor.argstype.Length; i++)
            {
                this.args[i] = constructor.argstype[i].toObject(this.arguaments[i]);
            }


        }

        public GSSFrameInstance(string frame, GSSFrame frameType, GSSFrameConstructor constructor, string label, string[] arguaments, object[] args)
        {
            this.frame = frame;
            this.frameType = frameType;
            this.constructor = constructor;
            this.label = label;
            this.arguaments = arguaments;
            this.args = args;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (label != null && label.Length > 0)
            {
                sb.Append(label);
                sb.Append(" : ");
            }
            sb.Append(frameType.framename);
            if (constructor.validArgs(arguaments))
            {
                sb.Append("(");
                for (int i = 0; i < arguaments.Length; i++)
                {
                    sb.Append(constructor.argstype[i].Stringify(arguaments[i]));
                    if (i < arguaments.Length - 1)
                    {
                        sb.Append(", ");
                    }
                }
                sb.Append(")");
            }
            else
            {
                sb.Append("(Invalid)");
            }

            return sb.ToString();
        }
    }
    [Serializable]
    class GSSFrame
    {
        public readonly static List<GSSFrame> frames = new List<GSSFrame>();

        public readonly string framename;
        public readonly GSSFrameType frametype;
        public readonly GSSFrameConstructor[] constructors;
        public readonly GSSFrameEnum[] frameenum;

        public GSSFrame(string framename, GSSFrameType frametype, GSSFrameConstructor[] constructors, GSSFrameEnum[] frameenum)
        {
            this.framename = framename;
            this.frametype = frametype;
            this.constructors = constructors;
            this.frameenum = frameenum;
        }

        public override string ToString()
        {
            return framename;
        }
    }
    [Serializable]
    class GSSFrameEnum
    {
        public readonly string enumname;
        public readonly string[] values;

        public GSSFrameEnum(string enumname, string[] values)
        {
            this.enumname = enumname;
            this.values = values;
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("Enum ");
            sb.Append(enumname);
            sb.Append(" [");
            for (int i = 0; i < values.Length; i++)
            {
                sb.Append(values[i]);
                if (i < values.Length - 1)
                    sb.Append(" / ");
            }
            sb.Append("]");

            return sb.ToString();
        }
        public override bool Equals(object obj)
        {
            if (obj is GSSFrameEnum)
            {
                GSSFrameEnum gssfe = obj as GSSFrameEnum;
                return gssfe.enumname.Equals(this.enumname) && gssfe.values.SequenceEqual(this.values);
            }
            return false;
        }
    }
    [Serializable]
    class GSSFrameConstructor
    {
        public readonly GSSSupportType[] argstype;
        public readonly string[] argsname;
        public GSSFrameConstructor(GSSSupportType[] argstype, string[] argsname)
        {
            this.argstype = argstype;
            this.argsname = argsname;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("Constructor ");
            sb.Append(" [");
            for (int i = 0; i < argstype.Length; i++)
            {
                sb.Append(argstype[i]);
                sb.Append(" ");
                sb.Append(argsname[i]);
                if (i < argstype.Length - 1)
                    sb.Append(" / ");
            }
            sb.Append("]");
            return sb.ToString();
        }
        public bool validArgs(string[] args)
        {
            if (args.Length != argstype.Length)
            {
                return false;
            }
            for (int i = 0; i < argstype.Length; i++)
            {
                if (argstype[i] == GSSSupportType.Integer && args[i].StartsWith("#"))
                {
                    continue;
                }
                if (!argstype[i].valid(args[i]))
                {
                    return false;
                }
            }
            return true;
        }
    }
    [Serializable]
    public sealed class GSSFrameType
    {
        public static readonly GSSFrameType Act = new GSSFrameType(1, "Act");
        public static readonly GSSFrameType Control = new GSSFrameType(2, "Control");
        public static readonly GSSFrameType ETC = new GSSFrameType(3, "ETC");
        public static readonly GSSFrameType Find = new GSSFrameType(4, "Find");
        public static readonly GSSFrameType Listen = new GSSFrameType(5, "Listen");
        public static readonly GSSFrameType Manage = new GSSFrameType(6, "Manage");
        public static readonly GSSFrameType Movement = new GSSFrameType(7, "Movement");
        public static readonly GSSFrameType Select = new GSSFrameType(8, "Select");
        public static readonly GSSFrameType Show = new GSSFrameType(9, "Show");
        public static readonly GSSFrameType Sort = new GSSFrameType(10, "Sort");


        //
        public readonly int value;
        public readonly String name;
        public static GSSFrameType[] All()
        {
            return new GSSFrameType[]
            {
                Act,
                Control,
                ETC,
                Find,
                Listen,
                Manage,
                Movement,
                Select,
                Show,
                Sort,
            };
        }
        public GSSFrameType(int value, string name)
        {
            this.value = value;
            this.name = name;
        }

        public override String ToString()
        {
            return name;
        }
        public override bool Equals(object obj)
        {
            if (obj is GSSFrameType)
            {
                GSSFrameType gssfe = obj as GSSFrameType;
                return gssfe.value.Equals(this.value) && gssfe.name.Equals(this.name);
            }
            return false;
        }
    }
    [Serializable]
    public sealed class GSSSkillOption
    {
        //
        public static readonly GSSSkillOption Permission = new GSSSkillOption(1, "Permission", GSSSupportType.String);
        public static readonly GSSSkillOption Interceptable = new GSSSkillOption(2, "Interceptable", GSSSupportType.Boolean);
        public static readonly GSSSkillOption MaximumAlive = new GSSSkillOption(3, "MaximumAlive", GSSSupportType.Integer);
        public static readonly GSSSkillOption MaximumOneTickFrame = new GSSSkillOption(4, "MaximumOneTickFrame", GSSSupportType.Integer);
        public static readonly GSSSkillOption BelongBlocksDelete = new GSSSkillOption(5, "BelongBlocksDelete", GSSSupportType.Boolean);
        public static readonly GSSSkillOption BelongEntitysRemove = new GSSSkillOption(6, "BelongEntitysRemove", GSSSupportType.Boolean);
        public static readonly GSSSkillOption EntityChangeRevert = new GSSSkillOption(7, "EntityChangeRevert", GSSSupportType.Boolean);
        //

        public readonly int value;
        public readonly String name;
        public readonly GSSSupportType type;
        public static GSSSkillOption getByName(string text)
        {
            switch (text)
            {
                default:
                    return null;
                case "Permission":
                    return Permission;
                case "Interceptable":
                    return Interceptable;
                case "MaximumAlive":
                    return MaximumAlive;
                case "MaximumOneTickFrame":
                    return MaximumOneTickFrame;
                case "BelongBlocksDelete":
                    return BelongBlocksDelete;
                case "BelongEntitysRemove":
                    return BelongEntitysRemove;
                case "EntityChangeRevert":
                    return EntityChangeRevert;
            }
        }
        public static GSSSkillOption[] All()
        {
            return new GSSSkillOption[]
            {
                Permission,
                Interceptable,
                MaximumAlive,
                MaximumOneTickFrame,
                BelongBlocksDelete,
                BelongEntitysRemove,
                EntityChangeRevert,
            };
        }
        public GSSSkillOption(int value, string name, GSSSupportType type)
        {
            this.value = value;
            this.name = name;
            this.type = type;
        }

        public override String ToString()
        {
            return name;
        }
        public override bool Equals(object obj)
        {
            if (obj is GSSSkillOption)
            {
                GSSSkillOption gssfe = obj as GSSSkillOption;
                return gssfe.value.Equals(this.value) && gssfe.name.Equals(this.name);
            }
            return false;
        }
    }
    [Serializable]
    public sealed class GSSSupportType
    {
        //    new String[]{
        //        // 실수형
        //        "double", "float",
        //        // 정수형
        //        "byte", "short", "int", "long",
        //        // 논리형
        //        "boolean",
        //        // 문자형
        //        "char",
        //        // 문장형
        //        "java.lang.String",
        //        "org.spongepowered.api.text.Text",
        //        // 벡터
        //        "com.flowpowered.math.vector.Vector3d",
        //        // Sponge 타입
        //        "org.spongepowered.api.entity.EntityType",
        //        "org.spongepowered.api.effect.potion.PotionEffectType",
        //        "org.spongepowered.api.block.BlockType",
        //        "org.spongepowered.api.effect.particle.ParticleType",
        //        "org.spongepowered.api.effect.sound.SoundType",
        //        "org.spongepowered.api.text.chat.ChatType",
        //        "org.spongepowered.api.item.ItemType",
        //        // 그 외의 모든 enum 형 지원
        //};

        //
        public static readonly GSSSupportType Byte = new GSSSupportType(1, "byte");
        public static readonly GSSSupportType Short = new GSSSupportType(2, "short");
        public static readonly GSSSupportType Integer = new GSSSupportType(3, "int");
        public static readonly GSSSupportType Long = new GSSSupportType(4, "long");
        //
        public static readonly GSSSupportType Double = new GSSSupportType(5, "double");
        public static readonly GSSSupportType Float = new GSSSupportType(6, "float");
        //
        public static readonly GSSSupportType Boolean = new GSSSupportType(7, "boolean", new string[]{
            "true",
            "false"
        });
        //
        public static readonly GSSSupportType Char = new GSSSupportType(8, "char");
        //
        public static readonly GSSSupportType String = new GSSSupportType(9, "String");
        public static readonly GSSSupportType Text = new GSSSupportType(10, "Text");
        //
        public static readonly GSSSupportType Vector3d = new GSSSupportType(11, "Vector3d");
        //
        public static readonly GSSSupportType EntityType = new GSSSupportType(12, "EntityType", new string[]{
            "minecraft:areaeffectcloud",
    "minecraft:armorstand",
    "minecraft:arrow",
    "minecraft:bat",
    "minecraft:blaze",
    "minecraft:boat",
    "minecraft:cavespider",
    "minecraft:chicken",
    "minecraft:complexpart",
    "minecraft:cow",
    "minecraft:creeper",
    "minecraft:dragonfireball",
    "minecraft:egg",
    "minecraft:endercrystal",
    "minecraft:enderdragon",
    "minecraft:enderman",
    "minecraft:endermite",
    "minecraft:entityhorse",
    "minecraft:eyeofendersignal",
    "minecraft:fallingsand",
    "minecraft:fireworksrocketentity",
    "minecraft:fishinghook",
    "minecraft:ghast",
    "minecraft:giant",
    "minecraft:guardian",
    "minecraft:item",
    "minecraft:itemframe",
    "minecraft:largefireball",
    "minecraft:lavaslime",
    "minecraft:leashknot",
    "minecraft:lightning",
    "minecraft:minecartchest",
    "minecraft:minecartcommandblock",
    "minecraft:minecartfurnace",
    "minecraft:minecarthopper",
    "minecraft:minecartrideable",
    "minecraft:minecartspawner",
    "minecraft:minecarttnt",
    "minecraft:mushroomcow",
    "minecraft:ocelot",
    "minecraft:ocelot",
    "minecraft:painting",
    "minecraft:pig",
    "minecraft:pigzombie",
    "minecraft:player",
    "minecraft:polarbear",
    "minecraft:primedtnt",
    "minecraft:rabbit",
    "minecraft:sheep",
    "minecraft:shulker",
    "minecraft:shulkerbullet",
    "minecraft:silverfish",
    "minecraft:skeleton",
    "minecraft:slime",
    "minecraft:smallfireball",
    "minecraft:snowball",
    "minecraft:snowman",
    "minecraft:spectralarrow",
    "minecraft:spider",
    "minecraft:squid",
    "minecraft:thrownenderpearl",
    "minecraft:thrownexpbottle",
    "minecraft:thrownpotion",
    "minecraft:villager",
    "minecraft:villagergolem",
    "minecraft:weather",
    "minecraft:witch",
    "minecraft:witherboss",
    "minecraft:witherskull",
    "minecraft:wolf",
    "minecraft:xporb",
    "minecraft:zombie",
    "sponge:human"
        });
        public static readonly GSSSupportType PotionEffectType = new GSSSupportType(12, "PotionEffectType", new string[]{
            "minecraft:absorption",
    "minecraft:blindness",
    "minecraft:fire_resistance",
    "minecraft:glowing",
    "minecraft:haste",
    "minecraft:health_boost",
    "minecraft:hunger",
    "minecraft:instant_damage",
    "minecraft:instant_health",
    "minecraft:invisibility",
    "minecraft:jump_boost",
    "minecraft:levitation",
    "minecraft:luck",
    "minecraft:mining_fatigue",
    "minecraft:nausea",
    "minecraft:night_vision",
    "minecraft:poison",
    "minecraft:regeneration",
    "minecraft:resistance",
    "minecraft:saturation",
    "minecraft:slowness",
    "minecraft:speed",
    "minecraft:strength",
    "minecraft:unluck",
    "minecraft:water_breathing",
    "minecraft:weakness",
    "minecraft:wither"
        });
        public static readonly GSSSupportType BlockType = new GSSSupportType(13, "BlockType", new string[]{
            "minecraft:acacia_door",
    "minecraft:acacia_fence",
    "minecraft:acacia_fence_gate",
    "minecraft:acacia_stairs",
    "minecraft:activator_rail",
    "minecraft:air",
    "minecraft:anvil",
    "minecraft:barrier",
    "minecraft:beacon",
    "minecraft:bed",
    "minecraft:bedrock",
    "minecraft:beetroots",
    "minecraft:birch_door",
    "minecraft:birch_fence",
    "minecraft:birch_fence_gate",
    "minecraft:birch_stairs",
    "minecraft:bone_block",
    "minecraft:bookshelf",
    "minecraft:brewing_stand",
    "minecraft:brick_block",
    "minecraft:brick_stairs",
    "minecraft:brown_mushroom",
    "minecraft:brown_mushroom_block",
    "minecraft:cactus",
    "minecraft:cake",
    "minecraft:carpet",
    "minecraft:carrots",
    "minecraft:cauldron",
    "minecraft:chain_command_block",
    "minecraft:chest",
    "minecraft:chorus_flower",
    "minecraft:chorus_plant",
    "minecraft:clay",
    "minecraft:coal_block",
    "minecraft:coal_ore",
    "minecraft:cobblestone",
    "minecraft:cobblestone_wall",
    "minecraft:cocoa",
    "minecraft:command_block",
    "minecraft:crafting_table",
    "minecraft:dark_oak_door",
    "minecraft:dark_oak_fence",
    "minecraft:dark_oak_fence_gate",
    "minecraft:dark_oak_stairs",
    "minecraft:daylight_detector",
    "minecraft:daylight_detector_inverted",
    "minecraft:deadbush",
    "minecraft:detector_rail",
    "minecraft:diamond_block",
    "minecraft:diamond_ore",
    "minecraft:dirt",
    "minecraft:dispenser",
    "minecraft:double_plant",
    "minecraft:double_stone_slab",
    "minecraft:double_stone_slab2",
    "minecraft:double_wooden_slab",
    "minecraft:dragon_egg",
    "minecraft:dropper",
    "minecraft:emerald_block",
    "minecraft:emerald_ore",
    "minecraft:enchanting_table",
    "minecraft:end_bricks",
    "minecraft:end_gateway",
    "minecraft:end_portal",
    "minecraft:end_portal_frame",
    "minecraft:end_rod",
    "minecraft:end_stone",
    "minecraft:ender_chest",
    "minecraft:farmland",
    "minecraft:fence",
    "minecraft:fence_gate",
    "minecraft:fire",
    "minecraft:flower_pot",
    "minecraft:flowing_lava",
    "minecraft:flowing_water",
    "minecraft:frosted_ice",
    "minecraft:furnace",
    "minecraft:glass",
    "minecraft:glass_pane",
    "minecraft:glowstone",
    "minecraft:gold_block",
    "minecraft:gold_ore",
    "minecraft:golden_rail",
    "minecraft:grass",
    "minecraft:grass_path",
    "minecraft:gravel",
    "minecraft:hardened_clay",
    "minecraft:hay_block",
    "minecraft:heavy_weighted_pressure_plate",
    "minecraft:hopper",
    "minecraft:ice",
    "minecraft:iron_bars",
    "minecraft:iron_block",
    "minecraft:iron_door",
    "minecraft:iron_ore",
    "minecraft:iron_trapdoor",
    "minecraft:jukebox",
    "minecraft:jungle_door",
    "minecraft:jungle_fence",
    "minecraft:jungle_fence_gate",
    "minecraft:jungle_stairs",
    "minecraft:ladder",
    "minecraft:lapis_block",
    "minecraft:lapis_ore",
    "minecraft:lava",
    "minecraft:leaves",
    "minecraft:leaves2",
    "minecraft:lever",
    "minecraft:light_weighted_pressure_plate",
    "minecraft:lit_furnace",
    "minecraft:lit_pumpkin",
    "minecraft:lit_redstone_lamp",
    "minecraft:lit_redstone_ore",
    "minecraft:log",
    "minecraft:log2",
    "minecraft:magma",
    "minecraft:melon_block",
    "minecraft:melon_stem",
    "minecraft:mob_spawner",
    "minecraft:monster_egg",
    "minecraft:mossy_cobblestone",
    "minecraft:mycelium",
    "minecraft:nether_brick",
    "minecraft:nether_brick_fence",
    "minecraft:nether_brick_stairs",
    "minecraft:nether_wart",
    "minecraft:nether_wart_block",
    "minecraft:netherrack",
    "minecraft:noteblock",
    "minecraft:oak_stairs",
    "minecraft:obsidian",
    "minecraft:packed_ice",
    "minecraft:piston",
    "minecraft:piston_extension",
    "minecraft:piston_head",
    "minecraft:planks",
    "minecraft:portal",
    "minecraft:potatoes",
    "minecraft:powered_comparator",
    "minecraft:powered_repeater",
    "minecraft:prismarine",
    "minecraft:pumpkin",
    "minecraft:pumpkin_stem",
    "minecraft:purpur_block",
    "minecraft:purpur_double_slab",
    "minecraft:purpur_pillar",
    "minecraft:purpur_slab",
    "minecraft:purpur_stairs",
    "minecraft:quartz_block",
    "minecraft:quartz_ore",
    "minecraft:quartz_stairs",
    "minecraft:rail",
    "minecraft:red_flower",
    "minecraft:red_mushroom",
    "minecraft:red_mushroom_block",
    "minecraft:red_nether_brick",
    "minecraft:red_sandstone",
    "minecraft:red_sandstone_stairs",
    "minecraft:redstone_block",
    "minecraft:redstone_lamp",
    "minecraft:redstone_ore",
    "minecraft:redstone_torch",
    "minecraft:redstone_wire",
    "minecraft:reeds",
    "minecraft:repeating_command_block",
    "minecraft:sand",
    "minecraft:sandstone",
    "minecraft:sandstone_stairs",
    "minecraft:sapling",
    "minecraft:sea_lantern",
    "minecraft:skull",
    "minecraft:slime",
    "minecraft:snow",
    "minecraft:snow_layer",
    "minecraft:soul_sand",
    "minecraft:sponge",
    "minecraft:spruce_door",
    "minecraft:spruce_fence",
    "minecraft:spruce_fence_gate",
    "minecraft:spruce_stairs",
    "minecraft:stained_glass",
    "minecraft:stained_glass_pane",
    "minecraft:stained_hardened_clay",
    "minecraft:standing_banner",
    "minecraft:standing_sign",
    "minecraft:sticky_piston",
    "minecraft:stone",
    "minecraft:stone_brick_stairs",
    "minecraft:stone_button",
    "minecraft:stone_pressure_plate",
    "minecraft:stone_slab",
    "minecraft:stone_slab2",
    "minecraft:stone_stairs",
    "minecraft:stonebrick",
    "minecraft:structure_block",
    "minecraft:structure_void",
    "minecraft:tallgrass",
    "minecraft:tnt",
    "minecraft:torch",
    "minecraft:trapdoor",
    "minecraft:trapped_chest",
    "minecraft:tripwire",
    "minecraft:tripwire_hook",
    "minecraft:unlit_redstone_torch",
    "minecraft:unpowered_comparator",
    "minecraft:unpowered_repeater",
    "minecraft:vine",
    "minecraft:wall_banner",
    "minecraft:wall_sign",
    "minecraft:water",
    "minecraft:waterlily",
    "minecraft:web",
    "minecraft:wheat",
    "minecraft:wooden_button",
    "minecraft:wooden_door",
    "minecraft:wooden_pressure_plate",
    "minecraft:wooden_slab",
    "minecraft:wool",
    "minecraft:yellow_flower"
        });
        public static readonly GSSSupportType ParticleType = new GSSSupportType(14, "ParticleType", new string[]{
           "minecraft:ambient_mob_spell",
    "minecraft:angry_villager",
    "minecraft:barrier",
    "minecraft:block_crack",
    "minecraft:block_dust",
    "minecraft:break_block",
    "minecraft:cloud",
    "minecraft:critical_hit",
    "minecraft:damage_indicator",
    "minecraft:dragon_breath",
    "minecraft:dragon_breath_attack",
    "minecraft:drip_lava",
    "minecraft:drip_water",
    "minecraft:enchanting_glyphs",
    "minecraft:end_rod",
    "minecraft:ender_teleport",
    "minecraft:explosion",
    "minecraft:falling_dust",
    "minecraft:fertilizer",
    "minecraft:fire_smoke",
    "minecraft:fireworks",
    "minecraft:fireworks_spark",
    "minecraft:flame",
    "minecraft:footstep",
    "minecraft:guardian_appearance",
    "minecraft:happy_villager",
    "minecraft:heart",
    "minecraft:huge_explosion",
    "minecraft:instant_spell",
    "minecraft:item_crack",
    "minecraft:item_take",
    "minecraft:large_explosion",
    "minecraft:large_smoke",
    "minecraft:lava",
    "minecraft:magic_critical_hit",
    "minecraft:mob_spell",
    "minecraft:mobspawner_flames",
    "minecraft:note",
    "minecraft:portal",
    "minecraft:redstone_dust",
    "minecraft:slime",
    "minecraft:smoke",
    "minecraft:snow_shovel",
    "minecraft:snowball",
    "minecraft:spell",
    "minecraft:splash_potion",
    "minecraft:suspended",
    "minecraft:suspended_depth",
    "minecraft:sweep_attack",
    "minecraft:town_aura",
    "minecraft:water_bubble",
    "minecraft:water_drop",
    "minecraft:water_splash",
    "minecraft:water_wake",
    "minecraft:witch_spell"
        });
        public static readonly GSSSupportType SoundType = new GSSSupportType(15, "SoundType", new string[]{
            "minecraft:ambient.cave",
    "minecraft:block.anvil.break",
    "minecraft:block.anvil.destroy",
    "minecraft:block.anvil.fall",
    "minecraft:block.anvil.hit",
    "minecraft:block.anvil.land",
    "minecraft:block.anvil.place",
    "minecraft:block.anvil.step",
    "minecraft:block.anvil.use",
    "minecraft:block.brewing_stand.brew",
    "minecraft:block.chest.close",
    "minecraft:block.chest.locked",
    "minecraft:block.chest.open",
    "minecraft:block.chorus_flower.death",
    "minecraft:block.chorus_flower.grow",
    "minecraft:block.cloth.break",
    "minecraft:block.cloth.fall",
    "minecraft:block.cloth.hit",
    "minecraft:block.cloth.place",
    "minecraft:block.cloth.step",
    "minecraft:block.comparator.click",
    "minecraft:block.dispenser.dispense",
    "minecraft:block.dispenser.fail",
    "minecraft:block.dispenser.launch",
    "minecraft:block.enchantment_table.use",
    "minecraft:block.end_gateway.spawn",
    "minecraft:block.enderchest.close",
    "minecraft:block.enderchest.open",
    "minecraft:block.fence_gate.close",
    "minecraft:block.fence_gate.open",
    "minecraft:block.fire.ambient",
    "minecraft:block.fire.extinguish",
    "minecraft:block.furnace.fire_crackle",
    "minecraft:block.glass.break",
    "minecraft:block.glass.fall",
    "minecraft:block.glass.hit",
    "minecraft:block.glass.place",
    "minecraft:block.glass.step",
    "minecraft:block.grass.break",
    "minecraft:block.grass.fall",
    "minecraft:block.grass.hit",
    "minecraft:block.grass.place",
    "minecraft:block.grass.step",
    "minecraft:block.gravel.break",
    "minecraft:block.gravel.fall",
    "minecraft:block.gravel.hit",
    "minecraft:block.gravel.place",
    "minecraft:block.gravel.step",
    "minecraft:block.iron_door.close",
    "minecraft:block.iron_door.open",
    "minecraft:block.iron_trapdoor.close",
    "minecraft:block.iron_trapdoor.open",
    "minecraft:block.ladder.break",
    "minecraft:block.ladder.fall",
    "minecraft:block.ladder.hit",
    "minecraft:block.ladder.place",
    "minecraft:block.ladder.step",
    "minecraft:block.lava.ambient",
    "minecraft:block.lava.extinguish",
    "minecraft:block.lava.pop",
    "minecraft:block.lever.click",
    "minecraft:block.metal.break",
    "minecraft:block.metal.fall",
    "minecraft:block.metal.hit",
    "minecraft:block.metal.place",
    "minecraft:block.metal.step",
    "minecraft:block.metal_pressureplate.click_off",
    "minecraft:block.metal_pressureplate.click_on",
    "minecraft:block.note.basedrum",
    "minecraft:block.note.bass",
    "minecraft:block.note.harp",
    "minecraft:block.note.hat",
    "minecraft:block.note.pling",
    "minecraft:block.note.snare",
    "minecraft:block.piston.contract",
    "minecraft:block.piston.extend",
    "minecraft:block.portal.ambient",
    "minecraft:block.portal.travel",
    "minecraft:block.portal.trigger",
    "minecraft:block.redstone_torch.burnout",
    "minecraft:block.sand.break",
    "minecraft:block.sand.fall",
    "minecraft:block.sand.hit",
    "minecraft:block.sand.place",
    "minecraft:block.sand.step",
    "minecraft:block.slime.break",
    "minecraft:block.slime.fall",
    "minecraft:block.slime.hit",
    "minecraft:block.slime.place",
    "minecraft:block.slime.step",
    "minecraft:block.snow.break",
    "minecraft:block.snow.fall",
    "minecraft:block.snow.hit",
    "minecraft:block.snow.place",
    "minecraft:block.snow.step",
    "minecraft:block.stone.break",
    "minecraft:block.stone.fall",
    "minecraft:block.stone.hit",
    "minecraft:block.stone.place",
    "minecraft:block.stone.step",
    "minecraft:block.stone_button.click_off",
    "minecraft:block.stone_button.click_on",
    "minecraft:block.stone_pressureplate.click_off",
    "minecraft:block.stone_pressureplate.click_on",
    "minecraft:block.tripwire.attach",
    "minecraft:block.tripwire.click_off",
    "minecraft:block.tripwire.click_on",
    "minecraft:block.tripwire.detach",
    "minecraft:block.water.ambient",
    "minecraft:block.waterlily.place",
    "minecraft:block.wood.break",
    "minecraft:block.wood.fall",
    "minecraft:block.wood.hit",
    "minecraft:block.wood.place",
    "minecraft:block.wood.step",
    "minecraft:block.wood_button.click_off",
    "minecraft:block.wood_button.click_on",
    "minecraft:block.wood_pressureplate.click_off",
    "minecraft:block.wood_pressureplate.click_on",
    "minecraft:block.wooden_door.close",
    "minecraft:block.wooden_door.open",
    "minecraft:block.wooden_trapdoor.close",
    "minecraft:block.wooden_trapdoor.open",
    "minecraft:enchant.thorns.hit",
    "minecraft:entity.armorstand.break",
    "minecraft:entity.armorstand.fall",
    "minecraft:entity.armorstand.hit",
    "minecraft:entity.armorstand.place",
    "minecraft:entity.arrow.hit",
    "minecraft:entity.arrow.hit_player",
    "minecraft:entity.arrow.shoot",
    "minecraft:entity.bat.ambient",
    "minecraft:entity.bat.death",
    "minecraft:entity.bat.hurt",
    "minecraft:entity.bat.loop",
    "minecraft:entity.bat.takeoff",
    "minecraft:entity.blaze.ambient",
    "minecraft:entity.blaze.burn",
    "minecraft:entity.blaze.death",
    "minecraft:entity.blaze.hurt",
    "minecraft:entity.blaze.shoot",
    "minecraft:entity.bobber.splash",
    "minecraft:entity.bobber.throw",
    "minecraft:entity.cat.ambient",
    "minecraft:entity.cat.death",
    "minecraft:entity.cat.hiss",
    "minecraft:entity.cat.hurt",
    "minecraft:entity.cat.purr",
    "minecraft:entity.cat.purreow",
    "minecraft:entity.chicken.ambient",
    "minecraft:entity.chicken.death",
    "minecraft:entity.chicken.egg",
    "minecraft:entity.chicken.hurt",
    "minecraft:entity.chicken.step",
    "minecraft:entity.cow.ambient",
    "minecraft:entity.cow.death",
    "minecraft:entity.cow.hurt",
    "minecraft:entity.cow.milk",
    "minecraft:entity.cow.step",
    "minecraft:entity.creeper.death",
    "minecraft:entity.creeper.hurt",
    "minecraft:entity.creeper.primed",
    "minecraft:entity.donkey.ambient",
    "minecraft:entity.donkey.angry",
    "minecraft:entity.donkey.chest",
    "minecraft:entity.donkey.death",
    "minecraft:entity.donkey.hurt",
    "minecraft:entity.egg.throw",
    "minecraft:entity.elder_guardian.ambient",
    "minecraft:entity.elder_guardian.ambient_land",
    "minecraft:entity.elder_guardian.curse",
    "minecraft:entity.elder_guardian.death",
    "minecraft:entity.elder_guardian.death_land",
    "minecraft:entity.elder_guardian.hurt",
    "minecraft:entity.elder_guardian.hurt_land",
    "minecraft:entity.enderdragon.ambient",
    "minecraft:entity.enderdragon.death",
    "minecraft:entity.enderdragon.flap",
    "minecraft:entity.enderdragon.growl",
    "minecraft:entity.enderdragon.hurt",
    "minecraft:entity.enderdragon.shoot",
    "minecraft:entity.enderdragon_fireball.explode",
    "minecraft:entity.endereye.launch",
    "minecraft:entity.endermen.ambient",
    "minecraft:entity.endermen.death",
    "minecraft:entity.endermen.hurt",
    "minecraft:entity.endermen.scream",
    "minecraft:entity.endermen.stare",
    "minecraft:entity.endermen.teleport",
    "minecraft:entity.endermite.ambient",
    "minecraft:entity.endermite.death",
    "minecraft:entity.endermite.hurt",
    "minecraft:entity.endermite.step",
    "minecraft:entity.enderpearl.throw",
    "minecraft:entity.experience_bottle.throw",
    "minecraft:entity.experience_orb.pickup",
    "minecraft:entity.experience_orb.touch",
    "minecraft:entity.firework.blast",
    "minecraft:entity.firework.blast_far",
    "minecraft:entity.firework.large_blast",
    "minecraft:entity.firework.large_blast_far",
    "minecraft:entity.firework.launch",
    "minecraft:entity.firework.shoot",
    "minecraft:entity.firework.twinkle",
    "minecraft:entity.firework.twinkle_far",
    "minecraft:entity.generic.big_fall",
    "minecraft:entity.generic.burn",
    "minecraft:entity.generic.death",
    "minecraft:entity.generic.drink",
    "minecraft:entity.generic.eat",
    "minecraft:entity.generic.explode",
    "minecraft:entity.generic.extinguish_fire",
    "minecraft:entity.generic.hurt",
    "minecraft:entity.generic.small_fall",
    "minecraft:entity.generic.splash",
    "minecraft:entity.generic.swim",
    "minecraft:entity.ghast.ambient",
    "minecraft:entity.ghast.death",
    "minecraft:entity.ghast.hurt",
    "minecraft:entity.ghast.scream",
    "minecraft:entity.ghast.shoot",
    "minecraft:entity.ghast.warn",
    "minecraft:entity.guardian.ambient",
    "minecraft:entity.guardian.ambient_land",
    "minecraft:entity.guardian.attack",
    "minecraft:entity.guardian.death",
    "minecraft:entity.guardian.death_land",
    "minecraft:entity.guardian.flop",
    "minecraft:entity.guardian.hurt",
    "minecraft:entity.guardian.hurt_land",
    "minecraft:entity.horse.ambient",
    "minecraft:entity.horse.angry",
    "minecraft:entity.horse.armor",
    "minecraft:entity.horse.breathe",
    "minecraft:entity.horse.death",
    "minecraft:entity.horse.eat",
    "minecraft:entity.horse.gallop",
    "minecraft:entity.horse.hurt",
    "minecraft:entity.horse.jump",
    "minecraft:entity.horse.land",
    "minecraft:entity.horse.saddle",
    "minecraft:entity.horse.step",
    "minecraft:entity.horse.step_wood",
    "minecraft:entity.hostile.big_fall",
    "minecraft:entity.hostile.death",
    "minecraft:entity.hostile.hurt",
    "minecraft:entity.hostile.small_fall",
    "minecraft:entity.hostile.splash",
    "minecraft:entity.hostile.swim",
    "minecraft:entity.husk.ambient",
    "minecraft:entity.husk.death",
    "minecraft:entity.husk.hurt",
    "minecraft:entity.husk.step",
    "minecraft:entity.irongolem.attack",
    "minecraft:entity.irongolem.death",
    "minecraft:entity.irongolem.hurt",
    "minecraft:entity.irongolem.step",
    "minecraft:entity.item.break",
    "minecraft:entity.item.pickup",
    "minecraft:entity.itemframe.add_item",
    "minecraft:entity.itemframe.break",
    "minecraft:entity.itemframe.place",
    "minecraft:entity.itemframe.remove_item",
    "minecraft:entity.itemframe.rotate_item",
    "minecraft:entity.leashknot.break",
    "minecraft:entity.leashknot.place",
    "minecraft:entity.lightning.impact",
    "minecraft:entity.lightning.thunder",
    "minecraft:entity.lingeringpotion.throw",
    "minecraft:entity.magmacube.death",
    "minecraft:entity.magmacube.hurt",
    "minecraft:entity.magmacube.jump",
    "minecraft:entity.magmacube.squish",
    "minecraft:entity.minecart.inside",
    "minecraft:entity.minecart.riding",
    "minecraft:entity.mooshroom.shear",
    "minecraft:entity.mule.ambient",
    "minecraft:entity.mule.death",
    "minecraft:entity.mule.hurt",
    "minecraft:entity.painting.break",
    "minecraft:entity.painting.place",
    "minecraft:entity.pig.ambient",
    "minecraft:entity.pig.death",
    "minecraft:entity.pig.hurt",
    "minecraft:entity.pig.saddle",
    "minecraft:entity.pig.step",
    "minecraft:entity.player.attack.crit",
    "minecraft:entity.player.attack.knockback",
    "minecraft:entity.player.attack.nodamage",
    "minecraft:entity.player.attack.strong",
    "minecraft:entity.player.attack.sweep",
    "minecraft:entity.player.attack.weak",
    "minecraft:entity.player.big_fall",
    "minecraft:entity.player.breath",
    "minecraft:entity.player.burp",
    "minecraft:entity.player.death",
    "minecraft:entity.player.hurt",
    "minecraft:entity.player.levelup",
    "minecraft:entity.player.small_fall",
    "minecraft:entity.player.splash",
    "minecraft:entity.player.swim",
    "minecraft:entity.polar_bear.ambient",
    "minecraft:entity.polar_bear.baby_ambient",
    "minecraft:entity.polar_bear.death",
    "minecraft:entity.polar_bear.hurt",
    "minecraft:entity.polar_bear.step",
    "minecraft:entity.polar_bear.warning",
    "minecraft:entity.rabbit.ambient",
    "minecraft:entity.rabbit.attack",
    "minecraft:entity.rabbit.death",
    "minecraft:entity.rabbit.hurt",
    "minecraft:entity.rabbit.jump",
    "minecraft:entity.sheep.ambient",
    "minecraft:entity.sheep.death",
    "minecraft:entity.sheep.hurt",
    "minecraft:entity.sheep.shear",
    "minecraft:entity.sheep.step",
    "minecraft:entity.shulker.ambient",
    "minecraft:entity.shulker.close",
    "minecraft:entity.shulker.death",
    "minecraft:entity.shulker.hurt",
    "minecraft:entity.shulker.hurt_closed",
    "minecraft:entity.shulker.open",
    "minecraft:entity.shulker.shoot",
    "minecraft:entity.shulker.teleport",
    "minecraft:entity.shulker_bullet.hit",
    "minecraft:entity.shulker_bullet.hurt",
    "minecraft:entity.silverfish.ambient",
    "minecraft:entity.silverfish.death",
    "minecraft:entity.silverfish.hurt",
    "minecraft:entity.silverfish.step",
    "minecraft:entity.skeleton.ambient",
    "minecraft:entity.skeleton.death",
    "minecraft:entity.skeleton.hurt",
    "minecraft:entity.skeleton.shoot",
    "minecraft:entity.skeleton.step",
    "minecraft:entity.skeleton_horse.ambient",
    "minecraft:entity.skeleton_horse.death",
    "minecraft:entity.skeleton_horse.hurt",
    "minecraft:entity.slime.attack",
    "minecraft:entity.slime.death",
    "minecraft:entity.slime.hurt",
    "minecraft:entity.slime.jump",
    "minecraft:entity.slime.squish",
    "minecraft:entity.small_magmacube.death",
    "minecraft:entity.small_magmacube.hurt",
    "minecraft:entity.small_magmacube.squish",
    "minecraft:entity.small_slime.death",
    "minecraft:entity.small_slime.hurt",
    "minecraft:entity.small_slime.jump",
    "minecraft:entity.small_slime.squish",
    "minecraft:entity.snowball.throw",
    "minecraft:entity.snowman.ambient",
    "minecraft:entity.snowman.death",
    "minecraft:entity.snowman.hurt",
    "minecraft:entity.snowman.shoot",
    "minecraft:entity.spider.ambient",
    "minecraft:entity.spider.death",
    "minecraft:entity.spider.hurt",
    "minecraft:entity.spider.step",
    "minecraft:entity.splash_potion.break",
    "minecraft:entity.splash_potion.throw",
    "minecraft:entity.squid.ambient",
    "minecraft:entity.squid.death",
    "minecraft:entity.squid.hurt",
    "minecraft:entity.stray.ambient",
    "minecraft:entity.stray.death",
    "minecraft:entity.stray.hurt",
    "minecraft:entity.stray.step",
    "minecraft:entity.tnt.primed",
    "minecraft:entity.villager.ambient",
    "minecraft:entity.villager.death",
    "minecraft:entity.villager.hurt",
    "minecraft:entity.villager.no",
    "minecraft:entity.villager.trading",
    "minecraft:entity.villager.yes",
    "minecraft:entity.witch.ambient",
    "minecraft:entity.witch.death",
    "minecraft:entity.witch.drink",
    "minecraft:entity.witch.hurt",
    "minecraft:entity.witch.throw",
    "minecraft:entity.wither.ambient",
    "minecraft:entity.wither.break_block",
    "minecraft:entity.wither.death",
    "minecraft:entity.wither.hurt",
    "minecraft:entity.wither.shoot",
    "minecraft:entity.wither.spawn",
    "minecraft:entity.wither_skeleton.ambient",
    "minecraft:entity.wither_skeleton.death",
    "minecraft:entity.wither_skeleton.hurt",
    "minecraft:entity.wither_skeleton.step",
    "minecraft:entity.wolf.ambient",
    "minecraft:entity.wolf.death",
    "minecraft:entity.wolf.growl",
    "minecraft:entity.wolf.howl",
    "minecraft:entity.wolf.hurt",
    "minecraft:entity.wolf.pant",
    "minecraft:entity.wolf.shake",
    "minecraft:entity.wolf.step",
    "minecraft:entity.wolf.whine",
    "minecraft:entity.zombie.ambient",
    "minecraft:entity.zombie.attack_door_wood",
    "minecraft:entity.zombie.attack_iron_door",
    "minecraft:entity.zombie.break_door_wood",
    "minecraft:entity.zombie.death",
    "minecraft:entity.zombie.hurt",
    "minecraft:entity.zombie.infect",
    "minecraft:entity.zombie.step",
    "minecraft:entity.zombie_horse.ambient",
    "minecraft:entity.zombie_horse.death",
    "minecraft:entity.zombie_horse.hurt",
    "minecraft:entity.zombie_pig.ambient",
    "minecraft:entity.zombie_pig.angry",
    "minecraft:entity.zombie_pig.death",
    "minecraft:entity.zombie_pig.hurt",
    "minecraft:entity.zombie_villager.ambient",
    "minecraft:entity.zombie_villager.converted",
    "minecraft:entity.zombie_villager.cure",
    "minecraft:entity.zombie_villager.death",
    "minecraft:entity.zombie_villager.hurt",
    "minecraft:entity.zombie_villager.step",
    "minecraft:item.armor.equip_chain",
    "minecraft:item.armor.equip_diamond",
    "minecraft:item.armor.equip_generic",
    "minecraft:item.armor.equip_gold",
    "minecraft:item.armor.equip_iron",
    "minecraft:item.armor.equip_leather",
    "minecraft:item.bottle.fill",
    "minecraft:item.bottle.fill_dragonbreath",
    "minecraft:item.bucket.empty",
    "minecraft:item.bucket.empty_lava",
    "minecraft:item.bucket.fill",
    "minecraft:item.bucket.fill_lava",
    "minecraft:item.chorus_fruit.teleport",
    "minecraft:item.elytra.flying",
    "minecraft:item.firecharge.use",
    "minecraft:item.flintandsteel.use",
    "minecraft:item.hoe.till",
    "minecraft:item.shield.block",
    "minecraft:item.shield.break",
    "minecraft:item.shovel.flatten",
    "minecraft:music.creative",
    "minecraft:music.credits",
    "minecraft:music.dragon",
    "minecraft:music.end",
    "minecraft:music.game",
    "minecraft:music.menu",
    "minecraft:music.nether",
    "minecraft:record.11",
    "minecraft:record.13",
    "minecraft:record.blocks",
    "minecraft:record.cat",
    "minecraft:record.chirp",
    "minecraft:record.far",
    "minecraft:record.mall",
    "minecraft:record.mellohi",
    "minecraft:record.stal",
    "minecraft:record.strad",
    "minecraft:record.wait",
    "minecraft:record.ward",
    "minecraft:ui.button.click",
    "minecraft:weather.rain",
    "minecraft:weather.rain.above"
        });
        public static readonly GSSSupportType ChatType = new GSSSupportType(16, "ChatType", new string[] { });
        public static readonly GSSSupportType ItemType = new GSSSupportType(17, "ItemType", new string[]{
            "NONE",
    "minecraft:acacia_boat",
    "minecraft:acacia_door",
    "minecraft:acacia_fence",
    "minecraft:acacia_fence_gate",
    "minecraft:acacia_stairs",
    "minecraft:activator_rail",
    "minecraft:anvil",
    "minecraft:apple",
    "minecraft:armor_stand",
    "minecraft:arrow",
    "minecraft:baked_potato",
    "minecraft:banner",
    "minecraft:barrier",
    "minecraft:beacon",
    "minecraft:bed",
    "minecraft:bedrock",
    "minecraft:beef",
    "minecraft:beetroot",
    "minecraft:beetroot_seeds",
    "minecraft:beetroot_soup",
    "minecraft:birch_boat",
    "minecraft:birch_door",
    "minecraft:birch_fence",
    "minecraft:birch_fence_gate",
    "minecraft:birch_stairs",
    "minecraft:blaze_powder",
    "minecraft:blaze_rod",
    "minecraft:boat",
    "minecraft:bone",
    "minecraft:bone_block",
    "minecraft:book",
    "minecraft:bookshelf",
    "minecraft:bow",
    "minecraft:bowl",
    "minecraft:bread",
    "minecraft:brewing_stand",
    "minecraft:brick",
    "minecraft:brick_block",
    "minecraft:brick_stairs",
    "minecraft:brown_mushroom",
    "minecraft:brown_mushroom_block",
    "minecraft:bucket",
    "minecraft:cactus",
    "minecraft:cake",
    "minecraft:carpet",
    "minecraft:carrot",
    "minecraft:carrot_on_a_stick",
    "minecraft:cauldron",
    "minecraft:chain_command_block",
    "minecraft:chainmail_boots",
    "minecraft:chainmail_chestplate",
    "minecraft:chainmail_helmet",
    "minecraft:chainmail_leggings",
    "minecraft:chest",
    "minecraft:chest_minecart",
    "minecraft:chicken",
    "minecraft:chorus_flower",
    "minecraft:chorus_fruit",
    "minecraft:chorus_fruit_popped",
    "minecraft:chorus_plant",
    "minecraft:clay",
    "minecraft:clay_ball",
    "minecraft:clock",
    "minecraft:coal",
    "minecraft:coal_block",
    "minecraft:coal_ore",
    "minecraft:cobblestone",
    "minecraft:cobblestone_wall",
    "minecraft:command_block",
    "minecraft:command_block_minecart",
    "minecraft:comparator",
    "minecraft:compass",
    "minecraft:cooked_beef",
    "minecraft:cooked_chicken",
    "minecraft:cooked_fish",
    "minecraft:cooked_mutton",
    "minecraft:cooked_porkchop",
    "minecraft:cooked_rabbit",
    "minecraft:cookie",
    "minecraft:crafting_table",
    "minecraft:dark_oak_boat",
    "minecraft:dark_oak_door",
    "minecraft:dark_oak_fence",
    "minecraft:dark_oak_fence_gate",
    "minecraft:dark_oak_stairs",
    "minecraft:daylight_detector",
    "minecraft:deadbush",
    "minecraft:detector_rail",
    "minecraft:diamond",
    "minecraft:diamond_axe",
    "minecraft:diamond_block",
    "minecraft:diamond_boots",
    "minecraft:diamond_chestplate",
    "minecraft:diamond_helmet",
    "minecraft:diamond_hoe",
    "minecraft:diamond_horse_armor",
    "minecraft:diamond_leggings",
    "minecraft:diamond_ore",
    "minecraft:diamond_pickaxe",
    "minecraft:diamond_shovel",
    "minecraft:diamond_sword",
    "minecraft:dirt",
    "minecraft:dispenser",
    "minecraft:double_plant",
    "minecraft:dragon_breath",
    "minecraft:dragon_egg",
    "minecraft:dropper",
    "minecraft:dye",
    "minecraft:egg",
    "minecraft:elytra",
    "minecraft:emerald",
    "minecraft:emerald_block",
    "minecraft:emerald_ore",
    "minecraft:enchanted_book",
    "minecraft:enchanting_table",
    "minecraft:end_bricks",
    "minecraft:end_crystal",
    "minecraft:end_portal_frame",
    "minecraft:end_rod",
    "minecraft:end_stone",
    "minecraft:ender_chest",
    "minecraft:ender_eye",
    "minecraft:ender_pearl",
    "minecraft:experience_bottle",
    "minecraft:farmland",
    "minecraft:feather",
    "minecraft:fence",
    "minecraft:fence_gate",
    "minecraft:fermented_spider_eye",
    "minecraft:filled_map",
    "minecraft:fire_charge",
    "minecraft:firework_charge",
    "minecraft:fireworks",
    "minecraft:fish",
    "minecraft:fishing_rod",
    "minecraft:flint",
    "minecraft:flint_and_steel",
    "minecraft:flower_pot",
    "minecraft:furnace",
    "minecraft:furnace_minecart",
    "minecraft:ghast_tear",
    "minecraft:glass",
    "minecraft:glass_bottle",
    "minecraft:glass_pane",
    "minecraft:glowstone",
    "minecraft:glowstone_dust",
    "minecraft:gold_block",
    "minecraft:gold_ingot",
    "minecraft:gold_nugget",
    "minecraft:gold_ore",
    "minecraft:golden_apple",
    "minecraft:golden_axe",
    "minecraft:golden_boots",
    "minecraft:golden_carrot",
    "minecraft:golden_chestplate",
    "minecraft:golden_helmet",
    "minecraft:golden_hoe",
    "minecraft:golden_horse_armor",
    "minecraft:golden_leggings",
    "minecraft:golden_pickaxe",
    "minecraft:golden_rail",
    "minecraft:golden_shovel",
    "minecraft:golden_sword",
    "minecraft:grass",
    "minecraft:grass_path",
    "minecraft:gravel",
    "minecraft:gunpowder",
    "minecraft:hardened_clay",
    "minecraft:hay_block",
    "minecraft:heavy_weighted_pressure_plate",
    "minecraft:hopper",
    "minecraft:hopper_minecart",
    "minecraft:ice",
    "minecraft:iron_axe",
    "minecraft:iron_bars",
    "minecraft:iron_block",
    "minecraft:iron_boots",
    "minecraft:iron_chestplate",
    "minecraft:iron_door",
    "minecraft:iron_helmet",
    "minecraft:iron_hoe",
    "minecraft:iron_horse_armor",
    "minecraft:iron_ingot",
    "minecraft:iron_leggings",
    "minecraft:iron_ore",
    "minecraft:iron_pickaxe",
    "minecraft:iron_shovel",
    "minecraft:iron_sword",
    "minecraft:iron_trapdoor",
    "minecraft:item_frame",
    "minecraft:jukebox",
    "minecraft:jungle_boat",
    "minecraft:jungle_door",
    "minecraft:jungle_fence",
    "minecraft:jungle_fence_gate",
    "minecraft:jungle_stairs",
    "minecraft:ladder",
    "minecraft:lapis_block",
    "minecraft:lapis_ore",
    "minecraft:lava_bucket",
    "minecraft:lead",
    "minecraft:leather",
    "minecraft:leather_boots",
    "minecraft:leather_chestplate",
    "minecraft:leather_helmet",
    "minecraft:leather_leggings",
    "minecraft:leaves",
    "minecraft:leaves2",
    "minecraft:lever",
    "minecraft:light_weighted_pressure_plate",
    "minecraft:lingering_potion",
    "minecraft:lit_pumpkin",
    "minecraft:log",
    "minecraft:log2",
    "minecraft:magma",
    "minecraft:magma_cream",
    "minecraft:map",
    "minecraft:melon",
    "minecraft:melon_block",
    "minecraft:melon_seeds",
    "minecraft:milk_bucket",
    "minecraft:minecart",
    "minecraft:mob_spawner",
    "minecraft:monster_egg",
    "minecraft:mossy_cobblestone",
    "minecraft:mushroom_stew",
    "minecraft:mutton",
    "minecraft:mycelium",
    "minecraft:name_tag",
    "minecraft:nether_brick",
    "minecraft:nether_brick_fence",
    "minecraft:nether_brick_stairs",
    "minecraft:nether_star",
    "minecraft:nether_wart",
    "minecraft:nether_wart_block",
    "minecraft:netherbrick",
    "minecraft:netherrack",
    "minecraft:noteblock",
    "minecraft:oak_stairs",
    "minecraft:obsidian",
    "minecraft:packed_ice",
    "minecraft:painting",
    "minecraft:paper",
    "minecraft:piston",
    "minecraft:planks",
    "minecraft:poisonous_potato",
    "minecraft:porkchop",
    "minecraft:potato",
    "minecraft:potion",
    "minecraft:prismarine",
    "minecraft:prismarine_crystals",
    "minecraft:prismarine_shard",
    "minecraft:pumpkin",
    "minecraft:pumpkin_pie",
    "minecraft:pumpkin_seeds",
    "minecraft:purpur_block",
    "minecraft:purpur_pillar",
    "minecraft:purpur_slab",
    "minecraft:purpur_stairs",
    "minecraft:quartz",
    "minecraft:quartz_block",
    "minecraft:quartz_ore",
    "minecraft:quartz_stairs",
    "minecraft:rabbit",
    "minecraft:rabbit_foot",
    "minecraft:rabbit_hide",
    "minecraft:rabbit_stew",
    "minecraft:rail",
    "minecraft:record_11",
    "minecraft:record_13",
    "minecraft:record_blocks",
    "minecraft:record_cat",
    "minecraft:record_chirp",
    "minecraft:record_far",
    "minecraft:record_mall",
    "minecraft:record_mellohi",
    "minecraft:record_stal",
    "minecraft:record_strad",
    "minecraft:record_wait",
    "minecraft:record_ward",
    "minecraft:red_flower",
    "minecraft:red_mushroom",
    "minecraft:red_mushroom_block",
    "minecraft:red_nether_brick",
    "minecraft:red_sandstone",
    "minecraft:red_sandstone_stairs",
    "minecraft:redstone",
    "minecraft:redstone_block",
    "minecraft:redstone_lamp",
    "minecraft:redstone_ore",
    "minecraft:redstone_torch",
    "minecraft:reeds",
    "minecraft:repeater",
    "minecraft:repeating_command_block",
    "minecraft:rotten_flesh",
    "minecraft:saddle",
    "minecraft:sand",
    "minecraft:sandstone",
    "minecraft:sandstone_stairs",
    "minecraft:sapling",
    "minecraft:sea_lantern",
    "minecraft:shears",
    "minecraft:shield",
    "minecraft:sign",
    "minecraft:skull",
    "minecraft:slime",
    "minecraft:slime_ball",
    "minecraft:snow",
    "minecraft:snow_layer",
    "minecraft:snowball",
    "minecraft:soul_sand",
    "minecraft:spawn_egg",
    "minecraft:speckled_melon",
    "minecraft:spectral_arrow",
    "minecraft:spider_eye",
    "minecraft:splash_potion",
    "minecraft:sponge",
    "minecraft:spruce_boat",
    "minecraft:spruce_door",
    "minecraft:spruce_fence",
    "minecraft:spruce_fence_gate",
    "minecraft:spruce_stairs",
    "minecraft:stained_glass",
    "minecraft:stained_glass_pane",
    "minecraft:stained_hardened_clay",
    "minecraft:stick",
    "minecraft:sticky_piston",
    "minecraft:stone",
    "minecraft:stone_axe",
    "minecraft:stone_brick_stairs",
    "minecraft:stone_button",
    "minecraft:stone_hoe",
    "minecraft:stone_pickaxe",
    "minecraft:stone_pressure_plate",
    "minecraft:stone_shovel",
    "minecraft:stone_slab",
    "minecraft:stone_slab2",
    "minecraft:stone_stairs",
    "minecraft:stone_sword",
    "minecraft:stonebrick",
    "minecraft:string",
    "minecraft:structure_block",
    "minecraft:structure_void",
    "minecraft:sugar",
    "minecraft:tallgrass",
    "minecraft:tipped_arrow",
    "minecraft:tnt",
    "minecraft:tnt_minecart",
    "minecraft:torch",
    "minecraft:trapdoor",
    "minecraft:trapped_chest",
    "minecraft:tripwire_hook",
    "minecraft:vine",
    "minecraft:water_bucket",
    "minecraft:waterlily",
    "minecraft:web",
    "minecraft:wheat",
    "minecraft:wheat_seeds",
    "minecraft:wooden_axe",
    "minecraft:wooden_button",
    "minecraft:wooden_door",
    "minecraft:wooden_hoe",
    "minecraft:wooden_pickaxe",
    "minecraft:wooden_pressure_plate",
    "minecraft:wooden_shovel",
    "minecraft:wooden_slab",
    "minecraft:wooden_sword",
    "minecraft:wool",
    "minecraft:writable_book",
    "minecraft:written_book",
    "minecraft:yellow_flower",
        });
        public static readonly GSSSupportType CoordinateSystem = new GSSSupportType(18, "CoordinateSystem", new string[] {
            "ABSOLUTE",
            "RELATIVE_CORDINATE",
            "RELATIVE_VIEWING",
            "RELATIVE_VIEWING_WITHOUT_Y",
            "RELATIVE_VIEWING_WITHOUT_HEIGHT",
        });
        //
        public readonly int value;
        public readonly String name;
        public readonly string[] kind;
        //
        public static GSSSupportType[] All()
        {
            return new GSSSupportType[]
            {
                GSSSupportType.BlockType,
                GSSSupportType.Boolean,
                GSSSupportType.Byte,
                GSSSupportType.Char,
                GSSSupportType.ChatType,
                GSSSupportType.Double,
                GSSSupportType.EntityType,
                GSSSupportType.Float,
                GSSSupportType.Integer,
                GSSSupportType.ItemType,
                GSSSupportType.Long,
                GSSSupportType.ParticleType,
                GSSSupportType.PotionEffectType,
                GSSSupportType.Short,
                GSSSupportType.SoundType,
                GSSSupportType.String,
                GSSSupportType.Text,
                GSSSupportType.Vector3d,
                GSSSupportType.CoordinateSystem,
            };
        }
        public static GSSSupportType getByName(string name)
        {
            var temp = All();
            foreach (var i in temp)
            {
                if (i.name.Equals(name))
                {
                    return i;
                }
            }
            return null;
        }
        public GSSSupportType(int value, string name)
        {
            this.value = value;
            this.name = name;
            this.kind = new string[] { };
        }
        public GSSSupportType(int value, string name, string[] kind)
        {
            this.value = value;
            this.name = name;
            this.kind = kind;
        }

        public GSSSupportType(string name, string[] kind)
        {
            this.value = 0;
            this.name = name;
            this.kind = kind;
        }

        public override String ToString()
        {
            return name;
        }
        public bool valid(string value)
        {
            //    GSSSupportType.Vector3d,
            if (this.kind.Length > 0)
            {
                return this.kind.Contains(value);
            }
            //
            if (this == Byte)
            {
                try
                {
                    System.Byte.Parse(value);
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else if (this == Char)
            {
                try
                {
                    System.Char.Parse(value);
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else if (this == Double)
            {
                try
                {
                    System.Double.Parse(value);
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else if (this == Float)
            {
                try
                {
                    System.Single.Parse(value);
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else if (this == Integer)
            {
                try
                {
                    System.Int32.Parse(value);
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else if (this == Long)
            {
                try
                {
                    System.Int64.Parse(value);
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else if (this == Short)
            {
                try
                {
                    System.Int16.Parse(value);
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else if (this == Vector3d)
            {
                return Regex.IsMatch(value, @"^\s*\[\s*[-+]?([0-9]*\.[0-9]+|[0-9]+|[0-9]+\.)\s*,\s*[-+]?([0-9]*\.[0-9]+|[0-9]+|[0-9]+\.)\s*,\s*[-+]?([0-9]*\.[0-9]+|[0-9]+|[0-9]+\.)\s*\]\s*$");
            }
            return true;
        }

        public string Stringify(string value)
        {
            //    GSSSupportType.Vector3d,
            if (this.kind.Length > 0)
            {
                return System.String.Format("\"{0}\"", value);
            }
            //
            if (this.Equals(Byte))
            {
                return value;
            }
            else if (this.Equals(Char))
            {
                return value;
            }
            else if (this.Equals(Double))
            {
                return value;
            }
            else if (this.Equals(Float))
            {
                return value;
            }
            else if (this.Equals(Integer))
            {
                if (value.StartsWith("#"))
                {
                    return "\"" + value + "\"";
                }
                return value;
            }
            else if (this.Equals(Long))
            {
                return value;
            }
            else if (this.Equals(Short))
            {
                return value;
            }
            else if (this.Equals(Vector3d))
            {
                return value;
            }
            else if (this.Equals(Text))
            {
                return value;
            }
            else if (this.Equals(String))
            {
                return "\"" + value + "\"";
            }

            return "Unknown";
        }
        public object toObject(string value)
        {
            //    GSSSupportType.Vector3d,
            if (this.kind.Length > 0 && this != Boolean)
            {
                return value;
            }
            //
            if (this.Equals(Boolean))
            {
                return System.Boolean.Parse(value);
            }
            else if (this.Equals(Byte))
            {
                return System.Byte.Parse(value);
            }
            else if (this.Equals(Char))
            {
                return System.Char.Parse(value);
            }
            else if (this.Equals(Double))
            {
                return System.Double.Parse(value);
            }
            else if (this.Equals(Float))
            {
                return System.Single.Parse(value);
            }
            else if (this.Equals(Integer))
            {
                if (value.StartsWith("#"))
                {
                    return value;
                }
                return System.Int32.Parse(value);
            }
            else if (this.Equals(Long))
            {
                return System.Int64.Parse(value);
            }
            else if (this.Equals(Short))
            {
                return System.Int16.Parse(value);
            }
            else if (this.Equals(Vector3d))
            {
                var each = value.Split('[')[1].Split(']')[0].Split(',');
                double[] temp = new double[3];
                for (int i = 0; i < 3; i++)
                {
                    temp[i] = System.Double.Parse(each[i]);
                }
                return temp;
            }
            else if (this.Equals(Text))
            {
                return value;
            }
            else if (this.Equals(String))
            {
                return value;
            }
            return null;
        }
        public override bool Equals(object obj)
        {
            if (obj is GSSSupportType)
            {
                GSSSupportType gssfe = obj as GSSSupportType;
                return gssfe.name.Equals(this.name);
            }
            return false;
        }
    }
}
