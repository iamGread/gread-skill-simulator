package gmail.ksh9345.gss.sponge.config;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class ConfigSimulator {
    public static String getMainModuleName() {
        return MainModuleName;
    }
    public int getOneTickSet() {
        return OneTickSet;
    }
    public synchronized void setOneTickSet(int oneTickSet) {
        OneTickSet = oneTickSet;
    }
    public int getTaskWorker() {
        return TaskWorker;
    }
    public synchronized void setTaskWorker(int taskWorker) {
        TaskWorker = taskWorker;
    }
    public int getRefreshInterval() {
        return RefreshInterval;
    }
    public synchronized void setRefreshInterval(int refreshInterval) {
        RefreshInterval = refreshInterval;
    }

    public int getDispenceAtTime() {
        return DispenceAtTime;
    }

    public synchronized void setDispenceAtTime(int dispenceAtTime) {
        DispenceAtTime = dispenceAtTime;
    }

    public int getActionTick() {
        return ActionTick;
    }

    public synchronized void setActionTick(int actionTick) {
        ActionTick = actionTick;
    }

    //
    private final static String MainModuleName = "GSS - Simulator Task";
    private int OneTickSet = 1;
    private int TaskWorker = 8;
    private int ActionTick = 1;
    private int RefreshInterval = 16;
    private int DispenceAtTime = 8;
    //
    private boolean ExceptionCheckingMode = true;

}
