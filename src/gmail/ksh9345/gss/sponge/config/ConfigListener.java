package gmail.ksh9345.gss.sponge.config;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class ConfigListener {
    public long getDefaultReserveSystemCheck() {
        return DefaultReserveSystemCheck;
    }

    public synchronized void setDefaultReserveSystemCheck(long defaultReserveSystemCheck) {
        DefaultReserveSystemCheck = defaultReserveSystemCheck;
    }

    //===================================================================================

    public String getMainModuleName() {
        return MainModuleName;
    }

    public boolean isAutoUsingEventTurnOn() {
        return autoUsingEventTurnOn;
    }

    public synchronized void setAutoUsingEventTurnOn(boolean autoUsingEventTurnOn) {
        this.autoUsingEventTurnOn = autoUsingEventTurnOn;
    }

    public boolean isOnGroundEvent() {
        return onGroundEvent;
    }

    public synchronized void setOnGroundEvent(boolean onGroundEvent) {
        this.onGroundEvent = onGroundEvent;
    }

    public boolean isOnMidairEvent() {
        return onMidairEvent;
    }

    public synchronized void setOnMidairEvent(boolean onMidairEvent) {
        this.onMidairEvent = onMidairEvent;
    }

    public boolean isOnFixEvent() {
        return onFixEvent;
    }

    public synchronized void setOnFixEvent(boolean onFixEvent) {
        this.onFixEvent = onFixEvent;
    }

    public boolean isOnDropByQEvent() {
        return onDropByQEvent;
    }

    public synchronized void setOnDropByQEvent(boolean onDropByQEvent) {
        this.onDropByQEvent = onDropByQEvent;
    }

    public boolean isOnMove() {
        return onMove;
    }

    public synchronized void setOnMove(boolean onMove) {
        this.onMove = onMove;
    }

    public boolean isOnAttack() {
        return onAttack;
    }

    public synchronized void setOnAttack(boolean onAttack) {
        this.onAttack = onAttack;
    }

    public boolean isOnEnter() {
        return onEnter;
    }

    public synchronized void setOnEnter(boolean onEnter) {
        this.onEnter = onEnter;
    }

    public boolean isOnInteract() {
        return onInteract;
    }

    public synchronized void setOnInteract(boolean onInteract) {
        this.onInteract = onInteract;
    }

    public long getDefaultGCListenerHelper() {
        return DefaultGCListenerHelper;
    }

    public synchronized void setDefaultGCListenerHelper(long defaultGCListenerHelper) {
        DefaultGCListenerHelper = defaultGCListenerHelper;
    }

    //===================================================================================
    // 이벤트 시뮬레이터 관련
    // 이벤트 시뮬레이터 관련 기본 설정값
    private final String MainModuleName = "GSS - Listener Task";
    // 내부 이벤트 핸들링 확인 시간
    private long DefaultReserveSystemCheck = 60;
    // 내부 가비지 컬렉팅 시간
    private long DefaultGCListenerHelper = 1000;
    private boolean autoUsingEventTurnOn;
    //
    private boolean turnOffThrow;

    /////////////////////////////////
    // 이벤트 관리
    private boolean onGroundEvent;
    private boolean onMidairEvent;
    private boolean onFixEvent;
    private boolean onDropByQEvent;
    //
    private boolean onMove;
    private boolean onAttack;
    private boolean onEnter;
    private boolean onInteract;
    /////////////////////////////////
}
