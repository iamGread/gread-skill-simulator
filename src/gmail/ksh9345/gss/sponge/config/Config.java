package gmail.ksh9345.gss.sponge.config;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class Config {
    public boolean isAutoInitialize() {
        return autoInitialize;
    }
    public synchronized void setAutoInitialize(boolean autoInitialize) {
        this.autoInitialize = autoInitialize;
    }
    public boolean isDebugingAllow() {
        return debugingAllow;
    }
    public synchronized void setDebugingAllow(boolean debugingAllow) {
        this.debugingAllow = debugingAllow;
    }

    //====================================================================================
    // gss 전체 설정
    // 서버 시작과 함게 시뮬레이터 시작
    private boolean autoInitialize = true;
    private boolean debugingAllow = false;
    //
    //====================================================================================
    public ConfigSimulator configSimulator = new ConfigSimulator();
    public ConfigListener configListener = new ConfigListener();
}
