package gmail.ksh9345.gss.sponge;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.control.IfSkillPositionBlockType;
import gmail.ksh9345.gss.core.system.VectorRotationSystem;
import org.spongepowered.api.block.BlockSoundGroup;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.trait.BlockTrait;
import org.spongepowered.api.data.Property;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.text.translation.Translation;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by ksh93 on 2017-02-05.
 */
public class test {
    public static void main(String[]args){
        Vector3d temp1 = new Vector3d(45, 135, 0);

        System.out.println(VectorRotationSystem.rotationToNormalVector(temp1));
        System.out.println(VectorRotationSystem.vectorToRotation(VectorRotationSystem.rotationToNormalVector(temp1)));

    }
}

