package gmail.ksh9345.gss.sponge.causes;

import gmail.ksh9345.gss.sponge.PEP;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.event.cause.Cause;

/**
 * Created by ksh93 on 2017-02-08.
 */
public class GSSBlocks {
    public static Cause revert(Living l){
        return Cause
                .source(PEP.pluginContainer)
                .owner(l)
                .named("GSS Revert", true)
                .build();
    }
//    public static Cause moveFrom(Living l, Location<World> to){
//        return Cause
//                .source(PEP.pluginContainer)
//                .owner(l)
//                .named("GSS Move From", true)
//                .named("to", to.copyForSubskill())
//                .build();
//    }
//    public static Cause moveTo(Living l, Location<World> from){
//        return Cause
//                .source(PEP.pluginContainer)
//                .owner(l)
//                .named("GSS Move To", true)
//                .named("from", from.copyForSubskill())
//                .build();
//    }
    public static Cause place(Living l){
        return Cause
                .source(PEP.pluginContainer)
                .owner(l)
                .named("GSS place", true)
                .build();
    }
}
