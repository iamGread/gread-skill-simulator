package gmail.ksh9345.gss.sponge;

/**
 * Created by ksh93 on 2017-02-06.
 */
public class Permission {
    public static String GSS = "GSS";
    //
    public static String ADMIN = GSS + ".admin";
    public static String Simulator = GSS + ".simulator";
    public static String Execute = GSS + ".execute";
    public static String Model = GSS + ".model";
    public static String Model_ADVANCED = Model + ".model_adv";

    public static String SkillOption = GSS + ".skillOption";
}
