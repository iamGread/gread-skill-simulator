package gmail.ksh9345.gss.sponge;

import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColor;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class Utils {
    public static Text printMessage(String title, TextColor tc, String message) {
        return (Text.join(
                Text.of("[ "),
                Text.builder(title).color(tc).build(),
                Text.of(" ] : " + message)
        ));
    }

}
