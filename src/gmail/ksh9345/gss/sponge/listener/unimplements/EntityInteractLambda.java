package gmail.ksh9345.gss.sponge.listener.unimplements;

import org.spongepowered.api.entity.Entity;

/**
 * Created by ksh93 on 2017-02-07.
 */
@FunctionalInterface
public interface EntityInteractLambda {
    public boolean run(Entity from, Entity to);
}
