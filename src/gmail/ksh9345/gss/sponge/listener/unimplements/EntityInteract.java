package gmail.ksh9345.gss.sponge.listener.unimplements;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.entity.InteractEntityEvent;

import java.util.Optional;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class EntityInteract implements EventListener<InteractEntityEvent> {
    EntityInteractLambda eil;

    public EntityInteract(EntityInteractLambda eil) {
        this.eil = eil;
    }

    @Override
    public void handle(InteractEntityEvent interactEntityEvent) throws Exception {
        Optional<Entity> e = interactEntityEvent.getCause().first(Entity.class);
        if (e.isPresent()){
            interactEntityEvent.setCancelled(eil.run(e.get(), interactEntityEvent.getTargetEntity()));
        }

    }
}
