package gmail.ksh9345.gss.sponge.listener;

import gmail.ksh9345.gss.lambdas.EntityBooleanLambda;
import gmail.ksh9345.gss.lambdas.EntityLambda;
import gmail.ksh9345.gss.sponge.CustomKeys;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.util.blockray.BlockRay;
import org.spongepowered.api.world.World;

import java.util.HashMap;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class MidairMovement implements EventListener<MoveEntityEvent> {
    public EntityLambda midair_sneak;
    public EntityBooleanLambda midair_jump;

    public MidairMovement(EntityLambda midair_sneak, EntityBooleanLambda midair_jump) {
        this.midair_sneak = midair_sneak;
        this.midair_jump = midair_jump;
    }

    @Override
    public void handle(MoveEntityEvent moveEntityEvent) throws Exception {
        Entity e = moveEntityEvent.getTargetEntity();
        if (e instanceof Player){
            Player p = (Player) e;
            // 땅에 있을 때
            if (p.isOnGround()){
                p.offer(Keys.CAN_FLY, true);
                p.offer(CustomKeys.CAN_MIDAIR_SNEAK, true);
            }
            // 땅에 없을 때
            else {
                if (p.get(Keys.IS_FLYING).orElse(false)){
                    // 크리에이티브 모드도 발동시 기본 비행은 취소
                    midair_jump.run(p);
                    p.offer(Keys.IS_FLYING, false);
                    p.offer(Keys.CAN_FLY, false);
                    //
                    if (p.gameMode().get().equals(GameModes.CREATIVE)){
                        p.offer(Keys.IS_FLYING, true);
                        p.offer(Keys.CAN_FLY, true);
                    }

                }

                if (p.get(Keys.IS_SNEAKING).orElse(false) && p.get(CustomKeys.CAN_MIDAIR_SNEAK).orElse(true)){
                    midair_sneak.run(p);
                    p.offer(CustomKeys.CAN_MIDAIR_SNEAK, false);
                }
            }
        }
    }
}

