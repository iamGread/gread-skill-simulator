package gmail.ksh9345.gss.sponge.listener;

import gmail.ksh9345.gss.lambdas.EntityLambda;
import gmail.ksh9345.gss.sponge.CustomKeys;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.entity.MoveEntityEvent;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class GroundMovement implements EventListener<MoveEntityEvent>{
    public EntityLambda groundsneak;
    public EntityLambda groundmove;

    public GroundMovement(EntityLambda groundsneak, EntityLambda groundmove) {
        this.groundsneak = groundsneak;
        this.groundmove = groundmove;
    }

    @Override
    public void handle(MoveEntityEvent moveEntityEvent) throws Exception {
        Entity e = moveEntityEvent.getTargetEntity();
        if (e instanceof Living){
            if (e.isOnGround()){
                // ==================================================================================
                Living l = (Living) e;
                boolean issneak = l.get(Keys.IS_SNEAKING).orElse(false);
                boolean cansneak = l.get(CustomKeys.CAN_GROUND_SNEAK).orElse(true);
                if (issneak && cansneak){
                    groundsneak.run(l);
                    l.offer(CustomKeys.CAN_GROUND_SNEAK, false);
                }else if (!issneak & !cansneak){
                    l.offer(CustomKeys.CAN_GROUND_SNEAK, true);
                }
                // ==================================================================================
                groundmove.run(l);
                // ==================================================================================
            }
        }
    }
}
