package gmail.ksh9345.gss.sponge.listener;

import gmail.ksh9345.gss.lambdas.EntityBooleanLambda;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.world.World;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class PreventMovement implements EventListener<MoveEntityEvent>{
    EntityBooleanLambda fix;
    EntityBooleanLambda stop;

    public PreventMovement(EntityBooleanLambda fix, EntityBooleanLambda stop) {
        this.fix = fix;
        this.stop = stop;
    }

    @Override
    public void handle(MoveEntityEvent moveEntityEvent) throws Exception {
        Entity temp = moveEntityEvent.getTargetEntity();
        Transform<World> from = moveEntityEvent.getFromTransform();
        //
        if (fix.run(temp)){
            moveEntityEvent.setToTransform(from);
        }
        if (stop.run(temp)){
            from.setRotation(moveEntityEvent.getToTransform().getRotationAsQuaternion());
            moveEntityEvent.setToTransform(from);
        }
    }
}
