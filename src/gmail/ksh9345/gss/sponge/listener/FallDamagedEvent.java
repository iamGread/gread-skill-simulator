package gmail.ksh9345.gss.sponge.listener;

import gmail.ksh9345.gss.lambdas.EntityBooleanLambda;
import gmail.ksh9345.gss.lambdas.EntityLambda;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.cause.entity.damage.source.DamageSource;
import org.spongepowered.api.event.cause.entity.damage.source.DamageSources;
import org.spongepowered.api.event.entity.DamageEntityEvent;

/**
 * Created by ksh93 on 2017-02-08.
 */
public class FallDamagedEvent implements EventListener<DamageEntityEvent>{
    EntityBooleanLambda isnofalldamage;
    EntityLambda waitFallDamage;

    public FallDamagedEvent(EntityBooleanLambda isnofalldamage, EntityLambda waitFallDamage) {
        this.isnofalldamage = isnofalldamage;
        this.waitFallDamage = waitFallDamage;
    }

    @Override
    public void handle(DamageEntityEvent damageEntityEvent) throws Exception {
        DamageSource temp = damageEntityEvent.getCause().first(DamageSource.class).orElse(null);
        boolean iscancel = false;
        if (temp.equals(DamageSources.FALLING)){
            waitFallDamage.run(damageEntityEvent.getTargetEntity());
            iscancel = isnofalldamage.run(damageEntityEvent.getTargetEntity());
        }
        damageEntityEvent.setCancelled(iscancel);
    }
}
