package gmail.ksh9345.gss.sponge.listener;

import com.google.common.collect.Lists;
import gmail.ksh9345.gss.lambdas.EntityEntitysLambda;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.cause.entity.damage.source.DamageSource;
import org.spongepowered.api.event.cause.entity.damage.source.DamageSources;
import org.spongepowered.api.event.entity.DamageEntityEvent;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class EntityHitEvent implements EventListener<DamageEntityEvent>{
    EntityEntitysLambda attacking;
    EntityEntitysLambda attacked;

    public EntityHitEvent(EntityEntitysLambda attacking, EntityEntitysLambda attacked) {
        this.attacking = attacking;
        this.attacked = attacked;
    }

    @Override
    public void handle(DamageEntityEvent damageEntityEvent) throws Exception {
        DamageSource temp = damageEntityEvent.getCause().first(DamageSource.class).orElse(null);
        if (temp.equals(DamageSources.GENERIC)){
            Living attacker;
            Living attackee;
            try {
                attacker = damageEntityEvent.getCause().first(Living.class).orElse(null);
                attackee = (Living) damageEntityEvent.getTargetEntity();
            }catch (Exception e){
                e.printStackTrace();
                return;
            }
            attacked.run(attackee, Lists.newArrayList(attacker));
            attacked.run(attacker, Lists.newArrayList(attackee));
        }
    }
}
