package gmail.ksh9345.gss.sponge.listener;

import gmail.ksh9345.gss.lambdas.EntityObjectsLambda;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * Created by ksh93 on 2017-02-08.
 */
public class LocationalEvent implements EventListener<MoveEntityEvent> {
    EntityObjectsLambda enter;
    EntityObjectsLambda speed;

    public LocationalEvent(EntityObjectsLambda enter, EntityObjectsLambda speed) {
        this.enter = enter;
        this.speed = speed;
    }

    @Override
    public void handle(MoveEntityEvent moveEntityEvent) throws Exception {
        Entity e = moveEntityEvent.getTargetEntity();
        if (e instanceof Living){
            Living l = (Living) e;
            Location<World> tolw = moveEntityEvent.getToTransform().getLocation();
            enter.run(l, tolw);
            speed.run(l, e.getVelocity());
        }
    }
}
