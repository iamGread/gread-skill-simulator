package gmail.ksh9345.gss.sponge.listener;

import gmail.ksh9345.gss.lambdas.EntityBooleanLambda;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.cause.entity.spawn.EntitySpawnCause;
import org.spongepowered.api.event.item.inventory.DropItemEvent;

import java.util.Optional;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class ItemDropEvent implements EventListener<DropItemEvent.Dispense>{
    EntityBooleanLambda qpl;

    public ItemDropEvent(EntityBooleanLambda qpl) {
        this.qpl = qpl;
    }

    @Override
    public void handle(DropItemEvent.Dispense dispense) throws Exception {
        Optional<EntitySpawnCause> temp1 = dispense.getCause().first(EntitySpawnCause.class);
        if (temp1.isPresent()){
            Entity temp2 = temp1.get().getEntity();
            if (temp2 instanceof Player){
                dispense.setCancelled(qpl.run(temp2));
            }

        }
    }
}
