package gmail.ksh9345.gss.sponge.command;


import gmail.ksh9345.gss.sponge.PEP;
import gmail.ksh9345.gss.sponge.Permission;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.effect.particle.ParticleType;
import org.spongepowered.api.effect.potion.PotionEffectType;
import org.spongepowered.api.effect.sound.SoundType;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.chat.ChatType;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.blockray.BlockRay;
import org.spongepowered.api.util.blockray.BlockRayHit;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.extent.EntityUniverse;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;

/**
 * Created by ksh93 on 2017-01-31.
 */
public class gss_tool {
    public enum Mode {
        GAMEINFO,
        VIEWING,
        TYPEPRINT,
    }

    private static void gameinfo(CommandSource commandSource, CommandContext commandContext) {
        if (commandSource instanceof Player) {
            //======================================================================================================
            commandSource.sendMessage(
                    Text.join(
                            Text.builder("[").build(),
                            Text.builder("Position").color(TextColors.GREEN).build(),
                            Text.builder("]").build()
                    ));
            commandSource.sendMessage(
                    Text.join(
                            Text.builder("    - Biome : ").build(),
                            Text.builder(((Player) commandSource).getLocation().getBiome().getName()).build()
                    ));
            commandSource.sendMessage(
                    Text.join(
                            Text.builder("    - Humidity : ").build(),
                            Text.builder(Double.toString(((Player) commandSource).getLocation().getBiome().getHumidity())).build()
                    ));
            commandSource.sendMessage(
                    Text.join(
                            Text.builder("    - Temperature : ").build(),
                            Text.builder(Double.toString(((Player) commandSource).getLocation().getBiome().getTemperature())).build()
                    ));
            commandSource.sendMessage(
                    Text.join(
                            Text.builder("    - Position : ").build(),
                            Text.builder(((Player) commandSource).getLocation().getPosition().toString()).build()
                    ));
            //======================================================================================================
            commandSource.sendMessage(
                    Text.join(
                            Text.builder("[").build(),
                            Text.builder("Rotation").color(TextColors.GREEN).build(),
                            Text.builder("]").build()
                    ));

            commandSource.sendMessage(
                    Text.join(
                            Text.builder("    - Psi : ").build(),
                            Text.builder(Double.toString(((Player) commandSource).getRotation().getX())).build(),
                            Text.builder(" Deg").color(TextColors.BLUE).build()
                    ));
            commandSource.sendMessage(
                    Text.join(
                            Text.builder("    - Theta : ").build(),
                            Text.builder(Double.toString(((Player) commandSource).getRotation().getY())).build(),
                            Text.builder(" Deg").color(TextColors.BLUE).build()
                    ));
            //======================================================================================================
            commandSource.sendMessage(
                    Text.join(
                            Text.builder("[").build(),
                            Text.builder("HeadRotation").color(TextColors.GREEN).build(),
                            Text.builder("]").build()
                    ));
            commandSource.sendMessage(
                    Text.join(
                            Text.builder("    - Psi : ").build(),
                            Text.builder(Double.toString(((Player) commandSource).getHeadRotation().getX())).build(),
                            Text.builder(" Deg").color(TextColors.BLUE).build()
                    ));
            commandSource.sendMessage(
                    Text.join(
                            Text.builder("    - Theta : ").build(),
                            Text.builder(Double.toString(((Player) commandSource).getHeadRotation().getY())).build(),
                            Text.builder(" Deg").color(TextColors.BLUE).build()
                    ));
        } else {
            commandSource.sendMessage(
                    Text.join(
                            Text.builder("[").build(),
                            Text.builder("Error").color(TextColors.RED).build(),
                            Text.builder("] : Only Player can use this").build()
                    ));
        }
    }

    private static void viewing(CommandSource commandSource, CommandContext commandContext) {
        if (commandSource instanceof Player) {
            commandSource.sendMessage(
                    Text.join(
                            Text.builder("[").build(),
                            Text.builder("Entitys").color(TextColors.GREEN).build(),
                            Text.builder("]").build()
                    ));
            Set<EntityUniverse.EntityHit> set = ((Player) commandSource).getWorld().getIntersectingEntities((Player) commandSource, 64);
            for (EntityUniverse.EntityHit eh : set) {
                commandSource.sendMessage(
                        Text.join(
                                Text.builder("    - ").build(),
                                Text.builder(eh.getEntity()).build(),
                                Text.builder(" : ").build(),
                                Text.builder(Double.toString(eh.getDistance())).build()

                        ));
            }
            commandSource.sendMessage(
                    Text.join(
                            Text.builder("[").build(),
                            Text.builder("Block").color(TextColors.GREEN).build(),
                            Text.builder("]").build()
                    ));
            BlockRay<World> br = BlockRay
                    .from((Entity) commandSource)
                    .distanceLimit(128)
                    .skipFilter(BlockRay.continueAfterFilter(BlockRay.onlyAirFilter(), 1))
                    .build();
            Optional<BlockRayHit<World>> hitOpt = br.end();
            if (hitOpt.isPresent()) {
                BlockRayHit<World> hit = hitOpt.get();
                commandSource.sendMessage(
                        Text.join(
                                Text.builder("    - ").build(),
                                Text.builder(hit.getPosition().toString()).build(),
                                Text.builder(" : ").build(),
                                Text.builder(hit.getLocation().getBlockType()).build()
                        ));


            } else {
                commandSource.sendMessage(
                        Text.join(
                                Text.builder("[").build(),
                                Text.builder("Error").color(TextColors.RED).build(),
                                Text.builder("] : Only Player can use this").build()
                        ));
            }
        }
    }
    private static void typeprint(CommandSource commandSource, CommandContext commandContext){
        Path p = Paths.get(PEP.game.getGameDirectory().toAbsolutePath().toString(), "typelist.txt");
        commandSource.sendMessage(Text.join(
                Text.of("[ "),
                Text.builder("GSS Toolkit").color(TextColors.GREEN).build(),
                Text.of(" ] : Typelist print at '" + p.toString() + "'")
        ));

        FileWriter fw = null;
        try {
            Files.deleteIfExists(p);
            Files.createFile(p);
            fw = new FileWriter(p.toFile());
            //=========================================================================================
            fw.write("<org.spongepowered.api.entity.EntityType>\n");
            for (EntityType tp :PEP.game.getRegistry().getAllOf(EntityType.class)){
                fw.write(String.format("id : %-40s , name : %-40s\n", tp.getId(), tp.getName()));
            }
            fw.write("\n");
            //=========================================================================================
            fw.write("<org.spongepowered.api.effect.potion.PotionEffectType>\n");
            for (PotionEffectType tp :PEP.game.getRegistry().getAllOf(PotionEffectType.class)){
                fw.write(String.format("id : %-40s , name : %-40s\n", tp.getId(), tp.getName()));
            }
            fw.write("\n");
            //=========================================================================================
            fw.write("<org.spongepowered.api.block.BlockType>\n");
            for (BlockType tp :PEP.game.getRegistry().getAllOf(BlockType.class)){
                fw.write(String.format("id : %-40s , name : %-40s\n", tp.getId(), tp.getName()));
            }
            fw.write("\n");
            //=========================================================================================
            fw.write("<org.spongepowered.api.effect.particle.ParticleType>\n");
            for (ParticleType tp :PEP.game.getRegistry().getAllOf(ParticleType.class)){
                fw.write(String.format("id : %-40s , name : %-40s\n", tp.getId(), tp.getName()));
            }
            fw.write("\n");
            //=========================================================================================
            fw.write("<org.spongepowered.api.effect.sound.SoundType>\n");
            for (SoundType tp :PEP.game.getRegistry().getAllOf(SoundType.class)){
                fw.write(String.format("id : %-40s , name : %-40s\n", tp.getId(), tp.getName()));
            }
            fw.write("\n");
            //=========================================================================================
            fw.write("<org.spongepowered.api.text.chat.ChatType>\n");
            for (ChatType tp :PEP.game.getRegistry().getAllOf(ChatType.class)){
                fw.write(String.format("id : %-40s , name : %-40s\n", tp.getId(), tp.getName()));
            }
            //=========================================================================================
            fw.write("<org.spongepowered.api.item.ItemType>\n");
            for (ItemType tp :PEP.game.getRegistry().getAllOf(ItemType.class)){
                fw.write(String.format("id : %-40s , name : %-40s\n", tp.getId(), tp.getName()));
            }

            fw.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    public static CommandSpec cs =
            CommandSpec
                    .builder()
                    .permission(Permission.ADMIN)
                    .arguments(GenericArguments.enumValue(Text.of("mode"), Mode.class))
                    .executor(new CommandExecutor() {
                        @Override
                        public CommandResult execute(CommandSource commandSource, CommandContext commandContext) throws CommandException {

                            switch (((Mode) commandContext.getOne(Text.of("mode")).get())) {
                                default:
                                case GAMEINFO:
                                    gss_tool.gameinfo(commandSource, commandContext);
                                    break;
                                case VIEWING:
                                    gss_tool.viewing(commandSource, commandContext);
                                case TYPEPRINT:
                                    gss_tool.typeprint(commandSource, commandContext);
                                    break;

                            }

                            //
                            return CommandResult
                                    .builder()
                                    .successCount(1)
                                    .affectedBlocks(0)
                                    .affectedEntities(0)
                                    .affectedItems(0)
                                    .build();
                        }
                    })
                    .build();
}
