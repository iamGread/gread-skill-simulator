package gmail.ksh9345.gss.sponge.command;

import gmail.ksh9345.gss.core.Errors.NoPermisionForSkillError;
import gmail.ksh9345.gss.core.skill.Model;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.sponge.PEP;
import gmail.ksh9345.gss.sponge.Utils;
import org.slf4j.helpers.Util;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by ksh93 on 2017-01-30.
 */
public class gss_do {

    public static CommandSpec cs =
            CommandSpec
                    .builder()
                    .description(Text.of("gss(GreadSkillSimulator)"))
                    .arguments(
                            GenericArguments.onlyOne(GenericArguments.playerOrSource(Text.of("caster"))),
                            new SkillCommandArguments(Text.of("skillname"))
                    )
                    .executor(new CommandExecutor() {
                        @Override
                        public CommandResult execute(CommandSource commandSource, CommandContext commandContext) throws CommandException {
                            Player pl = (Player) commandContext.getOne(Text.of("caster")).get();
                            Optional<Model> sm = commandContext.getOne(Text.of("skillname"));
                            if (sm.isPresent()){
                                try {
                                    PEP.simulatorMultiTask.Request(new Skill(sm.get(), pl));
                                }catch (NoPermisionForSkillError e){
                                    PEP.consoleSource.sendMessage(Utils.printMessage("GSS Do", TextColors.RED, "Do Fail"));
                                    e.printStackTrace();
                                }

                                //
                                return CommandResult
                                        .builder()
                                        .successCount(1)
                                        .affectedBlocks(0)
                                        .affectedEntities(0)
                                        .affectedItems(0)
                                        .build();
                            }else {
                                commandSource.sendMessage(Utils.printMessage("Error", TextColors.RED, "There is no Skill that name"));
                                return CommandResult
                                        .builder()
                                        .successCount(0)
                                        .affectedBlocks(0)
                                        .affectedEntities(0)
                                        .affectedItems(0)
                                        .build();
                            }

                        }
                    })
                    .build();
}
