package gmail.ksh9345.gss.sponge.command;

import gmail.ksh9345.gss.core.loader.Container;
import gmail.ksh9345.gss.core.skill.Model;
import gmail.ksh9345.gss.sponge.Permission;
import gmail.ksh9345.gss.sponge.Utils;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.lang.reflect.Field;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class gss_model {
    public enum Mode {
        BASIC,
        ADVANCED
    }

    private static void info(Mode mode, CommandSource cs, Model model) {
        switch (mode) {
            default:
            case BASIC:
                cs.sendMessage(Utils.printMessage(model.getNameOrigin(), TextColors.GREEN, model.getDescription()));
                break;
            case ADVANCED:
                cs.sendMessage(Text.of("================================================="));
                cs.sendMessage(Utils.printMessage("Original Name", TextColors.GREEN, model.getNameOrigin()));
                cs.sendMessage(Utils.printMessage("Name", TextColors.GREEN, model.getName()));
                cs.sendMessage(Utils.printMessage("Description", TextColors.GREEN, model.getDescription()));
                // 옵션 정보
                cs.sendMessage(Utils.printMessage("Permission", TextColors.BLUE, model.options.RealPermision()));
                cs.sendMessage(Utils.printMessage("BelongBlocksDelete", TextColors.BLUE, "" + model.options.BelongBlocksDelete));
                cs.sendMessage(Utils.printMessage("BelongEntitysRemove", TextColors.BLUE, "" + model.options.BelongEntitysRemove));
                cs.sendMessage(Utils.printMessage("EntityChangeRevert", TextColors.BLUE, "" + model.options.EntityChangeRevert));
                cs.sendMessage(Utils.printMessage("MaximumOneTickFrame", TextColors.BLUE, "" + model.options.MaximumOneTickFrame));
                cs.sendMessage(Utils.printMessage("MaximumAlive", TextColors.BLUE, "" + model.options.MaximumAlive));
                cs.sendMessage(Utils.printMessage("Interceptable", TextColors.BLUE, "" + model.options.Interceptable));
                // 통계 정보
                cs.sendMessage(Utils.printMessage("AverageRuntime", TextColors.LIGHT_PURPLE, "" + model.statistics.getAverageRuntime()));
                cs.sendMessage(Utils.printMessage("MaximumRuntime", TextColors.LIGHT_PURPLE, "" + model.statistics.getMaximumRuntime()));
                cs.sendMessage(Utils.printMessage("MinimumRuntime", TextColors.LIGHT_PURPLE, "" + model.statistics.getMinimumRuntime()));

                for (int i = 0; i < model.frames.length; i++) {
                    cs.sendMessage(Text.of("======================< " + i + " >======================"));
                    Class c = model.frames[i].getClass();
                    cs.sendMessage(Utils.printMessage("Frame Name", TextColors.DARK_GREEN, "" + c.getName()));
                    for (Field field: c.getDeclaredFields()){
                        try {
                            cs.sendMessage(Utils.printMessage(field.getName(), TextColors.DARK_AQUA, field.getType() + "(" + field.get(model.frames[i]).toString() + ")"));
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
                // 프레임 정보
                cs.sendMessage(Text.of("================================================="));

                break;
        }
    }

    public static CommandSpec cs =
            CommandSpec
                    .builder()
                    .permission(Permission.Model)
                    .description(Text.of("gss(GreadSkillSimulator)"))
                    .arguments(
                            GenericArguments.optional(GenericArguments.onlyOne(new SkillCommandArguments(Text.of("searchingFor")))),
                            GenericArguments.optional(GenericArguments.onlyOne(GenericArguments.enumValue(Text.of("mode"), gss_model.Mode.class)))
                    )
                    .executor(new CommandExecutor() {
                        @Override
                        public CommandResult execute(CommandSource commandSource, CommandContext commandContext) throws CommandException {
                            final Model[] model = new Model[1];
                            commandContext.getOne(Text.of("searchingFor")).ifPresent(o -> model[0] = (Model) o);
                            Mode mode = null;
                            mode = (Mode) commandContext.getOne(Text.of("mode")).orElse(Mode.BASIC);

                            if (model[0] == null) {
                                for (String key : Container.skillmodelmap.keySet()) {
                                    Model mdtemp = Container.Get(key);
                                    info(mode, commandSource, mdtemp);
                                }
                            } else {
                                info(mode, commandSource, model[0]);
                            }
                            return CommandResult
                                    .builder()
                                    .successCount(1)
                                    .affectedBlocks(0)
                                    .affectedEntities(0)
                                    .affectedItems(0)
                                    .build();
                        }
                    })
                    .build();
}
