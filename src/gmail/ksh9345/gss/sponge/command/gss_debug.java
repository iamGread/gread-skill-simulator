package gmail.ksh9345.gss.sponge.command;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;

/**
 * Created by ksh93 on 2017-02-04.
 */
public class gss_debug {
    protected enum Modes{
        LISTENER,
        ON,
        OFF,
        INFO,
    }
    public static CommandSpec cs =
            CommandSpec
                    .builder()
                    .permission("gss")
                    .description(Text.of("gss(GreadSkillSimulator)"))
                    .arguments(
                            GenericArguments.optional(GenericArguments.enumValue(Text.of("mode"), Modes.class))
                    )
                    .executor(new CommandExecutor() {
                        @Override
                        public CommandResult execute(CommandSource commandSource, CommandContext commandContext) throws CommandException {
                            Modes md = null;
                            if (commandContext.getOne(Text.of("mode")).isPresent()){
                                md =  (Modes) commandContext.getOne(Text.of("mode")).get();
                            }

                            switch (md){
                                default:
                                case LISTENER:

                                    break;
                                case ON:
                                    break;
                                case OFF:
                                    break;
                                case INFO:
                                    break;
                            }
                            return CommandResult
                                    .builder()
                                    .successCount(1)
                                    .affectedBlocks(0)
                                    .affectedEntities(0)
                                    .affectedItems(0)
                                    .build();
                        }
                    })
                    .build();
}
