package gmail.ksh9345.gss.sponge.command;

import gmail.ksh9345.gss.core.loader.Container;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.ArgumentParseException;
import org.spongepowered.api.command.args.CommandArgs;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.text.Text;

import java.util.Collections;
import java.util.List;

/**
 * Created by ksh93 on 2017-02-06.
 */
public class SkillCommandArguments extends CommandElement {
    protected SkillCommandArguments(Text key) {
        super(key);
    }

    @Override
    protected Object parseValue(CommandSource source, CommandArgs args) throws ArgumentParseException {
        return Container.Get(args.next());
    }
    @Override
    public List<String> complete(CommandSource src, CommandArgs args, CommandContext context) {
        try {
            return Container.Hint(args.next());
        } catch (ArgumentParseException e) {}
        return Collections.<String>emptyList();
    }

    @Override
    public Text getUsage(CommandSource src) {
        return Text.of("< Skillname >");
    }
}
