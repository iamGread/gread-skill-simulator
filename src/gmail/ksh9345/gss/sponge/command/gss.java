package gmail.ksh9345.gss.sponge.command;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;

/**
 * Created by ksh93 on 2017-01-30.
 */
public class gss {
    public static CommandSpec cs =
            CommandSpec
                    .builder()
                    .permission("gss") // 선택
                    .description(Text.of("gss(GreadSkillSimulator)"))// 선택
//                    .child(gss_debug.cs, "debug")
                    .child(gss_do.cs, "d", "do")// 선택 /gss do
                    .child(gss_debug.cs, "de", "debug")// 선택
                    .child(gss_tool.cs, "t", "tool")// 선택
                    .child(gss_simulator.cs, "s", "simulator")// 선택 /gss s
                    .child(gss_model.cs, "m", "model")// 선택
                    .child(gss_multipledo.cs, "md", "multipleDo")// 선택

                    .build(); // 선택
}

