package gmail.ksh9345.gss.sponge.command;

import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.sponge.PEP;
import gmail.ksh9345.gss.sponge.Permission;
import gmail.ksh9345.gss.sponge.Utils;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

/**
 * Created by ksh93 on 2017-02-05.
 */
public class gss_simulator {
    public enum Mode{
        RELOADMODELS,
        FORCERELOADMODELS,
        WORKERSINFO
    }
    public static CommandSpec cs =
            CommandSpec
                    .builder()
                    .permission(Permission.Simulator)
                    .description(Text.of("gss(GreadSkillSimulator)"))
                    .arguments(
                            GenericArguments.onlyOne(GenericArguments.enumValue(Text.of("mode"), Mode.class)))
                    .executor(new CommandExecutor() {
                        @Override
                        public CommandResult execute(CommandSource commandSource, CommandContext commandContext) throws CommandException {
                            final Mode[] md = {null};
                            commandContext.getOne(Text.of("mode")).ifPresent(o -> md[0] = (Mode) o);
                            switch (md[0]){
                                default:
                                case RELOADMODELS:
                                    if (commandSource instanceof ConsoleSource){
                                        PEP.getInstance().SmartReloadSkills();
                                    }else {
                                        commandSource.sendMessage(Utils.printMessage("Error", TextColors.RED, "Only Console Source can use this"));
                                    }
                                    break;
                                case FORCERELOADMODELS:
                                    if (commandSource instanceof ConsoleSource){
                                        PEP.getInstance().ReloadSkills();
                                    }else {
                                        commandSource.sendMessage(Utils.printMessage("Error", TextColors.RED, "Only Console Source can use this"));
                                    }
                                    break;
                                case WORKERSINFO:
                                    commandSource.sendMessage(Text.of("================================================="));
                                    commandSource.sendMessage(Utils.printMessage("PreDistributed", TextColors.BLUE, "" + PEP.simulatorMultiTask.UnreadySkill()));
                                    commandSource.sendMessage(Utils.printMessage("Syncronized Request", TextColors.GREEN, "" + PEP.simulatorMultiTask.SyncReqRemain()));
                                    commandSource.sendMessage(Utils.printMessage("GC Size", TextColors.RED, "" + PEP.simulatorMultiTask.GCLength()));
                                    commandSource.sendMessage(Utils.printMessage("Running Worker Size", TextColors.RED, "" + PEP.simulatorMultiTask.WorkersLength()));
                                    for (int i = 0; i < PEP.simulatorMultiTask.WorkersLength(); i++) {
                                        commandSource.sendMessage(Text.of("======================< " + i + " >======================"));
                                        SimulatorWorker sw = PEP.simulatorMultiTask.Worker(i);
                                        commandSource.sendMessage(Utils.printMessage("Worker Burden", TextColors.BLUE, "" + sw.getBurden()));
                                        commandSource.sendMessage(Utils.printMessage("Worker Running", TextColors.GREEN, "" + sw.getReadySkillSize()));
                                    }
                                    commandSource.sendMessage(Text.of("================================================="));
                                    break;
                            }
                            return CommandResult
                                    .builder()
                                    .successCount(1)
                                    .affectedBlocks(0)
                                    .affectedEntities(0)
                                    .affectedItems(0)
                                    .build();
                        }
                    })
                    .build();
}
