package gmail.ksh9345.gss.sponge.command;

import gmail.ksh9345.gss.sponge.Permission;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class gss_test {
    public enum Mode{
        TOGGLEGRAVITY,
    }
    public static CommandSpec cs =
            CommandSpec
                    .builder()
                    .permission(Permission.ADMIN)
                    .arguments(GenericArguments.enumValue(Text.of("mode"), Mode.class))
                    .executor(new CommandExecutor() {
                        @Override
                        public CommandResult execute(CommandSource commandSource, CommandContext commandContext) throws CommandException {
                            if(commandSource instanceof Player){
                                ((Player) commandSource).offer(Keys.HAS_GRAVITY, !((Player) commandSource).get(Keys.HAS_GRAVITY).orElse(true));
                            }
                            //
                            return CommandResult
                                    .builder()
                                    .successCount(1)
                                    .affectedBlocks(0)
                                    .affectedEntities(0)
                                    .affectedItems(0)
                                    .build();
                        }
                    })
                    .build();
}
