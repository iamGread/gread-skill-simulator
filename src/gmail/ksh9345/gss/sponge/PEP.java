package gmail.ksh9345.gss.sponge;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import gmail.ksh9345.gss.core.loader.*;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.skill.Model;
import gmail.ksh9345.gss.sponge.command.gss;
import gmail.ksh9345.gss.sponge.config.Config;
import org.slf4j.Logger;
import org.spongepowered.api.Game;
import org.spongepowered.api.Server;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.event.EventManager;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.GameReloadEvent;
import org.spongepowered.api.event.game.state.*;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.scheduler.Scheduler;
import org.spongepowered.api.text.format.TextColors;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

/**
 * GSS Main Plugin class
 * Plugin Entry Point
 * Created by iamGread on 2017-01-24.
 *
 *    _____                             _        _____   _      _   _   _        _____   _                       _           _
 *  / ____|                           | |      / ____| | |    (_) | | | |      / ____| (_)                     | |         | |
 * | |  __   _ __    ___    __ _    __| |     | (___   | | __  _  | | | |     | (___    _   _ __ ___    _   _  | |   __ _  | |_    ___    _ __
 * | | |_ | | '__|  / _ \  / _` |  / _` |      \___ \  | |/ / | | | | | |      \___ \  | | | '_ ` _ \  | | | | | |  / _` | | __|  / _ \  | '__|
 * | |__| | | |    |  __/ | (_| | | (_| |      ____) | |   <  | | | | | |      ____) | | | | | | | | | | |_| | | | | (_| | | |_  | (_) | | |
 * \_____| |_|     \___|  \__,_|  \__,_|     |_____/  |_|\_\ |_| |_| |_|     |_____/  |_| |_| |_| |_|  \__,_| |_|  \__,_|  \__|  \___/  |_|
 *
 *
 */

@Plugin(id = "greadskillsimulator", name = "Gread Skill Simulator", version = "0.6.4", authors = {"iamGread"}, description = "GSS is On Sponge based plugin for simulate definite action")
public class PEP {
    @Inject
    private void injectLogger(Logger logger){
        PEP.logger = logger;
    }
    public static Logger logger;
    //
    @Inject
    private void injectGame(Game game) {
        PEP.game = game;
        PEP.server = game.getServer();
        PEP.commandManager = PEP.game.getCommandManager();
        GSS_Path = Paths.get(PEP.game.getGameDirectory().toAbsolutePath().toString(), "GreadSeries", "GSS");
        consoleSource = game.getServer().getConsole();
    }
    public static Game game;
    public static Server server;
    public static Path GSS_Path;
    public static CommandManager commandManager;
    public static ConsoleSource consoleSource;
    //
    @Inject
    private void injectEventManager(EventManager eventManager) {
        PEP.eventManager = eventManager;
    }
    public static EventManager eventManager;

    //
    @Inject
    private void injectPluginContainer(PluginContainer pluginContainer) {
        PEP.pluginContainer = pluginContainer;
    }
    public static PluginContainer pluginContainer;
    //

    //
    public static PEP getInstance() {;
        Optional<?> pep = PEP.pluginContainer.getInstance();
        if (pep.get() instanceof PEP)
            return (PEP) pep.get();
        return null;
    }
    //================================================================================================================
    public static SimulatorMultiTask simulatorMultiTask = null;
    Config conf = null;
    //================================================================================================================
    public void ReloadSkills(){
        Container.Clear();
        // 스킬 로딩
        consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GREEN, "GSS Loader Start"));
        consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GRAY, ""));
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(GSS_Path)) {

            for (Path entry : stream) {
                if (Files.isDirectory(entry)) {
                    continue;
                }
                try {
                    //
                    if(entry.toString().toLowerCase().endsWith(".json")){
                        consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GREEN, "Start Load SKILL From '"+ entry.getFileName().toString() + "'"));
                        String hash = HashSHA256(entry);
                        consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GREEN, "SHA-256 : " + hash));
                        Model md = LoadSkillmodelFromPath(entry);
                        consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GREEN, "Load Complete '"+ entry.getFileName().toString() + "'"));
                        if(Container.Regist(md, entry, hash)){
                            consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GREEN, "Model Regist '"+ md.getName() + "'"));
                        }else {
                            consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.RED, "Model Regist Fail '"+ md.getName() + "' exist"));
                        }
                        consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GRAY, ""));
                    }
                } catch (Exception e1) {
                    consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.RED, "Load SKILL Fail From '"+ entry.getFileName().toString() + "'"));
                    e1.printStackTrace();
                }
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GREEN, "GSS Loader End"));
    }
    public void SmartReloadSkills(){
        Container.ClearNonexistPathModelFile();
        // 스킬 로딩
        consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GREEN, "GSS Smart Loader Start"));
        consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GRAY, ""));
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(GSS_Path)) {
            List<String> tempo = new ArrayList<>();
            for (Path entry : stream) {
                if (Files.isDirectory(entry)) {
                    continue;
                }
                try {
                    //
                    if(entry.toString().toLowerCase().endsWith(".json")){
                        consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GREEN, "Start Load SKILL From '"+ entry.getFileName().toString() + "'"));
                        String hash = HashSHA256(entry);
                        consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GREEN, "SHA-256 : " + hash));
                        Container.ModelFile mf = Container.GetByHash(hash);
                        if (mf == null){
                            Model md = LoadSkillmodelFromPath(entry);
                            consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GREEN, "Load Complete '"+ entry.getFileName().toString() + "'"));
                            if(Container.Regist(md, entry, hash)){
                                tempo.add(md.getName());
                                consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GREEN, "Model Regist '"+ md.getName() + "'"));
                            }else {
                                // 충돌하는 이름 중 만약 새로 추가된 것들 끼리 충돌한 경우
                                if(tempo.contains(md.getName())){
                                    consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.RED, "Model Regist Fail '"+ md.getName() + "' exist"));
                                }
                                // 충돌한 경우 중 이전의 내용과 충돌한 경우
                                else {
                                    Container.ForceRegist(md, entry, hash);
                                    consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.BLUE, "Update Model '"+ md.getName() + "' exist"));
                                }
                            }
                        }else {
                            //
                            consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.YELLOW, "Exist Hash'" + hash + "'"));
                        }
                        consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GREEN, ""));
                    }
                } catch (Exception e1) {
                    consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.RED, "Load SKILL Fail From '"+ entry.getFileName().toString() + "'"));
                    e1.printStackTrace();
                }
                consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GRAY, ""));
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        consoleSource.sendMessage(Utils.printMessage("GSS loader", TextColors.GREEN, "GSS Smart Loader End"));
    }
    private String HashSHA256(Path p){

        FileInputStream fis = null;
        MessageDigest md = null;
        String ret = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
            fis = new FileInputStream(p.toFile());
            FileChannel fc = fis.getChannel();
            ByteBuffer bb = ByteBuffer.allocate(1024);
            while (fc.read(bb) > 0){
                bb.flip();
                md.update(bb);
                bb.clear();
            }
            ret = Base64.getEncoder().encodeToString(md.digest());
        } catch (FileNotFoundException e) {
            return null;
        } catch (NoSuchAlgorithmException e) {
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return ret;
    }
    private Model LoadSkillmodelFromPath(Path p){
        Model sm = null;
        InputStreamReader fr = null;
        try {
            fr = new InputStreamReader(new FileInputStream(p.toFile()), "UTF-8");
            Gson gson = new Gson();
            JsonObject jo = gson.fromJson(fr, JsonObject.class);
            sm = Loader.LoadModel(jo);
        } catch (FileNotFoundException | UnsupportTypeArguments | FrameNotFoundException | ClassNotFoundException | NoMatchingConstructorException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            System.err.println("GSS only support UTF-8");
            e.printStackTrace();
        } finally {
            if (fr != null){
                try {
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sm;
    }
    //================================================================================================================
    @Listener
    public void onPreInit(GamePreInitializationEvent e) {
         conf = new Config();
        // 스킬 로드 위치 인식
        try {
            Files.createDirectories(GSS_Path);
            consoleSource.sendMessage(Utils.printMessage("GSS", TextColors.GREEN, "GSS Default Directory created"));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
    @Listener
    public void onInit(GameInitializationEvent e) {
        // 스킬 전체 로드작업
        ReloadSkills();
    }
    @Listener
    public void onPostInit(GamePostInitializationEvent e) {
    }
    @Listener
    public void onLoadComplete(GameLoadCompleteEvent e) {
        Scheduler sch = game.getScheduler();
        simulatorMultiTask = new SimulatorMultiTask();
        simulatorMultiTask.init(sch, this, conf);
        game.getCommandManager().register(this, gss.cs, "gss");
    }
    //========================================================================================================================//
    @Listener
    public void onServerStarted(GameStartedServerEvent e) {

    }
    @Listener
    public void onServerStarting(GameStartingServerEvent e) {

    }
    //========================================================================================================================//
    @Listener
    public void onServerStopping(GameStoppingServerEvent e) {

    }
    @Listener
    public void onServerStopped(GameStoppedServerEvent e) {

    }
    //========================================================================================================================//
    @Listener
    public void onGameReload(GameReloadEvent e) {

    }
}
