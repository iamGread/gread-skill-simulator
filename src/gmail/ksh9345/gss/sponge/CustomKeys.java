package gmail.ksh9345.gss.sponge;

import com.google.common.reflect.TypeToken;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.key.KeyFactory;
import org.spongepowered.api.data.value.mutable.Value;

/**
 * Created by ksh93 on 2017-04-14.
 */
public class CustomKeys {
    public final static Key<Value<Boolean>> CAN_GROUND_SNEAK = KeyFactory.makeSingleKey(
            TypeToken.of(Boolean.class),
            new TypeToken<Value<Boolean>>() {},
            DataQuery.of("CAN_GROUND_SNEAK"),
            "gss:CAN_GROUND_SNEAK",
            "CAN_GROUND_SNEAK"
    );
    public final static Key<Value<Boolean>> CAN_MIDAIR_SNEAK = KeyFactory.makeSingleKey(
            TypeToken.of(Boolean.class),
            new TypeToken<Value<Boolean>>() {},
            DataQuery.of("CAN_MIDAIR_SNEAK"),
            "gss:CAN_MIDAIR_SNEAK",
            "CAN_MIDAIR_SNEAK"
    );
}
