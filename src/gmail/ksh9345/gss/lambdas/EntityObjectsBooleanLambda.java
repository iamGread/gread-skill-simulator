package gmail.ksh9345.gss.lambdas;

import org.spongepowered.api.entity.Entity;

/**
 * Created by ksh93 on 2017-02-08.
 */
@FunctionalInterface
public interface EntityObjectsBooleanLambda {
    public boolean run(Entity e, Object... args);
}
