package gmail.ksh9345.gss.lambdas;

import org.spongepowered.api.entity.Entity;

/**
 * Created by ksh93 on 2017-02-07.
 */
@FunctionalInterface
public interface EntityLambda{
    public void run (Entity e);
}
