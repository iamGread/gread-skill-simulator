package gmail.ksh9345.gss.lambdas;

import org.spongepowered.api.entity.Entity;

import java.util.List;

/**
 * Created by ksh93 on 2017-02-08.
 */
@FunctionalInterface
public interface EntityEntitysObjectsBooleanLambda {
    public boolean run(Entity e, List<Entity> targets, Object ... args);
}
