package gmail.ksh9345.gss.lambdas;

import gmail.ksh9345.gss.core.skill.Skill;

/**
 * Created by ksh93 on 2017-02-09.
 */
@FunctionalInterface
public interface GCCALL {
    public void run(Skill skill);
}
