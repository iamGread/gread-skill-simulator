package gmail.ksh9345.gss.lambdas;

/**
 * Created by ksh93 on 2017-02-09.
 */
@FunctionalInterface
public interface SynchronizedTask {
    public void run();
}
