package gmail.ksh9345.gss.core.Errors;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * Created by ksh93 on 2017-02-11.
 */
public class SkillRunningError extends GSSError{
    int frame;
    GSSError err;

    public SkillRunningError(int frame, GSSError err) {
        this.frame = frame;
        this.err = err;
    }

    @Override
    public String getMessage() {
        return err.getMessage();
    }

    @Override
    public String getLocalizedMessage() {
        return err.getLocalizedMessage();
    }

    @Override
    public synchronized Throwable getCause() {
        return err.getCause();
    }
    @Override
    public void printStackTrace() {
        err.printStackTrace();
    }

    @Override
    public void printStackTrace(PrintStream s) {
        err.printStackTrace(s);
    }

    @Override
    public void printStackTrace(PrintWriter s) {
        err.printStackTrace(s);
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return err.fillInStackTrace();
    }

    @Override
    public StackTraceElement[] getStackTrace() {
        return err.getStackTrace();
    }

    @Override
    public String toString() {
        return "SkillRunningError [" + frame + " - '" + err.toString() + "']";
    }
}
