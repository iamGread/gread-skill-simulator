package gmail.ksh9345.gss.core.Errors;

/**
 * Created by ksh93 on 2017-02-11.
 */
public class NoPermisionForSkillError extends GSSError {
    String requirePermision;

    public NoPermisionForSkillError(String requirePermision) {
        this.requirePermision = requirePermision;
    }

    @Override
    public String toString() {
        return "NoPermisionForSkillError ['" + requirePermision + "']";
    }
}
