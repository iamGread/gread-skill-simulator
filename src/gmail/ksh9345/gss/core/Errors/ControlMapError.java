package gmail.ksh9345.gss.core.Errors;

import gmail.ksh9345.gss.core.skill.control.ControlData;

/**
 * Created by ksh93 on 2017-02-12.
 */
public class ControlMapError extends GSSError{
    String require;
    ControlData cd;
    int frame;

    public ControlMapError(String require, ControlData cd, int frame) {
        this.require = require;
        this.cd = cd;
        this.frame = frame;
    }
}
