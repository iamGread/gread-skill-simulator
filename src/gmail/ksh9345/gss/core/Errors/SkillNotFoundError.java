package gmail.ksh9345.gss.core.Errors;

/**
 * Created by ksh93 on 2017-02-11.
 */
public class SkillNotFoundError extends GSSError{
    String find;

    public SkillNotFoundError(String find) {
        this.find = find;
    }

    @Override
    public String toString() {
        return "SkillNotFoundError ['" + find + "']";
    }
}
