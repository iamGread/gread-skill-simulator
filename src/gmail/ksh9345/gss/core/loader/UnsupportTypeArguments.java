package gmail.ksh9345.gss.core.loader;

/**
 * 지원하지 않는 자료형을 클래스에서 사용시 발생
 * Created by ksh93 on 2017-02-06.
 */
public class UnsupportTypeArguments extends Exception{
    public UnsupportTypeArguments() {
        super();
    }

    public UnsupportTypeArguments(String message) {
        super(message);
    }

    public UnsupportTypeArguments(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportTypeArguments(Throwable cause) {
        super(cause);
    }

    protected UnsupportTypeArguments(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
