package gmail.ksh9345.gss.core.loader;

import gmail.ksh9345.gss.core.skill.Model;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ksh93 on 2017-01-24.
 */
public class Container {
    public static class ModelFile{
        public ModelFile(Model md, Path path, String hash) {
            this.md = md;
            this.path = path;
            this.hash = hash;
        }

        public Model md;
        public Path path;
        public String hash;
    }
    private static final Object lock = new Object();
    public static HashMap<String, ModelFile> skillmodelmap = new HashMap<>();
    public static boolean Regist(Model sm, Path p, String hash){
        if (sm == null){
            return false;
        }

        if (skillmodelmap.containsKey(sm.getName())){
            return false;
        }
        synchronized (lock){
            skillmodelmap.put(sm.getName(), new ModelFile(sm, p, hash));
        }
        return true;
    }
    public static void ForceRegist(Model sm, Path p, String hash){
        if (sm == null){
            return ;
        }
        synchronized (lock){
            skillmodelmap.put(sm.getName(), new ModelFile(sm, p, hash));
        }
        return;
    }
    public static Model Get(String key){
        ModelFile mf = skillmodelmap.get(key);
        if (mf != null)
            return mf.md;
        return null;
    }
    public static ModelFile GetByHash(String hash){
        final ModelFile[] md = {null};
        skillmodelmap.forEach((s, modelFile) -> {
            if(modelFile.hash.equals(hash)){
                md[0] = modelFile;
            }
        });
        return md[0];
    }
    public static void ClearNonexistPathModelFile(){
        skillmodelmap.entrySet().removeIf(stringModelFileEntry -> !stringModelFileEntry.getValue().path.toFile().exists());
    }
    public static List<String> Hint(String hint){
        List<String> temp = new ArrayList<>();
        for (String key : skillmodelmap.keySet()){
            if (key.startsWith(hint)){
                temp.add(key);
            }
        }
        return temp;
    }
    public static void Clear(){
        synchronized (lock){
            skillmodelmap.clear();
        }

    }
}
