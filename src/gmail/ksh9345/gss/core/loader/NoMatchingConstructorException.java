package gmail.ksh9345.gss.core.loader;

/**
 * 일치하는 컨스트럭터가 존재하지 않는 경우 발생
 * Created by ksh93 on 2017-02-06.
 */
public class NoMatchingConstructorException extends Exception{
    public NoMatchingConstructorException() {
        super();
    }

    public NoMatchingConstructorException(String message) {
        super(message);
    }

    public NoMatchingConstructorException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoMatchingConstructorException(Throwable cause) {
        super(cause);
    }

    protected NoMatchingConstructorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
