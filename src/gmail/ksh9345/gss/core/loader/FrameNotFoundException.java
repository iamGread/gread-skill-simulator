package gmail.ksh9345.gss.core.loader;

/**
 * 존재하지 않는 프레임 사용시 발생
 * Created by ksh93 on 2017-02-06.
 */
public class FrameNotFoundException extends Exception{
    public FrameNotFoundException() {
        super();
    }

    public FrameNotFoundException(String message) {
        super(message);
    }

    public FrameNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public FrameNotFoundException(Throwable cause) {
        super(cause);
    }

    protected FrameNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
