package gmail.ksh9345.gss.core.loader;

/**
 * Enum 값을 요구하는 부분에 String 이외의 요소를 넣은 경우 발생
 * Created by ksh93 on 2017-02-06.
 */
public class ExpectEnumException extends Exception{
    public ExpectEnumException() {
        super();
    }

    public ExpectEnumException(String message) {
        super(message);
    }

    public ExpectEnumException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExpectEnumException(Throwable cause) {
        super(cause);
    }

    protected ExpectEnumException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}