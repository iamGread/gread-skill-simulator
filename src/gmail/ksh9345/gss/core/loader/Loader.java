package gmail.ksh9345.gss.core.loader;

import com.flowpowered.math.vector.Vector3d;
import com.google.gson.*;
import com.sun.org.apache.xpath.internal.operations.Bool;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.frames.etc.Padding;
import gmail.ksh9345.gss.core.skill.Model;
import gmail.ksh9345.gss.core.skill.ModelBuilder;
import gmail.ksh9345.gss.core.skill.Option;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.effect.particle.ParticleType;
import org.spongepowered.api.effect.potion.PotionEffectType;
import org.spongepowered.api.effect.sound.SoundType;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.chat.ChatType;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.util.*;

/**
 * Created by ksh93 on 2017-01-24.
 */
public class Loader {
    public static String[] defaulttypes = new String[]{
            // 실수형
            "double", "float",
            // 정수형
            "byte", "short", "int", "long",
            // 논리형
            "boolean",
            // 문자형
            "char",
            // 문장형
            "java.lang.String",
            "org.spongepowered.api.text.Text",
            // 벡터
            "com.flowpowered.math.vector.Vector3d",
            // Sponge 타입
            "org.spongepowered.api.entity.EntityType",
            "org.spongepowered.api.effect.potion.PotionEffectType",
            "org.spongepowered.api.block.BlockType",
            "org.spongepowered.api.effect.particle.ParticleType",
            "org.spongepowered.api.effect.sound.SoundType",
            "org.spongepowered.api.text.chat.ChatType",
            "org.spongepowered.api.item.ItemType",
            // 그 외의 모든 enum 형 지원
    };

    public static class LoadPackage {
        protected String PACKAGEPATH;
        protected String[] FRAMETYPES;

        public LoadPackage(String PACKAGEPATH, String[] FRAMETYPES) {
            this.PACKAGEPATH = PACKAGEPATH;
            this.FRAMETYPES = FRAMETYPES;
        }

        public String getPACKAGEPATH() {
            return PACKAGEPATH;
        }

        public List<String> getFRAMETYPES() {
            List<String> temp = new ArrayList<>();
            for (String str : FRAMETYPES) {
                temp.add(new String(str));
            }
            return temp;
        }
    }

    public static final LoadPackage DefaultPKG = new LoadPackage(
            "gmail.ksh9345.gss.core.frames",
            new String[]{"act", "control", "etc", "find", "manage", "movement", "select", "show", "sort", "listen"});

    //
    //
    public static Model LoadModel(String str) throws FrameNotFoundException, UnsupportTypeArguments, NoMatchingConstructorException, ClassNotFoundException, IllegalArgumentException, UnsupportedEncodingException {
        return LoadModel(new Gson().fromJson(new InputStreamReader(new ByteArrayInputStream(str.getBytes()), "UTF-8"), JsonObject.class));


    }

    public static Model LoadModel(JsonObject jo) throws FrameNotFoundException, UnsupportTypeArguments, NoMatchingConstructorException, ClassNotFoundException, IllegalArgumentException {
        String skillname = "";
        skillname = jo.get("skillname").getAsString();
        //
        String description = "";
        try {
            description = jo.get("description").getAsString();
        } catch (Exception e) {
        }
        //
        Option opt = new Option();
        try {
            opt = new Gson().fromJson(jo.get("options"), Option.class);
        } catch (Exception e) {
        }
        JsonArray ja = jo.get("frames").getAsJsonArray();
        preprocess(ja, false);
        //
        List<BaseFrame> frames = loadListedFrames(ja);
        //
        //
        ModelBuilder smb = new ModelBuilder(skillname);
        for (BaseFrame bf : frames) {
            smb.addFrame(bf);
        }
        smb.setDescription(description);
        smb.setOptions(opt);
        return smb.Build();
    }

    private static void preprocess(JsonArray ja, boolean multiframe) throws IllegalArgumentException {
        HashMap<String, Integer> labeltable = new HashMap<>();
        // 라벨 테이블 구성
        for (int i = 0; i < ja.size(); i++) {
            JsonObject frame;
            try {
                frame = ja.get(i).getAsJsonObject();
            } catch (Exception e) {
                throw new IllegalArgumentException("frames must be map");
            }

            String strlabel;
            try {
                strlabel = frame.get("label").getAsString();
            } catch (Exception e) {
                continue;
            }
            if (labeltable.containsKey(strlabel)) {
                throw new IllegalArgumentException("Already Have Lable : " + strlabel);
            }
            labeltable.putIfAbsent(strlabel, i);
        }
        //
        for (JsonElement je : ja) {
            JsonObject frame = je.getAsJsonObject();
            JsonArray args;
            try {
                args = frame.get("args").getAsJsonArray();
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException("args Must Be array");
            }
            //============================================//
            JsonArray labeled;
            if (multiframe) {
                /// 멀티프레임 미구현됨
                labeled = new JsonArray();
            } else {
                labeled = new JsonArray();
                for (JsonElement arg : args) {
                    try {
                        String temp = arg.getAsString();
                        if (temp.startsWith("#")) {
                            Integer index = labeltable.get(temp.substring(1));
                            if (index == null) {
                                throw new IllegalArgumentException("undeclared label : " + temp.substring(1));
                            } else {
                                labeled.add(new JsonPrimitive(index));
                            }
                        } else {
                            labeled.add(arg);
                        }
                    } catch (IllegalArgumentException | IllegalStateException e) {
                        labeled.add(arg);
                    }
                }
            }
            frame.remove("args");
            frame.add("args", labeled);
        }
    }

    private static List<BaseFrame> loadListedFrames(JsonArray ja) throws FrameNotFoundException, NoMatchingConstructorException, UnsupportTypeArguments, ClassNotFoundException, IllegalArgumentException {
        List<BaseFrame> lbf = new ArrayList<>();
        for (int i = 0; i < ja.size(); i++) {
            lbf.add(loadFrame(ja.get(i).getAsJsonObject()));
        }
        lbf.add(new Padding());
        return lbf;
    }

    private static BaseFrame loadFrame(JsonObject jo) throws FrameNotFoundException, NoMatchingConstructorException, UnsupportTypeArguments, ClassNotFoundException, IllegalArgumentException {
        Class temp = getFrame(jo.get("frame").getAsString());
        if (temp == null)
            throw new FrameNotFoundException(jo.get("frame").getAsString());
        Constructor[] cstrcts = temp.getConstructors();
        Object ret = null;
        for (Constructor cstrct : cstrcts) {
            try {
                ret = useConstructor(cstrct, jo.get("args").getAsJsonArray());
                if (ret != null) {
                    break;
                }
            } catch (UnsupportTypeArguments | ClassNotFoundException | IllegalArgumentException | IllegalStateException e) {
                continue;
            }
        }
        if (ret == null) {
            throw new NoMatchingConstructorException(jo.get("frame").getAsString());
        }
        return (BaseFrame) ret;
    }

    private static Class getFrame(String framename) {

        Class cl = null;
        for (String temp : DefaultPKG.FRAMETYPES) {
            try {
                cl = Class.forName(DefaultPKG.PACKAGEPATH + "." + temp + "." + framename);
                if (cl != null) {
//                    if (cl.isAssignableFrom(BaseFrame.class)){
//                        break;
//                    }else {
//                        cl = null;
//                        continue;
//                    }
                }
            } catch (ClassNotFoundException e) {
            }
        }
        return cl;
    }

    private static Object getObjectByParameter(String typename, JsonElement je) throws UnsupportTypeArguments, ClassNotFoundException, IllegalArgumentException {
        boolean isString = false;
        boolean isNumber = false;
        boolean isBoolean = false;
        if (je.isJsonPrimitive()) {
            JsonPrimitive jp = je.getAsJsonPrimitive();
            isString = jp.isString();
            isNumber = jp.isNumber();
            isBoolean = jp.isBoolean();
        }
        switch (typename) {
            default:
                throw new UnsupportTypeArguments(typename);
                //===========================================================
                // 스폰지 타입
            case "org.spongepowered.api.entity.EntityType":
            case "org.spongepowered.api.effect.potion.PotionEffectType":
            case "org.spongepowered.api.block.BlockType":
            case "org.spongepowered.api.effect.particle.ParticleType":
            case "org.spongepowered.api.effect.sound.SoundType":
            case "org.spongepowered.api.text.chat.ChatType":
            case "org.spongepowered.api.item.ItemType":
                Class spongetype = Class.forName(typename);
                Optional<?> temp = Sponge.getGame().getRegistry().getType(spongetype, je.getAsString());
                if (!temp.isPresent()) {
                    throw new IllegalArgumentException("None Exist Sponge Type '" + je.getAsString() + "' for '" + typename + "'");
                }
                return (temp.get());
            //===========================================================
            // 실수형
            case "float":
                if (!isNumber) {
                    throw new IllegalArgumentException();
                }
                return (je.getAsFloat());
            case "double":
                if (!isNumber) {
                    throw new IllegalArgumentException();
                }
                return (je.getAsDouble());
            //===========================================================
            // 정수형
            case "byte":
                if (!isNumber) {
                    throw new IllegalArgumentException();
                }
                return (je.getAsByte());
            case "short":
                if (!isNumber) {
                    throw new IllegalArgumentException();
                }
                return (je.getAsShort());
            case "int":
                if (!isNumber) {
                    throw new IllegalArgumentException();
                }
                return (je.getAsInt());
            case "long":
                if (!isNumber) {
                    throw new IllegalArgumentException();
                }
                return (je.getAsLong());

            //===========================================================
            // 논리형
            case "boolean":
                if (!isBoolean) {
                    throw new IllegalArgumentException();
                }
                return (je.getAsBoolean());
            //===========================================================
            // 문자형
            case "char":
                return (je.getAsCharacter());
            //===========================================================
            // 문장형

            case "java.lang.String":
                if (!isString) {
                    throw new IllegalArgumentException();
                }
                return (je.getAsString());
            case "org.spongepowered.api.text.Text":
                return (TextSerializers.JSON.deserialize(je.getAsString()));
            //===========================================================
            // 벡터
            case "com.flowpowered.math.vector.Vector3d":
                JsonArray temp_vector = je.getAsJsonArray();
                if (temp_vector.size() != 3) {
                    throw new IllegalArgumentException("Vector size must be 3");
                }
                return new Vector3d(
                        temp_vector.get(0).getAsDouble(),
                        temp_vector.get(1).getAsDouble(),
                        temp_vector.get(2).getAsDouble()
                );
        }
    }
    private static Object useConstructor(Constructor cstrct, JsonArray ja) throws UnsupportTypeArguments, ClassNotFoundException, IllegalArgumentException {
        Parameter[] params = cstrct.getParameters();
        List<Object> values = new ArrayList<>();

        // 매개변수 체크
        if (params.length != ja.size()) {
            // 매개변수 개수가 틀린 경우 확인 생략
            return null;
        }
        for (int i = 0; i < params.length; i++) {
            //==================================================================================================
            // enum 타입의 경우
            if (params[i].getType().isEnum()) {
                // 해당 enum 값 넘김
                values.add(Enum.valueOf(params[i].getType().asSubclass(Enum.class), ja.get(i).getAsString()));
            }
            //==================================================================================================
            //배열의 경우
            else if (params[i].getType().isArray()) {
                String tp = params[i].getType().getComponentType().getName();
                List<Object> arrayObject = new ArrayList<>();
                JsonArray innerja = ja.get(i).getAsJsonArray();
                for (JsonElement je : innerja) {
                    arrayObject.add(getObjectByParameter(tp, je));
                }
                switch (tp) {
                    default:
                        throw new UnsupportTypeArguments(tp);
                        //===========================================================
                        // 스폰지 타입
                    case "org.spongepowered.api.entity.EntityType":
                        values.add(arrayObject.stream().toArray(EntityType[]::new));
                        break;
                    case "org.spongepowered.api.effect.potion.PotionEffectType":
                        values.add(arrayObject.stream().toArray(PotionEffectType[]::new));
                        break;
                    case "org.spongepowered.api.block.BlockType":
                        values.add(arrayObject.stream().toArray(BlockType[]::new));
                        break;
                    case "org.spongepowered.api.effect.particle.ParticleType":
                        values.add(arrayObject.stream().toArray(ParticleType[]::new));
                        break;
                    case "org.spongepowered.api.effect.sound.SoundType":
                        values.add(arrayObject.stream().toArray(SoundType[]::new));
                        break;
                    case "org.spongepowered.api.text.chat.ChatType":
                        values.add(arrayObject.stream().toArray(ChatType[]::new));
                        break;
                    case "org.spongepowered.api.item.ItemType":
                        values.add(arrayObject.stream().toArray(ItemType[]::new));
                        break;
                    //===========================================================
                    // 실수형
                    case "float":
                        values.add(arrayObject.stream().toArray(Float[]::new));
                        break;
                    case "double":
                        values.add(arrayObject.stream().toArray(Double[]::new));
                        break;
                    //===========================================================
                    // 정수형
                    case "byte":
                        values.add(arrayObject.stream().toArray(Byte[]::new));
                        break;
                    case "short":
                        values.add(arrayObject.stream().toArray(Short[]::new));
                        break;
                    case "int":
                        values.add(arrayObject.stream().toArray(Integer[]::new));
                        break;
                    case "long":
                        values.add(arrayObject.stream().toArray(Long[]::new));
                        break;

                    //===========================================================
                    // 논리형
                    case "boolean":
                        values.add(arrayObject.stream().toArray(Boolean[]::new));
                        break;
                    //===========================================================
                    // 문자형
                    case "char":
                        values.add(arrayObject.stream().toArray(Character[]::new));
                        break;
                    //===========================================================
                    // 문장형

                    case "java.lang.String":
                        values.add(arrayObject.stream().toArray(String[]::new));
                        break;
                    case "org.spongepowered.api.text.Text":
                        values.add(arrayObject.stream().toArray(Text[]::new));
                        break;
                    //===========================================================
                    // 벡터
                    case "com.flowpowered.math.vector.Vector3d":
                        values.add(arrayObject.stream().toArray(Vector3d[]::new));
                        break;
                }
            }
            //==================================================================================================
            // class 혹은 기본 자료형
            else {
                values.add(getObjectByParameter(params[i].getType().getName(), ja.get(i)));
            }
        }
        Object ret = null;
        try {
            ret = cstrct.newInstance(values.toArray());
        } catch (Exception e) {
        }
        return ret;
    }

}
