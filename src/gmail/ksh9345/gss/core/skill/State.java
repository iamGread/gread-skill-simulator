package gmail.ksh9345.gss.core.skill;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.Lists;
import gmail.ksh9345.gss.core.Errors.GSSError;
import gmail.ksh9345.gss.core.skill.control.ControlData;
import gmail.ksh9345.gss.core.skill.control.ControlListenerData;
import gmail.ksh9345.gss.sponge.causes.GSSBlocks;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.value.mutable.Value;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by ksh93 on 2017-01-18.
 */
public class State {
    protected State(){}
    public State(Living caster, Skill skill) {
        this.caster = caster;
        this.originalSkill = skill;
        //
        this.targets = new ArrayList<>();
        this.targetStack = new Stack<>();
        //
        this.location = this.caster.getLocation().copy();
        this.rotation = this.caster.getHeadRotation();
        //
        this.world = caster.getWorld();
        this.startPosition = new Vector3d(caster.getLocation().getPosition());
        this.startHeadRotation = new Vector3d(caster.getHeadRotation());
        this.startRotation = new Vector3d(caster.getRotation());
        this.startTime = new Date().getTime();
        //
        this.currentTime = this.startTime;
        //
        this.framelength = this.originalSkill.length();
        this.executionframe = 0;
        this.skillActivation = true;
        this.tickExecution = true;
        //
        this.controlmap = new HashMap<>();
        this.belongEntitys = new HashMap<>();
        this.belongBlock = new ArrayList<>();
        changedEntityKeys = new HashMap<>();
        //
        this.error = null;
        this.executionframe = 0;
    }
    public State(Living caster, Skill skill, Location<World> loc, Vector3d rot) {
        this.caster = caster;
        this.originalSkill = skill;
        //
        this.targets = new ArrayList<>();
        this.targetStack = new Stack<>();
        //
        this.location = loc;
        this.rotation = rot;
        //
        this.world = caster.getWorld();
        this.startPosition = new Vector3d(caster.getLocation().getPosition());
        this.startHeadRotation = new Vector3d(caster.getHeadRotation());
        this.startRotation = new Vector3d(caster.getRotation());
        this.startTime = new Date().getTime();
        //
        this.currentTime = this.startTime;
        //
        this.framelength = this.originalSkill.length();
        this.executionframe = 0;
        this.skillActivation = true;
        this.tickExecution = true;
        //
        this.controlmap = new HashMap<>();
        this.belongEntitys = new HashMap<>();
        this.belongBlock = new ArrayList<>();
        changedEntityKeys = new HashMap<>();
        //
        this.error = null;
        this.executionframe = 0;
    }
    //
    protected CopyOnWriteArrayList<SubSkill> asyncSubSkills = new CopyOnWriteArrayList<>();
    protected SubSkill syncSubSkill = null;
    //
    public int getCurrentFrame() {
        return executionframe - 1;
    }
    public void clearChildren() {
        if (syncSubSkill != null){
            syncSubSkill.state.clearChildren();
        }
        for (SubSkill asyncSubSkill : asyncSubSkills) {
            asyncSubSkill.state.clearChildren();
        }
        if (originalSkill.model.options.BelongEntitysRemove){
            belongEntitys.values().forEach(
                    Entity::remove
            );
        }
        if (originalSkill.model.options.BelongBlocksDelete){
            BlockChange bc;
            HashMap<Vector3i, BlockChange> temp = new HashMap<>();
            ListIterator<BlockChange> llbc = belongBlock.listIterator(belongBlock.size());
            for (;llbc.hasPrevious();){
                bc = llbc.previous();
                temp.putIfAbsent(bc.blockPosition, bc);
            }
            temp.forEach(
                    (vector3i, blockChange) -> {
                        if (blockChange.temp == this){
                            world.setBlock(vector3i, blockChange.previous, GSSBlocks.revert(caster));
                        }
                    }
            );
        }
    }
    public void jump(int jmp){
        if (jmp < 0){
            this.executionframe = this.framelength;
        }else {
            this.executionframe = jmp;
        }

    }
    public void placeBlock(int code, Vector3d position, BlockType tp){
        Location<World> lw = world.getLocation(position);
        BlockState bs = null;
        Vector3i blockpos = lw.getBlockPosition();

        for (BlockChange blockChange : belongBlock) {
            // 이미 해당 코드가 입력된 경우
            if (blockChange.code == code){
                return;
            }
            // 해당 위치에 더 이전의 기록이 있는 경우 기록들 중 가장 이전의 것을 가져옴
            if ( bs == null && blockChange.blockPosition.equals(blockpos)){
                bs = blockChange.previous;
            }
        }
        // 이전 기록이 없으면 현재 기록으로 대체
        if (bs == null){
            bs = lw.getBlock();
        }
        //
        lw.setBlockType(tp, GSSBlocks.place(caster));
        belongBlock.add(new BlockChange(this, bs, lw.getBlockPosition(), tp, code));
    }
    public void placeBlock(Vector3d position, BlockType tp){
        Location<World> lw = world.getLocation(position);
        BlockState bs = null;
        Vector3i blockpos = lw.getBlockPosition();

        for (BlockChange blockChange : belongBlock) {
            // 해당 위치에 더 이전의 기록이 있는 경우 기록들 중 가장 이전의 것을 가져옴
            if ( bs == null && blockChange.blockPosition.equals(blockpos)){
                bs = blockChange.previous;
            }
        }
        // 이전 기록이 없으면 현재 기록으로 대체
        if (bs == null){
            bs = lw.getBlock();
        }
        lw.setBlockType(tp, GSSBlocks.place(caster));
        belongBlock.add(new BlockChange(this, bs, lw.getBlockPosition(), tp, -1));
    }
    public void deleteBlock(int code){
        BlockChange bc = null;
        for (BlockChange blockChange : belongBlock) {
            // 이미 해당 코드가 입력된 경우
            if (blockChange.code == code){
                bc = blockChange;
            }
            // 해당 위치에 더 최신의 기록이 있는 경우 동작 취소
            if ( bc != null && blockChange.blockPosition.equals(bc.blockPosition)){
                return;
            }
        }
        // 이전 기록이 없으면 현재 기록으로 대체
        if (bc == null){
            return;
        }
        world.setBlock(bc.blockPosition, bc.previous, GSSBlocks.revert(caster));
        belongBlock.remove(bc);
    }
    public void setError(GSSError err){
        this.erroredFrame = getCurrentFrame();
        this.error = err;
    }
    public boolean active(int code, List<Entity> le, boolean expire){
        if (code < 0){
            return true;
        }
        ControlData cd = controlmap.get(code);
        if (cd != null){
            if (cd instanceof ControlListenerData){
                return ((ControlListenerData) cd).on(le, expire);
            }
        }
        return false;
    }
    //===========================================================================//
    // 프래임에서 목표로 잡는 것들
    public Living caster;
    public List<Entity> targets;
    public Stack<List<Entity>> targetStack;
    //===========================================================================//
    // 스킬이 시전되는 위치
    public Location<World> location;
    public Vector3d rotation;
    //===========================================================================//
    // 실행 시의 상태에 대한 설명
    public World world;
    public Vector3d startPosition;
    public Vector3d startHeadRotation;
    public Vector3d startRotation;
    public long startTime;
    //===========================================================================//
    // 메타데이터
    public Skill originalSkill;
    //===========================================================================//
    // 프레임 실행용
    public long currentTime;
    //===========================================================================//
    // 제어 관련
    public Map<Integer, ControlData> controlmap;
    public Date pauseReservation;
    public int framelength; //  마지막 프레임까지의 길이입니다.
    protected int executionframe; // 현재 실행중인 프레임의 인덱스입니다.
    public boolean skillActivation; // 이 플래그가 false면 스킬은 다음 프레임이 실행되기 전 종료됩니다.
    public boolean tickExecution; // 다음 프래임이 현재 틱 내에서 실행하게 내버려 둘지 결정하는 플래그입니다.
    //===========================================================================//
    // 소속
    public Map<Integer, Entity> belongEntitys;
    public List<BlockChange> belongBlock;
    public Map<Entity, KeyChange> changedEntityKeys;
    public class KeyChange{
        public Map<Key<Value<?>>, ?> changes;

        public KeyChange() {
            changes = new HashMap<>();
        }
    }
    //===========================================================================//
    // 에러 처리
    public GSSError error;
    public int erroredFrame;
    //===========================================================================//
    public class BlockChange{
        BlockState previous;
        Vector3i blockPosition;
        BlockType bt;
        int code;
        State temp;

        public BlockChange(State temp, BlockState previous, Vector3i blockPosition, BlockType bt, int code) {
            this.temp = temp;
            this.previous = previous;
            this.blockPosition = blockPosition;
            this.bt = bt;
            this.code = code;
        }
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<State : 0x");
        sb.append(Integer.toHexString(this.hashCode()));
        sb.append(">\n");
        //====================================================
        sb.append("- SKILL\n");

        sb.append("    Position : ");
        sb.append(this.location);
        sb.append("\n");
        sb.append("    Rotation : ");
        sb.append(this.rotation);
        sb.append("\n");
        //====================================================
        sb.append("- Start\n");

        sb.append("    Start Position : ");
        sb.append(this.startPosition);
        sb.append("\n");

        sb.append("    Start Rotation : ");
        sb.append(this.startRotation);
        sb.append("\n");

        sb.append("    Start Headrotation : ");
        sb.append(this.startHeadRotation);
        sb.append("\n");
        //====================================================
        sb.append("- Targets\n");
        for (Entity temp : this.targets) {
            sb.append("    ");
            sb.append(temp);
            sb.append("\n");
        }
        sb.append("    <TargetStack>");
        sb.append("    Size : ");
        sb.append(targetStack.size());
        sb.append("\n");
        //====================================================


        return sb.toString();
    }
    public State copyForSubskill(int c){
        State temp = new State();
        temp.caster = caster;
        temp.targets = new ArrayList<>();
        temp.targets.addAll(targets);
        temp.targetStack = new Stack<>();
        //
        temp.location = location.copy();
        temp.rotation = new Vector3d(rotation);
        //
        temp.world = world;
        temp.startPosition = new Vector3d(startPosition);
        temp.startHeadRotation = new Vector3d(startHeadRotation);
        temp.startRotation = new Vector3d(startRotation);
        temp.startTime = (startTime);
        //
        temp.originalSkill = originalSkill;
        //
        temp.currentTime = currentTime;
        //
        temp.controlmap = new HashMap<>();
        temp.pauseReservation = null;
        temp.framelength = c;
        temp.executionframe = 0;
        temp.skillActivation = true;
        temp.tickExecution = true;
        //
        temp.belongEntitys = new HashMap<>();
        temp.belongBlock = this.belongBlock;
        changedEntityKeys = new HashMap<>();
        return temp;
    }
}
