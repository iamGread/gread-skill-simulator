package gmail.ksh9345.gss.core.skill;

/**
 * Created by ksh93 on 2017-02-09.
 */
public class Statistic {
    public static int activeCountReset = 4096;
    public static short needInfoInterval= 32;
    //
    private long minimumRuntime = Integer.MAX_VALUE;
    private long maximumRuntime = 0;
    private long averageRuntime = 0;

    private int activeCount = 0;
    private short ndInfo = (short) (needInfoInterval + 1);
    //
    public boolean needInfo(){
        if (ndInfo > needInfoInterval) {
            ndInfo = 0;
            return true;
        }
        ndInfo += 1;
        return false;
    }
    public void sendRuntime(long runtime){
        // 카운트 초기화
        if (activeCount > activeCountReset){
            activeCount = 1;
        }
        // 최대, 최소시간 설정
        if (runtime > maximumRuntime){
            maximumRuntime = runtime;
        }
        if (runtime < minimumRuntime){
            minimumRuntime = runtime;
        }
        long temp = averageRuntime * activeCount + runtime;
        activeCount ++;
        averageRuntime = temp / activeCount;
    }

    public long getAverageRuntime(){
        return averageRuntime;
    }
    public long getMinimumRuntime() {
        return minimumRuntime;
    }

    public long getMaximumRuntime() {
        return maximumRuntime;
    }
}
