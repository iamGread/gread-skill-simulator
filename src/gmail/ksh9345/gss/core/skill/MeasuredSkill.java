package gmail.ksh9345.gss.core.skill;

import gmail.ksh9345.gss.core.Errors.SkillRunningError;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;

import java.util.Date;

/**
 *  시간 측정 기능이 있는 Skill의 래핑 클래스의 일종입니다.
 * Created by ksh93 on 2017-02-09.
 */
public class MeasuredSkill extends Skill {
    long runtime = 0;

    public MeasuredSkill(Skill skill) {
        super(skill);
    }

    public long getRuntime() {
        return runtime;
    }

    public synchronized void setRuntime(long runtime) {
        this.runtime = runtime;
    }

    @Override
    public boolean Act(SimulatorMultiTask smt, SimulatorWorker sw, Date now) throws SkillRunningError {
        long temp = new Date().getTime();
        boolean ret =  super.Act(smt, sw, now);
        runtime += new Date().getTime() - temp;
        return ret;
    }
}
