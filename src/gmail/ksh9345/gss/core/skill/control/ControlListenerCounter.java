package gmail.ksh9345.gss.core.skill.control;

import org.spongepowered.api.entity.Entity;

import java.util.List;

/**
 * Created by ksh93 on 2017-02-12.
 */
public class ControlListenerCounter implements ControlListenerData{
    private final Object lock = new Object();
    long expire;
    ListenerInner temp = null;
    int count = 0;

    public ControlListenerCounter(long expire, int counter) {
        this.expire = expire;
        this.count = counter;
    }

    @Override
    public boolean on(List<Entity> targets, boolean expire) {
        boolean ret = false;
        synchronized (lock){
            if (temp == null){
                ret = true;
                temp = new ListenerInner(targets, expire);
            }
        }
        return ret;
    }

    @Override
    public boolean remain(long current) {
        return current < expire;
    }

    @Override
    public boolean counter() {
        count--;
        return count > 0;
    }

    @Override
    public void expire() {
        count = 0;
        expire = 0;
    }

    @Override
    public ListenerInner next() {
        ListenerInner ret = null;
        synchronized (lock){
            ret = temp;
            temp = null;
        }
        return ret;
    }
}
