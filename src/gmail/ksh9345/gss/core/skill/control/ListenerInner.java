package gmail.ksh9345.gss.core.skill.control;

import org.spongepowered.api.entity.Entity;

import java.util.List;

/**
 * Created by ksh93 on 2017-02-12.
 */
public class ListenerInner {
    public List<Entity> data = null;
    public boolean expire = false;

    public ListenerInner(List<Entity> data, boolean expire) {
        this.data = data;
        this.expire = expire;
    }
}
