package gmail.ksh9345.gss.core.skill.control;

import org.spongepowered.api.entity.Entity;

import java.util.List;

/**
 * Created by ksh93 on 2017-02-12.
 */
public interface ControlListenerData extends ControlData{
    public boolean on(List<Entity> targets, boolean expire);
    public ListenerInner next();
    public boolean remain(long current);
    public boolean counter();
    public void expire();
}
