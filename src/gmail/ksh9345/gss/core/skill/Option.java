package gmail.ksh9345.gss.core.skill;

import gmail.ksh9345.gss.sponge.Permission;

/**
 * Created by ksh93 on 2017-01-24.
 */
public class Option {
    public String Permission = null;
    public boolean Interceptable = false;
    public int MaximumAlive = 100 * 1000; // -1값이면 무한수명을 가짐 기본값 100 초
    public int MaximumOneTickFrame = 8; // 한번에 재생 가능한 최대 프레임 수
    //
    public boolean BelongBlocksDelete = true;
    public boolean BelongEntitysRemove = true;
    public boolean EntityChangeRevert = true;
    public String RealPermision(){
        if (Permission == null){
            return "";
        }
        return gmail.ksh9345.gss.sponge.Permission.Execute + "." + Permission;
    }
}
