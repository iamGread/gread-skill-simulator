package gmail.ksh9345.gss.core.skill;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.Errors.SkillRunningError;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by ksh93 on 2017-01-21.
 */
public class Skill {
    public Model model;
    public State state;
    //

    protected Skill(Skill skill){
        model = skill.model;
        state = skill.state;
    }
    protected Skill(Model md, State st){
        model = md;
        state = st;
    }
    public Skill(Model sm, Living caster) {
        this.model = sm;
        this.state = new State(caster, this);
    }
    public Skill(Model sm, Living caster, List<Entity> targets) {
        this.model = sm;
        this.state = new State(caster, this);
        this.state.targets = targets;
    }
    public Skill(Model sm, Living caster, Location<World> loc, Vector3d rot) {
        this.model = sm;
        this.state = new State(caster, this, loc, rot);
    }
    public Skill(Model sm, Living caster, Location<World> loc, Vector3d rot, List<Entity> targets) {
        this.model = sm;
        this.state = new State(caster, this, loc, rot);
        this.state.targets = targets;
    }
    //
    public void requestAsync(SubSkill ss){
        state.asyncSubSkills.add(ss);
    }

    public void requestSync(SubSkill ss){
        state.syncSubSkill = ss;
    }
    //
    // 한 틱에 작동할 동작들
    // 정상적인 종료의 경우 false, 아직 남은 프레임이 있는 경우 true, 비정상적인 동작시 throw
    public boolean Act(SimulatorMultiTask smt, SimulatorWorker sw, Date now) throws SkillRunningError {
        //============================================================================================================//
        // 비동기화된 스킬
        for (SubSkill asyncSubSkill : state.asyncSubSkills) {
            if (asyncSubSkill.Act(smt, sw, now)){
                asyncSubSkill.state.clearChildren();
                state.asyncSubSkills.remove(asyncSubSkill);
            }
        }
        //============================================================================================================//
        // 동기화된 스킬
        if (state.syncSubSkill != null){
            boolean subskillResult = state.syncSubSkill.Act(smt, sw, now);
            // 스킬 미종료시
            if (!subskillResult){
                return false;
            }
            state.syncSubSkill.state.clearChildren();
            state.syncSubSkill = null;
        }
        //============================================================================================================//
        // 초기화
        this.state.tickExecution = true;
        // 재시작 요청이 있었는지 확인
        if (this.state.pauseReservation != null){
            if (this.state.pauseReservation.before(now)){
                this.state.pauseReservation = null;
            }else {
                return false;
            }
        }
        //================================================================
        // 실제 실행 부분
        this.state.currentTime = new Date().getTime();
        for (int i = 0; i < model.options.MaximumOneTickFrame && this.state.skillActivation && this.state.tickExecution; i++) {
            this.ActWhile(smt, sw);
        }
        //============================================================================================================//
        // 서브스킬이 남아있으면 중단 못함
        if (state.asyncSubSkills.size() > 0 || state.syncSubSkill != null){
            return false;
        }
        // sa 플레그가 설정되면 전부 재생 이전에 종료
        if (!state.skillActivation){
            return true;
        }
        // 마지막 프레임까지 재생했는지 확인
        if (state.executionframe < state.framelength) {
            return false;
        }
        return true;
    }
    private void ActWhile(SimulatorMultiTask smt, SimulatorWorker sw) throws SkillRunningError {
        // 재생할 프레임 불러옴
        BaseFrame exe = model.frames[state.executionframe];
        // 실행 프레임 카운트 올림
        state.executionframe++; //  실행중 스테이트 변경으로 실행 프레임이 변경 될 수 있으므로 여기에 위치
        // 재생함
        exe.Do(smt, smt.sl, sw, this, this.state);
        // 재생후
        if (this.state.error != null) {
            throw new SkillRunningError(this.state.erroredFrame, this.state.error);
        }
        // 프레임 종료 확인
        if (this.state.executionframe >= this.state.framelength){
            this.state.skillActivation = false;
        }
    }
    public int length() {
        return model.length();
    }
}
