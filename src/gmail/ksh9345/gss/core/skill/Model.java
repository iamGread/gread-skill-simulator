package gmail.ksh9345.gss.core.skill;

import gmail.ksh9345.gss.core.frames.BaseFrame;

import java.nio.file.Path;
import java.util.List;

/**
 * Created by ksh93 on 2017-01-24.
 */
public class Model {
    // 동작 관련 정보
    public final BaseFrame[] frames;
    // 스킬 설명 구조
    private String nameorigin;
    private String name;
    private Path from;
    private String description;
    // 옵션 정보
    public final Option options;
    // 스킬 런타임 정보
    public final Statistic statistics;

    public Model(List<BaseFrame> frames, Option options, String name, String description) {
        this.frames = frames.toArray(new BaseFrame[frames.size()]);
        this.options = options;
        this.name = name.replace(' ', '_');
        this.nameorigin = name;
        this.description = description;
        statistics = new Statistic();
    }
    //

    public String getName() {
        return name;
    }
    public String getNameOrigin() {
        return nameorigin;
    }


    public int length(){
        return frames.length;
    }
    public String getDescription() {
        return description;
    }
    //

}
