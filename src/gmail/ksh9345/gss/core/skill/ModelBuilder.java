package gmail.ksh9345.gss.core.skill;

import com.sun.istack.internal.NotNull;
import gmail.ksh9345.gss.core.frames.BaseFrame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ksh93 on 2017-02-11.
 */
public class ModelBuilder {
    @FunctionalInterface
    public interface LabeledFrameRequest {
        @NotNull
        public BaseFrame request(int labeled);
    }
    Map<String, Object> nametable;
    Map<Object, String> requesttable;
    List<Object> frametable;
    //
    Option options;
    String name;
    String description;

    public Option getOptions() {
        return options;
    }

    public void setOptions(Option options) {
        this.options = options;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    //

    public ModelBuilder(String name) {
        this.name = name;
        //
        nametable = new HashMap<>();
        requesttable = new HashMap<>();
        frametable = new ArrayList<>();
        options = new Option();
        description = "";
    }

    public void addFrame(@NotNull BaseFrame bf){
        frametable.add(bf);
    }
    public void addFrame(String label, @NotNull BaseFrame bf){
        nametable.put(label, bf);
        frametable.add(bf);
    }
    public void addFrame(String label, @NotNull LabeledFrameRequest lbd){
        requesttable.put(lbd, label);
        frametable.add(lbd);
    }
    public void addFrame(String findlabel, String registlabel, @NotNull LabeledFrameRequest lbd){
        requesttable.put(lbd, findlabel);
        nametable.put(registlabel, lbd);
        frametable.add(lbd);
    }

    //
    public Model Build(){
        List<BaseFrame> temp = new ArrayList<>();
        for (Object o: frametable){
            if (o instanceof BaseFrame){
                temp.add((BaseFrame) o);
            }else if (o instanceof LabeledFrameRequest){
                String find = requesttable.get(o);
                int index = frametable.indexOf(nametable.get(find));
                temp.add(((LabeledFrameRequest) o).request(index));
            }
        }
        return new Model(temp, options, name, description);
    }
}