package gmail.ksh9345.gss.core.simulator;

import gmail.ksh9345.gss.core.Errors.SkillRunningError;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.lambdas.GCCALL;

import java.util.Date;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by ksh93 on 2017-02-08.
 */
public class SimulatorWorker implements Runnable {
    SimulatorMultiTask smt;
    //

    //
    private CopyOnWriteArrayList<Skill> working;
    //
    private long burden;
    private int refreshInterval;
    private int refreshCount;
    private GCCALL gccall;

    SimulatorWorker(SimulatorMultiTask smt, int refreshInterval, GCCALL gccall) {
        this.smt = smt;
        this.working = new CopyOnWriteArrayList<>();
        this.burden = 0;
        this.refreshInterval = refreshInterval;
        this.refreshCount = 0;
        this.gccall = gccall;
    }
    void request(Skill sk){
        working.add(sk);

    }
    private void callGC(Skill sk){
        gccall.run(sk);
        working.remove(sk);
    }
    private void refreshScale(){
        long temp = 0;
        for (Skill skill : working) {
            temp += skill.model.statistics.getAverageRuntime();
        }
        this.setBurden(temp);
    }
    public long getBurden() {
        return burden;
    }
    public int getReadySkillSize(){
        return working.size();
    }
    private synchronized void setBurden(long burden) {
        this.burden = burden;
    }

    @Override
    public void run() {
        if (working.size() == 0) {
            return;
        }
        //========================================================//
        // 작업 부하 측정
        if (refreshCount > refreshInterval){
            refreshCount = 0;
            refreshScale();
        }
        refreshCount ++;
        //========================================================//
        // 스킬 시뮬레이팅
        for (Skill sk : working) {
            Date now = new Date();
            // 스킬의 최대 생존시간 넘김
            if (sk.state.startTime + sk.model.options.MaximumAlive < now.getTime()){
                callGC(sk);
            }else {
                try {
                    // 스킬 프레임 모두 순환 완료시 스킬 액티브 리스트에서 제외
                    if (sk.Act(smt, this, now)) {
                        callGC(sk);
                    }
                } catch (SkillRunningError e) {
                    e.printStackTrace();
                    callGC(sk);
                }
            }
        }
        //========================================================//
    }
}
