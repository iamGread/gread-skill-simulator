package gmail.ksh9345.gss.core.simulator;

import com.google.common.collect.Lists;
import gmail.ksh9345.gss.core.simulator.hook.Hook;
import gmail.ksh9345.gss.sponge.PEP;
import gmail.ksh9345.gss.sponge.config.ConfigListener;
import gmail.ksh9345.gss.sponge.listener.*;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.entity.DamageEntityEvent;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.item.inventory.DropItemEvent;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.scheduler.Scheduler;
import org.spongepowered.api.scheduler.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class SimulatorListener {
    public SimulatorListener(SimulatorMultiTask smt) {
        this.smt = smt;
    }

    SimulatorMultiTask smt;
    //==================================================================================================================
    private ConcurrentHashMap<Entity, CopyOnWriteArrayList<Hook>> eventmap;
    //==================================================================================================================
    public void reqist(Entity et, Hook hook){
        synchronized (eventmap){
            CopyOnWriteArrayList<Hook> temp = eventmap.get(et);
            if (temp != null){
                temp.add(hook);
            }else {
                temp = new CopyOnWriteArrayList<>();
                temp.add(hook);
                eventmap.put(et, temp);
            }
        }
    }
    //==================================================================================================================
    boolean active(Entity e, SimulatorEventList sel, List<Entity> entities ,Object ... args){
        boolean ret = false;
        List<Hook> evs = eventmap.get(e);
        if (evs != null){
            if (sel.equals(SimulatorEventList.NO_FALL_DAMAGE)){
                for (Hook ev : evs) {
                    System.out.println(ev);
                    if (ev.getEventType().equals(sel)){
                        if (ev.active(args)){
                            if (ev.getSkill().state.active(ev.getFrameCount(), entities, false)){
                                ret = true;
                            }else {
                                evs.remove(ev);
                            }
                        }
                    }
                }
            }else {
                for (Hook ev : evs) {
                    if (ev.getEventType().equals(sel)){
                        if (ev.active(args)){
                            if (!ev.getSkill().state.active(ev.getFrameCount(), entities, false)){
                                evs.remove(ev);
                                ret = false;
                            }else {
                                ret = true;
                            }
                        }
                    }
                }
            }
        }
        return ret;
    }
    void remove(Hook h){
        h.getSkill().state.active(h.getFrameCount(), Lists.newArrayList(), true);
    }
    //==================================================================================================================
    public Task listenerMain;
    public MidairMovement midairMovement;
    public GroundMovement groundMovement;
    public PreventMovement preventMovement;
    public ItemDropEvent itemDropEvent;
    public FallDamagedEvent fallDamagedEvent;
    public EntityHitEvent entityHitEvent;
    public LocationalEvent locationalEvent;
    //==================================================================================================================
    public void init(Scheduler sch, Object plugin, ConfigListener cl) {
        eventmap = new ConcurrentHashMap<>();
        //
        listenerMain = sch.createTaskBuilder()
                .interval(cl.getDefaultReserveSystemCheck(), TimeUnit.MILLISECONDS)
                .name(cl.getMainModuleName())
                .execute(new Runnable() {
                    @Override
                    public void run() {
                        // Reserve 관리
                        Date now = new Date();
                        List<Entity> gcs = new ArrayList<>();
                        for (Entity e : eventmap.keySet()) {
                            if (e.isRemoved()){
                                gcs.add(e);
                            }
                            List<Hook> hooks = eventmap.get(e);
                            for (Hook hook : hooks) {
                                if (hook.expire(now)){
                                    hook.getSkill().state.active(hook.getFrameCount(), Lists.newArrayList(), true);
                                    remove(hook);
                                    hooks.remove(hook);
                                }
                            }
                        }
                        for (Entity entity : gcs) {
                            eventmap.remove(entity);
                        }
                    }
                }).submit(plugin);
        //===================================================================================//
        midairMovement = new MidairMovement(
                e -> active(e, SimulatorEventList.MIDAIR_SNEAK, Lists.newArrayList(e)),
                e -> active(e, SimulatorEventList.MIDAIR_JUMP, Lists.newArrayList(e))
        );
        PEP.eventManager.registerListener(plugin, MoveEntityEvent.class, midairMovement);
        //===================================================================================//
        groundMovement = new GroundMovement(
                e -> active(e, SimulatorEventList.GROUND_SNEAK, Lists.newArrayList(e)),
                e -> active(e, SimulatorEventList.GROUND_MOVE, Lists.newArrayList(e))
        );
        PEP.eventManager.registerListener(plugin, MoveEntityEvent.class, groundMovement);
        //===================================================================================//
        preventMovement = new PreventMovement(
                e -> active(e, SimulatorEventList.FIX, Lists.newArrayList()),
                e -> active(e, SimulatorEventList.STOP, Lists.newArrayList())
        );
        PEP.eventManager.registerListener(plugin, MoveEntityEvent.class, preventMovement);
        //===================================================================================//
        itemDropEvent = new ItemDropEvent(
                e -> active(e, SimulatorEventList.DROP_BY_Q, Lists.newArrayList(e))
        );
        PEP.eventManager.registerListener(plugin, DropItemEvent.Dispense.class, itemDropEvent);
        //===================================================================================//
        fallDamagedEvent = new FallDamagedEvent(
                e -> active(e, SimulatorEventList.NO_FALL_DAMAGE, Lists.newArrayList()),
                e -> active(e, SimulatorEventList.FALL_DAMAGED, Lists.newArrayList(e))
        );
        PEP.eventManager.registerListener(plugin, DamageEntityEvent.class, fallDamagedEvent);
        //===================================================================================//
        entityHitEvent = new EntityHitEvent(
                (e, targets) -> {
                    try {
                        targets.add(0, e);
                        active(e, SimulatorEventList.ATTACKING, targets);
                    }catch (Exception ex){

                    }
                },
                (e, targets) -> {
                    try {
                        targets.add(0, e);
                        active(e, SimulatorEventList.ATTACKED, targets);
                    }catch (Exception ex){

                    }
                }
        );
        PEP.eventManager.registerListener(plugin, DamageEntityEvent.class, entityHitEvent);
        //===================================================================================//
        locationalEvent = new LocationalEvent(
                (e, args) -> active(e, SimulatorEventList.ENTER, Lists.newArrayList(e), args),
                (e, args) -> active(e, SimulatorEventList.SPEED, Lists.newArrayList(e), args)
        );
        PEP.eventManager.registerListener(plugin, MoveEntityEvent.class, locationalEvent);
    }
}


