package gmail.ksh9345.gss.core.simulator;

/**
 * Created by ksh93 on 2017-02-07.
 */
public enum SimulatorEventList {
    // 이벤트 제어형
    FIX,
    STOP,
    NO_FALL_DAMAGE,
    //==============================================================
    // 이벤트 발동형
    MIDAIR_JUMP,
    MIDAIR_SNEAK,
    GROUND_SNEAK,
    GROUND_MOVE,
    //
    DROP_BY_Q,
    //
    FALL_DAMAGED,
    //
    ATTACKING,
    ATTACKED,

    ENTER,
    SPEED,
    ;
}