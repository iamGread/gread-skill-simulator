package gmail.ksh9345.gss.core.simulator;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.Errors.NoPermisionForSkillError;
import gmail.ksh9345.gss.core.loader.Container;
import gmail.ksh9345.gss.core.skill.MeasuredSkill;
import gmail.ksh9345.gss.core.skill.Model;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.lambdas.SynchronizedTask;
import gmail.ksh9345.gss.sponge.PEP;
import gmail.ksh9345.gss.sponge.Permission;
import gmail.ksh9345.gss.sponge.Utils;
import gmail.ksh9345.gss.sponge.config.Config;
import gmail.ksh9345.gss.sponge.config.ConfigListener;
import gmail.ksh9345.gss.sponge.config.ConfigSimulator;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.scheduler.Scheduler;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by ksh93 on 2017-01-18.
 */
public class SimulatorMultiTask {
    Task simulator_main = null;
    Task action_main = null;
    SimulatorWorker[] sws;
    //
    public SimulatorListener sl;
    //
    CopyOnWriteArrayList<Skill> preDistribution = null; // 분배 이전의 스킬들
    CopyOnWriteArrayList<Skill> gc = null; // 종료후 스킬 정리 과정
    CopyOnWriteArrayList<SynchronizedTask> synchronizedTasks = null; // 동기화가 필요한 작업들을 위한 함수 목록
    // 분배를 위한 여유측정 간격
    int dispenseAtTime;
    //
    public void init(Scheduler sch, Object plug, Config conf) {
        ConfigSimulator cs = conf.configSimulator;
        ConfigListener cl = conf.configListener;
        //
        String taskname = cs.getMainModuleName();
        // 리스너 준비
        sl = new SimulatorListener(this);
        sl.init(sch, plug, cl);
        //
        // 객체 준비
        preDistribution = new CopyOnWriteArrayList<>();
        gc = new CopyOnWriteArrayList<>();
        synchronizedTasks = new CopyOnWriteArrayList<>();
        dispenseAtTime = cs.getDispenceAtTime();
        //===================================================================================
        // 워커 세팅
        // 동기적 게임 데이터 수정을 위함
        action_main = sch.createTaskBuilder()
                .intervalTicks(cs.getActionTick())
                .name(taskname + "[Synchronized Action]")
                .execute(new Runnable() {
                    @Override
                    public void run() {
                        for (SynchronizedTask synchronizedTask : synchronizedTasks) {
                            try {
                                synchronizedTask.run();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            synchronizedTasks.remove(synchronizedTask);
                        }
                    }
                })
                .submit(plug);

        sws = new SimulatorWorker[cs.getTaskWorker()];
        for (int i = 0; i < sws.length; i++) {
            sws[i] = new SimulatorWorker(this, cs.getRefreshInterval(), skill -> gc.add(skill));
            sch.createTaskBuilder()
                    .name(taskname + " [" + i + "]")
                    .execute(sws[i])
                    .intervalTicks(cs.getOneTickSet())
                    .async()
                    .submit(plug);
        }
        //
        //===================================================================================
        // 메인태스크 준비
        // 메인태스크 동작(GC, 분배)
        simulator_main = sch.createTaskBuilder()
                .intervalTicks(cs.getOneTickSet())
                .name(taskname)
                .async()
                .execute(new Runnable() {
                    @Override
                    public void run() {
                        // 작업 분배
                        int dispenceCount = 0;
                        // 첫번째 여유로운 워커를 찾음
                        SimulatorWorker lowBurdenWorker = null;
                        lowBurdenWorker = sws[0];
                        for (SimulatorWorker simulateWorker : sws) {
                            if (lowBurdenWorker.getBurden() > simulateWorker.getBurden()) {
                                lowBurdenWorker = simulateWorker;
                            }
                        }
                        // 작업 분배
                        for (Skill skill : preDistribution) {
                            // 분배 회차당 분배가 끝났으면 다시 가장 여유로운 워커를 찾음
                            if (dispenceCount > dispenseAtTime) {
                                lowBurdenWorker = sws[0];
                                for (SimulatorWorker simulateWorker : sws) {
                                    if (lowBurdenWorker.getBurden() > simulateWorker.getBurden()) {
                                        lowBurdenWorker = simulateWorker;
                                    }
                                }
                                dispenceCount = 0;
                            }
                            // 워커에게 작업 분배 전 이번에 동작시간 측정이 필요한지 확인
                            if (skill.model.statistics.needInfo()) {
                                // 동작시간이 측정되는 작업 분배
                                lowBurdenWorker.request(new MeasuredSkill(skill));
                            } else {
                                // 작업 분배
                                lowBurdenWorker.request(skill);
                            }
                            preDistribution.remove(skill);
                            dispenceCount++;
                        }
                        // 가비지 컬렉팅
                        for (Skill skill : gc) {
                            skill.state.clearChildren();
                            if (skill instanceof MeasuredSkill) {
                                skill.model.statistics.sendRuntime(((MeasuredSkill) skill).getRuntime());
                            }
                            PEP.consoleSource.sendMessage(Utils.printMessage("GC", TextColors.GREEN, skill.toString() + " is Finished"));
                            gc.remove(skill);
                        }
                    }
                })
                .submit(plug);

        //===================================================================================
        // 워커 준비

    }

    //================================================================================================================================
    // 리퀘스트 계통
    public boolean Request(String skillname, Living lv) throws NoPermisionForSkillError{

        Model sm = Container.Get(skillname);
        if (sm == null) {
            return false;
        }
        //
        this.Request(new Skill(sm, lv));
        return true;
    }
    public boolean Request(String skillname, Living lv, List<Entity> targets) throws NoPermisionForSkillError{

        Model sm = Container.Get(skillname);
        if (sm == null) {
            return false;
        }
        //
        this.Request(new Skill(sm, lv, targets));
        return true;
    }
    public boolean Request(String skillname, Living lv, Location<World> loc, Vector3d rot, String... args) throws NoPermisionForSkillError{

        Model sm = Container.Get(skillname);
        if (sm == null) {
            return false;
        }
        //
        this.Request(new Skill(sm, lv, loc, rot));
        return true;
    }
    public boolean Request(String skillname, Living lv, Location<World> loc, Vector3d rot, List<Entity> targets, String... args) throws NoPermisionForSkillError{

        Model sm = Container.Get(skillname);
        if (sm == null) {
            return false;
        }
        //
        this.Request(new Skill(sm, lv, loc, rot, targets));
        return true;
    }
    public void Request(Skill sk) throws NoPermisionForSkillError{
        if (sk == null) {
            return;
        }
        if (sk.model.options.Permission == null){
            preDistribution.add(sk);
        }else {
            String permission = sk.model.options.RealPermision();
            if (sk.state.caster instanceof Subject){
                Subject sub = (Subject) sk.state.caster;
                if (sub.hasPermission(permission)){
                    preDistribution.add(sk);
                    return;
                }
            }
            throw new NoPermisionForSkillError(permission);
        }
    }
    //================================================================================================================================
    // 동기화 작업 요청
    public void synchronizeRequest(SynchronizedTask st) {
        if (st != null) {
            synchronizedTasks.add(st);
        }else {
            PEP.logger.warn("Synchronize Request can't process null");
        }
    }
    //================================================================================================================================
    // 서브프로세스 관련
    public int WorkersLength(){
        return sws.length;
    }
    public SimulatorWorker Worker(int index){
        if (sws.length > index){
            return sws[index];
        }
        return null;
    }
    //================================================================================================================================
    // GC 관련
    public int GCLength(){
        return gc.size();
    }
    //================================================================================================================================
    // 동기화된 작업들 관련
    public int SyncReqRemain(){
        return synchronizedTasks.size();
    }
    //================================================================================================================================
    // 미분배 스킬들
    public int UnreadySkill(){
        return preDistribution.size();
    }

}
