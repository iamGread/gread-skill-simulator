package gmail.ksh9345.gss.core.simulator.hook;

/**
 * Created by ksh93 on 2017-02-12.
 */
@FunctionalInterface
public interface Conditional {
    public boolean run(Object ... args);
}
