package gmail.ksh9345.gss.core.simulator.hook;

import gmail.ksh9345.gss.core.simulator.SimulatorEventList;
import gmail.ksh9345.gss.core.skill.Skill;

import java.util.Date;

/**
 * Created by ksh93 on 2017-02-12.
 */
public interface Hook {
    public SimulatorEventList getEventType();
    public Skill getSkill();
    public int getFrameCount();
    //
    public boolean active(Object ... args);
    public boolean next();
    public boolean expire(Date time);
}
