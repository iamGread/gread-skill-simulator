package gmail.ksh9345.gss.core.simulator.hook;

import gmail.ksh9345.gss.core.simulator.SimulatorEventList;
import gmail.ksh9345.gss.core.skill.Skill;

import java.util.Date;

/**
 * Created by ksh93 on 2017-02-12.
 */
public class SingleHook implements Hook{
    Skill skill;
    int frame;
    Date expi;
    SimulatorEventList sel;

    public SingleHook(SimulatorEventList sel, Skill skill, int frame, Date expi) {
        this.skill = skill;
        this.frame = frame;
        this.expi = expi;
        this.sel = sel;
    }

    @Override
    public SimulatorEventList getEventType() {
        return sel;
    }

    @Override
    public Skill getSkill() {
        return skill;
    }

    @Override
    public int getFrameCount() {
        return frame;
    }

    @Override
    public boolean active(Object... args) {
        return true;
    }

    @Override
    public boolean next() {
        return false;
    }

    @Override
    public boolean expire(Date time) {
        return time.after(expi);
    }
}
