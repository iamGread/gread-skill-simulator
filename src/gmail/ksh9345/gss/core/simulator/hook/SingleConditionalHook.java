package gmail.ksh9345.gss.core.simulator.hook;

import gmail.ksh9345.gss.core.simulator.SimulatorEventList;
import gmail.ksh9345.gss.core.skill.Skill;

import java.util.Date;

/**
 * Created by ksh93 on 2017-02-12.
 */
public class SingleConditionalHook extends SingleHook{
    Conditional cond;

    public SingleConditionalHook(SimulatorEventList sel, Skill skill, int frame, Date expi, Conditional cond) {
        super(sel, skill, frame, expi);
        this.cond = cond;
    }

    @Override
    public boolean active(Object... args) {
        return cond.run(args);
    }
}
