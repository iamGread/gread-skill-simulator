package gmail.ksh9345.gss.core.frames.listen;

import gmail.ksh9345.gss.core.Errors.ControlMapError;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorEventList;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.simulator.hook.SingleConditionalHook;
import gmail.ksh9345.gss.core.simulator.hook.SingleHook;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.skill.control.ControlData;
import gmail.ksh9345.gss.core.skill.control.ControlListenerCounter;
import gmail.ksh9345.gss.core.skill.control.ListenerInner;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Date;

/**
 * Created by ksh93 on 2017-02-06.
 */
public class WaitEnter implements BaseFrame{
    private class WaitEnterControlData extends ControlListenerCounter {
        public WaitEnterControlData(long expire, int counter) {
            super(expire, counter);
        }
    }
    public enum Mode{
        SKILL,
        CASTER,
        FIRST_TARGET,
    }
    public enum TargetSelect {
        CASTER,
        TARGETS,
    }
    public Mode md;
    public TargetSelect ts;
    public int ifActive;
    public int ifExpire;
    public int after;
    public int count;
    public double length;

    public WaitEnter(Mode md, TargetSelect ts, int ifActive, int ifExpire, int after, int count, double length) {
        this.md = md;
        this.ts = ts;
        this.ifActive = ifActive;
        this.ifExpire = ifExpire;
        this.after = after;
        this.count = count;
        this.length = length;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        SimulatorEventList sel = SimulatorEventList.ENTER;
        Location<World> pivot;
        switch (md){
            default:
            case SKILL:
                pivot = st.location.copy();
                break;
            case CASTER:
                pivot = st.caster.getLocation().copy();
                break;
            case FIRST_TARGET:
                if (st.targets.size()  < 1){
                    return;
                }
                pivot = st.targets.get(0).getLocation().copy();
                break;
        }
        int cur = st.getCurrentFrame();
        ControlData cd = st.controlmap.get(cur);
        WaitEnterControlData wrscd;
        if (cd == null){
            wrscd = new WaitEnterControlData(st.currentTime + after, count);
            st.controlmap.put(cur, wrscd);
            switch (ts){
                case CASTER:
                    sl.reqist(st.caster, new SingleConditionalHook(sel, sk, cur, new Date(st.currentTime + after),
                            args -> {
                        Location<World> lw = (Location<World>)args[0];
                        return lw.getExtent().equals(pivot.getExtent()) && lw.getPosition().distance(pivot.getPosition()) < length;
                    }));
                    break;
                case TARGETS:
                    for (Entity target : st.targets) {
                        sl.reqist(target, new SingleConditionalHook(sel, sk, cur, new Date(st.currentTime + after), args -> {
                            Location<World> lw = (Location<World>)args[0];
                            return lw.getExtent().equals(pivot.getExtent()) && lw.getPosition().distance(pivot.getPosition()) < length;
                        }));
                    }
                    break;
            }
        }else if (cd instanceof WaitEnterControlData){
            wrscd = (WaitEnterControlData) cd;
            if (wrscd.remain(st.currentTime)){
                ListenerInner li = wrscd.next();
                if (li != null) {
                    st.targetStack.push(st.targets);
                    st.targets = li.data;
                    if (li.expire) {
                        st.jump(ifExpire);
                        wrscd.expire();
                    } else {
                        st.jump(ifActive);
                        if (wrscd.counter()){
                            switch (ts){
                                case CASTER:
                                    sl.reqist(st.caster, new SingleConditionalHook(sel, sk, cur, new Date(st.currentTime + after),
                                            args -> {
                                                Location<World> lw = (Location<World>)args[0];
                                                return lw.getExtent().equals(pivot.getExtent()) && lw.getPosition().distance(pivot.getPosition()) < length;
                                            }));
                                    break;
                                case TARGETS:
                                    for (Entity target : st.targets) {
                                        sl.reqist(target, new SingleConditionalHook(sel, sk, cur, new Date(st.currentTime + after), args -> {
                                            Location<World> lw = (Location<World>)args[0];
                                            return lw.getExtent().equals(pivot.getExtent()) && lw.getPosition().distance(pivot.getPosition()) < length;
                                        }));
                                    }
                                    break;
                            }
                        }else {
                            wrscd.expire();
                        }
                    }
                }
            }else {
                st.jump(ifExpire);
                wrscd.expire();
            }
        }else {
            st.setError(new ControlMapError("WaitEnterControlData", cd, cur));
        }
    }
}
