package gmail.ksh9345.gss.core.frames.listen;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorEventList;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.simulator.hook.UnlitmitedHook;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.Entity;

import java.util.Date;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class NoFallDamage implements BaseFrame{
    public int after;
    public int count;

    public NoFallDamage(int after, int count) {
        this.after = after;
        if (count < 1){
            this.count = -1;
        }
    }
    public NoFallDamage(int after) {
        this.after = after;
        this.count = 1;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        for (Entity target : st.targets) {
            sl.reqist(target, new UnlitmitedHook(SimulatorEventList.NO_FALL_DAMAGE, sk, -1, new Date(st.currentTime + after)));
        }
    }
}
