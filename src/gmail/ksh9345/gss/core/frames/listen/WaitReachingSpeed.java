package gmail.ksh9345.gss.core.frames.listen;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.Errors.ControlMapError;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorEventList;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.simulator.hook.SingleConditionalHook;
import gmail.ksh9345.gss.core.simulator.hook.SingleHook;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.skill.control.ControlData;
import gmail.ksh9345.gss.core.skill.control.ControlListenerCounter;
import gmail.ksh9345.gss.core.skill.control.ListenerInner;
import org.spongepowered.api.entity.Entity;

import java.util.Date;

/**
 * Created by ksh93 on 2017-02-06.
 */
public class WaitReachingSpeed implements BaseFrame{
    private class WaitReachingSpeedControlData extends ControlListenerCounter {
        public WaitReachingSpeedControlData(long expire, int counter) {
            super(expire, counter);
        }
    }
    public enum TargetSelect {
        CASTER,
        TARGETS,
    }
    public TargetSelect ts;
    public int ifActive;
    public int ifExpire;
    public int after;
    public int count;
    public double speed;
    public WaitReachingSpeed(TargetSelect ts, int ifActive, int ifExpire, int after, int count, double speed) {
        this.ts = ts;
        this.ifActive = ifActive;
        this.ifExpire = ifExpire;
        this.after = after;
        this.count = count;
        this.speed = speed;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        SimulatorEventList sel = SimulatorEventList.SPEED;
        int cur = st.getCurrentFrame();
        ControlData cd = st.controlmap.get(cur);
        WaitReachingSpeedControlData wrscd;
        if (cd == null){
            wrscd = new WaitReachingSpeedControlData(st.currentTime + after, count);
            st.controlmap.put(cur, wrscd);
            switch (ts){
                case CASTER:
                    sl.reqist(st.caster, new SingleConditionalHook(sel, sk, cur, new Date(st.currentTime + after), args -> ((Vector3d)args[0]).length() > speed));
                    break;
                case TARGETS:
                    for (Entity target : st.targets) {
                        sl.reqist(target, new SingleConditionalHook(sel, sk, cur, new Date(st.currentTime + after), args -> ((Vector3d)args[0]).length() > speed));
                    }
                    break;
            }
        }else if (cd instanceof WaitReachingSpeedControlData){
            wrscd = (WaitReachingSpeedControlData) cd;
            if (wrscd.remain(st.currentTime)){
                ListenerInner li = wrscd.next();
                if (li != null) {
                    st.targetStack.push(st.targets);
                    st.targets = li.data;
                    if (li.expire) {
                        st.jump(ifExpire);
                        wrscd.expire();
                    } else {
                        st.jump(ifActive);
                        if (wrscd.counter()){
                            switch (ts){
                                case CASTER:
                                    sl.reqist(st.caster, new SingleConditionalHook(sel, sk, cur, new Date(st.currentTime + after), args -> ((Vector3d)args[0]).length() > speed));
                                    break;
                                case TARGETS:
                                    for (Entity target : st.targets) {
                                        sl.reqist(target, new SingleConditionalHook(sel, sk, cur, new Date(st.currentTime + after), args -> ((Vector3d)args[0]).length() > speed));
                                    }
                                    break;
                            }
                        }else {
                            wrscd.expire();
                        }
                    }
                }
            }else {
                st.jump(ifExpire);
                wrscd.expire();
            }
        }else {
            st.setError(new ControlMapError("WaitReachingSpeedControlData", cd, cur));
        }


    }
}
