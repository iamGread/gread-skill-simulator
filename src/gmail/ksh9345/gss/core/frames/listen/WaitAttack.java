package gmail.ksh9345.gss.core.frames.listen;

import gmail.ksh9345.gss.core.Errors.ControlMapError;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorEventList;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.simulator.hook.SingleHook;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.skill.control.ControlData;
import gmail.ksh9345.gss.core.skill.control.ControlListenerCounter;
import gmail.ksh9345.gss.core.skill.control.ListenerInner;
import org.spongepowered.api.entity.Entity;

import java.util.Date;

/**
 * Created by ksh93 on 2017-02-06.
 */
public class WaitAttack implements BaseFrame{
    private class WaitAttackControlData extends ControlListenerCounter {
        public WaitAttackControlData(long expire, int counter) {
            super(expire, counter);
        }
    }
    public enum Type{
        ATTACKING,
        ATTACKED,
        ;
    }
    public enum Mode{
        CASTER,
        TARGETS
    }
    public Type tp;
    public Mode md;
    public int ifActive;
    public int ifExpire;
    public int after;
    public int count;

    public WaitAttack(Type tp, Mode md, int ifActive, int ifExpire, int after, int count) {
        this.tp = tp;
        this.md = md;
        this.ifActive = ifActive;
        this.ifExpire = ifExpire;
        this.after = after;
        this.count = count;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        SimulatorEventList sel;
        switch (tp){
            default:
            case ATTACKING:
                sel = SimulatorEventList.ATTACKING;
                break;
            case ATTACKED:
                sel = SimulatorEventList.ATTACKED;
                break;
        }
        int cur = st.getCurrentFrame();
        ControlData cd = st.controlmap.get(cur);
        WaitAttackControlData wrscd;
        if (cd == null) {
            wrscd = new WaitAttackControlData(st.currentTime + after, count);
            st.controlmap.put(cur, wrscd);
            switch (md) {
                case CASTER:
                    sl.reqist(st.caster, new SingleHook(sel, sk, cur, new Date(st.currentTime + after)));
                    break;
                case TARGETS:
                    for (Entity target : st.targets) {
                        sl.reqist(target, new SingleHook(sel, sk, cur, new Date(st.currentTime + after)));
                    }
                    break;
            }
        } else if (cd instanceof WaitAttackControlData) {
            wrscd = (WaitAttackControlData) cd;

            if (wrscd.remain(st.currentTime)){
                ListenerInner li = wrscd.next();
                if (li != null) {
                    st.targetStack.push(st.targets);
                    st.targets = li.data;
                    if (li.expire) {
                        st.jump(ifExpire);
                        wrscd.expire();
                    } else {
                        st.jump(ifActive);
                        if (wrscd.counter()){
                            switch (md) {
                                case CASTER:
                                    sl.reqist(st.caster, new SingleHook(sel, sk, cur, new Date(st.currentTime + after)));
                                    break;
                                case TARGETS:
                                    for (Entity target : st.targets) {
                                        sl.reqist(target, new SingleHook(sel, sk, cur, new Date(st.currentTime + after)));
                                    }
                                    break;
                            }
                        }else {
                            wrscd.expire();
                        }
                    }
                }
            }else {
                st.jump(ifExpire);
                wrscd.expire();
            }
        } else {
            st.setError(new ControlMapError("WaitAttackControlData", cd, cur));
        }
    }
}
