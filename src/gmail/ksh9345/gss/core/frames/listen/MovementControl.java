package gmail.ksh9345.gss.core.frames.listen;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorEventList;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.simulator.hook.UnlitmitedHook;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.Entity;

import java.util.Date;

/**
 * 대상 앤티티를 고정합니다.
 * Created by ksh93 on 2017-02-01.
 */
public class MovementControl implements BaseFrame {
    public boolean fixLotation;
    public int after;
    public int count;

    public MovementControl(boolean fixLotation, int after, int count) {
        this.fixLotation = fixLotation;
        this.after = after;
        if (count < 1){
            this.count = -1;
        }
    }
    public MovementControl(boolean fixLotation, int after) {
        this.fixLotation = fixLotation;
        this.after = after;
        this.count = 1;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        SimulatorEventList sel;

        if (fixLotation){
            sel = SimulatorEventList.FIX;
        }else {
            sel = SimulatorEventList.STOP;
        }
        //
        for (Entity target : st.targets) {
            sl.reqist(target, new UnlitmitedHook(sel, sk, -1, new Date(st.currentTime + after)));
        }
    }
}
