package gmail.ksh9345.gss.core.frames.movement;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.system.CoordinateSystem;
import gmail.ksh9345.gss.core.system.VectorRotationSystem;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;

/**
 * Created by ksh93 on 2017-02-13.
 */
public class Turn implements BaseFrame {
    public enum Mode {
        CASTER_LOCATION,
        FIRST_TARGET_LOCATION,
    }
    Mode md;
    double percent;

    public Turn(Mode md, double percent) {
        this.md = md;
        this.percent = percent;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {

        Vector3d from = new Vector3d(st.location.getPosition());
        Vector3d to;
        switch (this.md){
            default:
            case CASTER_LOCATION:
                to = st.caster.getLocation().getPosition();
                break;
            case FIRST_TARGET_LOCATION:
                if (st.targets.size() < 1){
                    return;
                }
                Entity e = st.targets.get(0);
                to = e.getLocation().getPosition();
                break;
        }
        try {
            Vector3d torot = to.sub(from).normalize();
            Vector3d rot = VectorRotationSystem.rotationToNormalVector(st.rotation);
            Vector3d temp3 = torot.sub(rot);
            st.rotation = VectorRotationSystem.vectorToRotation(rot.add(temp3.mul(percent)));
        }catch (ArithmeticException e){
            return;
        }
    }

}
