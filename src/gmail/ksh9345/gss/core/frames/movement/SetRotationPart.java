package gmail.ksh9345.gss.core.frames.movement;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;

/**
 * Created by ksh93 on 2017-04-19.
 */
public class SetRotationPart implements BaseFrame {
    public enum Part{
        THETA,
        PHI,
    }
    public enum Mode{
        SET,
        ADD,
        MUL
    }


    Part witch;
    Mode mode;
    double to;

    public SetRotationPart(Part witch, double to) {
        this.witch = witch;
        this.mode = Mode.SET;
        this.to = to;
    }
    public SetRotationPart(Part witch, Mode mode, double to) {
        this.witch = witch;
        this.mode = mode;
        this.to = to;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        double temp;
        switch (witch){
            default:
            case THETA:
                temp = st.rotation.getY();
                break;
            case PHI:
                temp = st.rotation.getX();
                break;
        }
        switch (mode){
            default:
            case SET:
                temp = to;
                break;
            case ADD:
                temp += to;
                break;
            case MUL:
                temp *= to;
                break;
        }
        switch (witch){
            default:
            case THETA:
                st.rotation = new Vector3d(st.rotation.getX(), temp, 0);
                break;
            case PHI:
                st.rotation = new Vector3d(temp, st.rotation.getY(), 0);
                break;
        }
    }
}