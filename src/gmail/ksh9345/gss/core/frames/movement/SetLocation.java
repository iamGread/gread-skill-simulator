package gmail.ksh9345.gss.core.frames.movement;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.system.CoordinateSystem;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;

/**
 * Created by ksh93 on 2017-02-08.
 */
public class SetLocation implements BaseFrame {
    public enum Mode{
        SKILL,
        CASTER,
        FIRST_TARGET
    }
    Mode md;
    CoordinateSystem cs;
    Vector3d arg;

    public SetLocation(Mode md, CoordinateSystem cs, Vector3d arg) {
        this.md = md;
        this.cs = cs;
        this.arg = arg;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Vector3d temp;
        switch (md){
            default:
            case SKILL:
                temp = cs.transform(st.location.getPosition(), st.rotation, arg);
                st.location = st.location.setPosition(temp);
                break;
            case CASTER:
                temp = cs.transform(st.caster.getLocation().getPosition(), st.caster.getHeadRotation(), arg);
                st.location = st.location.setPosition(temp);
                break;
            case FIRST_TARGET:
                if (st.targets.size() < 1){
                    return;
                }
                Entity entity = st.targets.get(0);
                if (entity instanceof Living){
                    temp = cs.transform(entity.getLocation().getPosition(), ((Living) entity).getHeadRotation(), arg);
                    st.location = st.location.setPosition(temp);
                }else {
                    temp = cs.transform(entity.getLocation().getPosition(), entity.getRotation(), arg);
                    st.location = st.location.setPosition(temp);
                }
                break;
        }
    }
}
