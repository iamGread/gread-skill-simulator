package gmail.ksh9345.gss.core.frames.movement;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;

/**
 * Created by ksh93 on 2017-02-08.
 */
public class SetRotation implements BaseFrame{
    public enum Mode{
        SET,
        SKILL,
        CASTER,
        FIRST_TARGET
    }
    Mode md;
    double theta;
    double phi;

    public SetRotation(Mode md, Vector3d rot) {
        this.md = md;
        this.theta = rot.getY();
        this.phi = -rot.getX();
    }
    public SetRotation(Mode md, double phi, double theta) {
        this.md = md;
        this.theta = theta;
        this.phi = -phi;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Vector3d temp;
        switch (md){
            default:
            case SET:
                st.rotation = new Vector3d(phi, theta, 0);
            case SKILL:
                st.rotation = st.rotation.add(phi, theta, 0);
                break;
            case CASTER:
                temp = st.caster.getHeadRotation();
                temp.add(phi, theta, 0);
                st.rotation = temp;
                break;
            case FIRST_TARGET:
                if (st.targets.size() < 1){
                    return;
                }
                Entity entity = st.targets.get(0);
                if (entity instanceof Living){
                    temp = ((Living) entity).getHeadRotation();
                    temp.add(phi, theta, 0);
                    st.rotation = temp;
                }else {
                    temp = entity.getRotation();
                    temp.add(phi, theta, 0);
                    st.rotation = temp;
                }
                break;
        }
    }
}
