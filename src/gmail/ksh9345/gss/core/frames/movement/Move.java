package gmail.ksh9345.gss.core.frames.movement;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.system.CoordinateSystem;
import gmail.ksh9345.gss.core.system.VectorRotationSystem;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.world.Location;

/**
 * SKILL_DIRECTION 모드에서 arg는 ROTATION 모드로 동작함
 * 그 외에는 좌표계로서 동작함
 * Created by ksh93 on 2017-02-01.
 */
public class Move implements BaseFrame {
    public enum Mode {
        SKILL_DIRECTION,
        CASTER_LOCATION,
        FIRST_TARGET_LOCATION,
    }

    public Mode md;
    public double length;
    public CoordinateSystem cs;
    public Vector3d arg;
    public Move(Mode md, double length, CoordinateSystem cs, Vector3d arg) {
        this.md = md;
        this.length = length;
        this.cs = cs;
        this.arg = arg;
    }
    public Move(Mode md, double length) {
        this.md = md;
        this.length = length;
        this.cs = CoordinateSystem.RELATIVE_CORDINATE;
        this.arg = Vector3d.ZERO;
    }
    public Move(double length) {
        this.md = Mode.SKILL_DIRECTION;
        this.length = length;
        this.cs = CoordinateSystem.RELATIVE_CORDINATE;
        this.arg = Vector3d.ZERO;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Vector3d temp;
        Vector3d temp_skill = st.location.getPosition();
        Location to = null;
        switch (this.md) {
            default:
            case SKILL_DIRECTION:
                to = st.location.copy().setPosition(
                        temp_skill.add(
                                moveadd(st.rotation, arg, length)
                        )
                );
                break;
            case CASTER_LOCATION:
                temp = cs.transform(st.caster.getLocation().getPosition(), st.caster.getHeadRotation(), arg);
                temp = temp.sub(temp_skill);
                temp = temp.normalize().mul(length);
                to = st.location.copy().setPosition(temp_skill.add(temp));
                break;
            case FIRST_TARGET_LOCATION:
                if (st.targets.size() < 1){
                    return;
                }
                Entity e = st.targets.get(0);
                if (e instanceof Living){
                    temp = cs.transform(e.getLocation().getPosition(), ((Living) e).getHeadRotation(), arg);
                }else {
                    temp = cs.transform(e.getLocation().getPosition(), e.getRotation(), arg);
                }

                temp = temp.sub(temp_skill);
                temp = temp.normalize().mul(length);
                to = st.location.copy().setPosition(temp_skill.add(temp));
                break;
        }
        st.location = to;
    }

    private Vector3d moveadd(Vector3d angle, Vector3d angle2, double length) {
        double theta = Math.toRadians(angle.getY() + angle2.getY()) + Math.PI/2;
        double phi = Math.toRadians(angle.getX() + angle2.getX());
        //
        double y = - 1 * Math.sin(phi);
        double xz = 1 * Math.cos(phi);
        double x = xz * Math.cos(theta);
        double z = xz * Math.sin(theta);
        return new Vector3d(x, y, z).mul(length);
    }
}
