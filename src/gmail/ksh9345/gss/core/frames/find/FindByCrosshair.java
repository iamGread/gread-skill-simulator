package gmail.ksh9345.gss.core.frames.find;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.world.extent.EntityUniverse;

import java.util.Set;

/**
 * Created by ksh93 on 2017-01-18.
 */
public class FindByCrosshair implements BaseFrame{
    public double MaxDistance;

    public FindByCrosshair(double maxDistance) {
        MaxDistance = maxDistance;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Set<EntityUniverse.EntityHit> seh = st.caster.getWorld().getIntersectingEntities(st.caster, MaxDistance);
        for (EntityUniverse.EntityHit eh:seh){
            Entity e = eh.getEntity();
            if (!e.equals(st.caster)){
                st.targets.add(e);
            }
        }
    }
}
