package gmail.ksh9345.gss.core.frames.find;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.sponge.PEP;
import org.spongepowered.api.Server;
import org.spongepowered.api.entity.living.player.Player;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class FindByName implements BaseFrame{
    public enum Mode{
        EQUARE,
        REGEX,
    }
    public Mode mode;
    public String name;
    public Pattern pt = null;
    public FindByName(Mode mode, String name) {
        this.mode = mode;
        this.name = name;
        if(mode == Mode.REGEX){
            this.pt = Pattern.compile(name);
        }
    }

    public FindByName(String name) {
        this.mode = Mode.EQUARE;
        this.name = name;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        switch (mode){
            default:
            case EQUARE:
                Optional<Player> opt_p = PEP.server.getPlayer(name);
                opt_p.ifPresent(player -> st.targets.add(player));
                break;
            case REGEX:
                for (Player player : PEP.server.getOnlinePlayers()) {
                    if(pt.matcher(player.getName()).find()){
                        st.targets.add(player);
                    }
                }
                break;
        }

    }
}
