package gmail.ksh9345.gss.core.frames.find;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.system.CoordinateSystem;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.util.AABB;

import java.util.Set;

/**
 * Created by ksh93 on 2017-01-18.
 */
public class FindByLocation implements BaseFrame {


    public enum Mode {
        CASTER,
        SKILL,
        FIRST_TRAGET
    }

    public CoordinateSystem lr;
    public Mode fr;
    public Vector3d arg;
    //
    public Vector3d radsize;

    public FindByLocation(Mode fr, CoordinateSystem lr, Vector3d arg, Vector3d radsize) {
        this.lr = lr;
        this.fr = fr;
        this.radsize = radsize;
        this.arg = arg;
    }

    //

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Vector3d findpos;
        switch (fr){
            default:
            case CASTER:
                findpos = this.lr.transform(st.caster.getLocation().getPosition(), st.caster.getHeadRotation(), this.arg);
                break;
            case FIRST_TRAGET:
                if (st.targets.size() < 1){
                    return;
                }
                Entity temp = st.targets.get(0);
                if (temp instanceof Living){
                    findpos = this.lr.transform(temp.getLocation().getPosition(), ((Living) temp).getHeadRotation(), this.arg);
                }else {
                    findpos = this.lr.transform(temp.getLocation().getPosition(), temp.getRotation(), this.arg);
                }
                break;
            case SKILL:
                findpos = this.lr.transform(st.location.getPosition(), st.rotation, this.arg);
                break;
        }
        findpos = this.lr.transform(findpos, st.rotation, this.arg);
        Set<Entity> se;
        se = st.world.getIntersectingEntities(new AABB(findpos.sub(radsize), findpos.add(radsize)));
        for(Entity e: se){
            st.targets.add(e);
        }
    }
}

