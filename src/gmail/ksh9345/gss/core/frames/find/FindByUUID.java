package gmail.ksh9345.gss.core.frames.find;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.sponge.PEP;

import java.util.UUID;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class FindByUUID implements BaseFrame{
    public String uuid;
    public UUID uuidObj;

    public FindByUUID(String uuid) {
        this.uuid = uuid;
        this.uuidObj = UUID.fromString(uuid);
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        if (uuidObj == null){
            return;
        }
        PEP.server.getPlayer(uuidObj).ifPresent(player -> st.targets.add(player));
    }
}
