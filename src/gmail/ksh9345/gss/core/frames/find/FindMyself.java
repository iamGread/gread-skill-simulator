package gmail.ksh9345.gss.core.frames.find;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;

/**
 * Created by ksh93 on 2017-01-18.
 */
public class FindMyself implements BaseFrame {
    public FindMyself() {
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        st.targets.add(st.caster);
    }
}
