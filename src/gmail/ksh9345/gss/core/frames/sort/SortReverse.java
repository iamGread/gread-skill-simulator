package gmail.ksh9345.gss.core.frames.sort;

import com.google.common.collect.Lists;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;

/**
 * Created by ksh93 on 2017-01-19.
 */
public class SortReverse implements BaseFrame {
    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        st.targets = Lists.reverse(st.targets);
    }
}
