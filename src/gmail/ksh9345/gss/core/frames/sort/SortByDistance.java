package gmail.ksh9345.gss.core.frames.sort;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.Entity;

import java.util.Comparator;

/**
 * Created by ksh93 on 2017-01-19.
 */
public class SortByDistance implements BaseFrame {
    public enum Mode {
        POSITION,
        CASTER,
        FIRSTTARGET,
    }

    public class ComparatorVector3d implements Comparator<Entity> {
        private Vector3d comppivot;

        public ComparatorVector3d(Vector3d comppivot) {
            this.comppivot = comppivot;
        }

        @Override
        public int compare(Entity o1, Entity o2) {
            double temp1 = o1.getLocation().getPosition().distance(comppivot);
            double temp2 = o2.getLocation().getPosition().distance(comppivot);
            if (temp1 > temp2) {
                return 1;
            } else if (temp1 < temp2) {
                return -1;
            }
            return 0;
        }
    }

    public Mode pivot;

    public SortByDistance(Mode pivot) {
        this.pivot = pivot;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Vector3d temp;
        double x = 0, y = 0, z = 0;
        //==============================================================================//
        switch (pivot) {
            default:
            case POSITION:
                temp = st.location.getPosition();
                break;
            case CASTER:
                temp = st.caster.getLocation().getPosition();
                break;
            case FIRSTTARGET:
                try {
                    temp = st.targets.get(0).getLocation().getPosition();
                } catch (Exception e) {
                    return;
                }
                break;
        }
        // ============================================================================== //
        st.targets.sort(new ComparatorVector3d(temp));
    }
}
