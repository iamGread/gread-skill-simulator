package gmail.ksh9345.gss.core.frames.sort;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.text.Text;

import java.util.Comparator;
import java.util.Optional;

/**
 * Created by ksh93 on 2017-01-19.
 */
public class SortByGameElements implements BaseFrame {
    public enum GameElements{
        HP(new Comparator<Entity>() {
            @Override
            public int compare(Entity o1, Entity o2) {
                Optional<Double> o1h = o1.get(Keys.HEALTH);
                Optional<Double> o2h = o2.get(Keys.HEALTH);
                double o1hd;
                double o2hd;
                if(o1h.isPresent()){
                    o1hd = o1h.get();
                }else {
                    o1hd = 0;
                }
                if(o2h.isPresent()){
                    o2hd = o2h.get();
                }else {
                    o2hd = 0;
                }
                //
                if(o1hd > o2hd){
                    return 1;
                }else if(o1hd < o2hd){
                    return -1;
                }else {
                    return 0;
                }
            }
        }),
        MAXHP(new Comparator<Entity>() {
            @Override
            public int compare(Entity o1, Entity o2) {
                Optional<Double> o1h = o1.get(Keys.MAX_HEALTH);
                Optional<Double> o2h = o2.get(Keys.MAX_HEALTH);
                double o1hd;
                double o2hd;
                if(o1h.isPresent()){
                    o1hd = o1h.get();
                }else {
                    o1hd = 0;
                }
                if(o2h.isPresent()){
                    o2hd = o2h.get();
                }else {
                    o2hd = 0;
                }
                //
                if(o1hd > o2hd){
                    return 1;
                }else if(o1hd < o2hd){
                    return -1;
                }else {
                    return 0;
                }
            }
        }),
        EXPERIENCE(new Comparator<Entity>() {
            @Override
            public int compare(Entity o1, Entity o2) {
                Optional<Integer> o1h = o1.get(Keys.TOTAL_EXPERIENCE);
                Optional<Integer> o2h = o2.get(Keys.TOTAL_EXPERIENCE);
                int o1hi;
                int o2hi;
                if(o1h.isPresent()){
                    o1hi = o1h.get();
                }else {
                    o1hi = 0;
                }
                if(o2h.isPresent()){
                    o2hi = o2h.get();
                }else {
                    o2hi = 0;
                }
                //
                if(o1hi > o2hi){
                    return 1;
                }else if(o1hi < o2hi){
                    return -1;
                }else {
                    return 0;
                }
            }
        }),
        DISPLAYNAME(new Comparator<Entity>() {
            @Override
            public int compare(Entity o1, Entity o2) {
                Optional<Text> o1h = o1.get(Keys.DISPLAY_NAME);
                Optional<Text> o2h = o2.get(Keys.DISPLAY_NAME);
                String o1hs;
                String o2hs;
                if(o1h.isPresent()){
                    o1hs = o1h.get().toPlain();
                }else {
                    o1hs = "";
                }
                if(o2h.isPresent()){
                    o2hs = o2h.get().toPlain();
                }else {
                    o2hs = "";
                }
                return o1hs.compareTo(o2hs);
            }
        });
        //==================================//
        public Comparator<Entity> comparator;
        GameElements(Comparator<Entity> comparator) {
            this.comparator = comparator;
        }
    }
    //
    public GameElements ge;

    public SortByGameElements(GameElements ge) {
        this.ge = ge;
    }

    //

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        GameElements temp = this.ge;
        st.targets.sort(temp.comparator);
    }
}
