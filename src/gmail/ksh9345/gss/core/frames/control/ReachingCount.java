package gmail.ksh9345.gss.core.frames.control;

import gmail.ksh9345.gss.core.Errors.ControlMapError;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.skill.control.ControlData;

/**
 * Created by ksh93 on 2017-02-06.
 */
public class ReachingCount implements BaseFrame {
    private class ReachingCountControlData implements ControlData{
        int remain;

        public ReachingCountControlData(int remain) {
            this.remain = remain;
        }
    }
    public int to;
    public int count;

    public ReachingCount(int to, int count) {
        this.to = to;
        this.count = count;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        int cur = st.getCurrentFrame();
        ControlData cd = st.controlmap.get(cur);
        if (cd == null){
            st.controlmap.put(cur, new ReachingCountControlData(count));
        }else if (cd instanceof ReachingCountControlData){
            ReachingCountControlData rccd = (ReachingCountControlData) cd;
            rccd.remain -= 1;
            if (rccd.remain <= 0){
                st.jump(to);
                st.controlmap.remove(cur);
            }
        }else {
            st.setError(new ControlMapError("ReachingCountControlData", cd, cur));
        }
    }
}
