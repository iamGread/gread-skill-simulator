package gmail.ksh9345.gss.core.frames.control;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;

import java.util.Date;

/**
 * Created by ksh93 on 2017-02-01.
 */
public class Sleep implements BaseFrame {
    public long millis;

    public Sleep(long millis) {
        this.millis = millis;
    }


    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        st.tickExecution = false;
        st.pauseReservation = new Date(new Date().getTime() + millis);
    }
}
