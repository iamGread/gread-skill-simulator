package gmail.ksh9345.gss.core.frames.control;

import gmail.ksh9345.gss.core.Errors.ControlMapError;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.skill.control.ControlData;

/**
 * Created by ksh93 on 2017-02-06.
 */
public class ReachingAfterTime implements BaseFrame {
    private class ReachingAfterTimeControlData implements ControlData{
        long expire;

        public ReachingAfterTimeControlData(long expire) {
            this.expire = expire;
        }
    }
    public int to;
    public int millis;

    public ReachingAfterTime(int to, int millis) {
        this.to = to;
        this.millis = millis;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        int cur = st.getCurrentFrame();
        ControlData cd = st.controlmap.get(cur);
        if (cd == null){
            st.controlmap.put(cur, new ReachingAfterTimeControlData(st.currentTime + millis));
        }else if (cd instanceof ReachingAfterTimeControlData){
            ReachingAfterTimeControlData ratcd = (ReachingAfterTimeControlData) cd;
            if (st.currentTime > ratcd.expire){
                st.jump(to);
                st.controlmap.remove(cur);
            }
        }else {
            st.setError(new ControlMapError("ReachingAfterTimeControlData", cd, cur));
        }
    }
}
