package gmail.ksh9345.gss.core.frames.control;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;

/**
 * Created by ksh93 on 2017-02-04.
 */
public class IfTargetSize implements BaseFrame{
    public enum Mode{
        ABOVE,
        BELOW,
        ABOVE_EQUARE,
        BELOW_EQUARE,
        EQUARE,
        NOT_EQUARE
    }

    public IfTargetSize(Mode mode, int arg, int jmp) {
        this.mode = mode;
        this.arg = arg;
        this.jmp = jmp;
    }

    public Mode mode;
    public int arg;
    public int jmp;

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        switch (this.mode){
            case ABOVE:
                if (st.targets.size() > arg){
                    st.jump(jmp);
                }
                break;
            case BELOW:
                if (st.targets.size() < arg){
                    st.jump(jmp);
                }
                break;
            case ABOVE_EQUARE:
                if (st.targets.size() >= arg){
                    st.jump(jmp);
                }
                break;
            case BELOW_EQUARE:
                if (st.targets.size() <= arg){
                    st.jump(jmp);
                }
                break;
            case EQUARE:
                if (st.targets.size() == arg){
                    st.jump(jmp);
                }
                break;
            case NOT_EQUARE:
                if (st.targets.size() != arg){
                    st.jump(jmp);
                }
                break;
        }
    }

}
