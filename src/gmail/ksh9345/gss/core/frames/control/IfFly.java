package gmail.ksh9345.gss.core.frames.control;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.frames.act.ActBlink;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;

/**
 * Created by ksh93 on 2017-02-13.
 */
public class IfFly implements BaseFrame{
    public enum Mode{
        CASTER,
        FIRST_TARGET
    }
    Mode md;
    int to;
    public IfFly(Mode md, int to) {
        this.md = md;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        switch (md){
            case CASTER:
                if (st.caster.get(Keys.IS_FLYING).orElse(false)){
                    st.jump(to);
                }
                break;
            case FIRST_TARGET:
                Entity e = st.targets.get(0);
                if (e != null && e.get(Keys.IS_FLYING).orElse(false)){
                    st.jump(to);
                }
                break;
        }
    }
}
