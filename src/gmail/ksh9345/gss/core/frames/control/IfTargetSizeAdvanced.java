package gmail.ksh9345.gss.core.frames.control;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;

import java.util.Arrays;

/**
 * Created by ksh93 on 2017-02-06.
 */
public class IfTargetSizeAdvanced implements BaseFrame {
    public enum Mode {
        BETWEEN,
        MATCH,
        UNMATCH,
    }

    public IfTargetSizeAdvanced(Mode mode, int jmp, int[] args) {
        this.mode = mode;
        this.args = args;
        this.jmp = jmp;
        switch (mode) {
            case BETWEEN:
                int max = 0;
                int min = 0;
                for (int arg : args) {
                    if (arg < min) {
                        min = arg;
                    }
                    if (arg > max) {
                        max = arg;
                    }
                }
                this.args = new int[]{min, max};
                break;
        }
    }

    public Mode mode;
    public int[] args;
    public int jmp;

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        if (args.length < 1){
            return;
        }
        switch (mode) {
            case BETWEEN:
                if (args[0] <= st.targets.size() && st.targets.size() < args[1]) {
                    st.jump(jmp);
                }
                break;
            case MATCH:
                if (Arrays.binarySearch(args, st.targets.size()) >= 0) {
                    st.jump(jmp);
                }
            case UNMATCH:
                if (Arrays.binarySearch(args, st.targets.size()) < 0) {
                    st.jump(jmp);
                }
        }
    }
}

