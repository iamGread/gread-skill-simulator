package gmail.ksh9345.gss.core.frames.control;

import com.google.common.collect.Lists;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;

import java.util.List;

/**
 * Created by ksh93 on 2017-02-07.
 */
public class IfSkillPositionBlockType implements BaseFrame {
    public enum Macro{
        UNPENETRATABLE,
        PENETRATABLE,
    }
    public enum Mode{
        MATCH,
        UNMATCH
    }

    public Mode md;
    public List<BlockType> bts;
    public int to;
    public IfSkillPositionBlockType(Macro macro, int to) {
        switch (macro){
            default:
            case PENETRATABLE:
                this.md = Mode.MATCH;
                this.bts = Lists.newArrayList(
                        BlockTypes.AIR,
                        BlockTypes.WATER,
                        BlockTypes.LAVA,
                        BlockTypes.FLOWING_LAVA,
                        BlockTypes.FLOWING_WATER,
                        BlockTypes.TALLGRASS,
                        BlockTypes.GRASS
                );
                this.to = to;
                break;
            case UNPENETRATABLE:
                this.md = Mode.UNMATCH;
                this.bts = Lists.newArrayList(
                        BlockTypes.AIR,
                        BlockTypes.WATER,
                        BlockTypes.LAVA,
                        BlockTypes.FLOWING_LAVA,
                        BlockTypes.FLOWING_WATER,
                        BlockTypes.TALLGRASS,
                        BlockTypes.GRASS
                );
                this.to = to;
                break;
        }
    }
    public IfSkillPositionBlockType(Mode md, int to, BlockType[] bts) {
        this.md = md;
        this.bts = Lists.newArrayList(bts);
        this.to = to;
    }
    public IfSkillPositionBlockType(Mode md, int to, BlockType blockType) {
        this.md = md;
        this.bts = Lists.newArrayList();
        this.bts.add(blockType);
        this.to = to;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        switch (md){
            default:
            case MATCH:
                if (bts.contains(st.location.getBlockType())){
                    st.jump(to);
                }
                break;
            case UNMATCH:
                if (!bts.contains(st.location.getBlockType())){
                    st.jump(to);
                }
                break;
        }
    }
}
