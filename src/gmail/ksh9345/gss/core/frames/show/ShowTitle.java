package gmail.ksh9345.gss.core.frames.show;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.title.Title;

/**
 * Created by ksh93 on 2017-01-21.
 */
public class ShowTitle implements BaseFrame {
    public Title title;

    public ShowTitle(Text titleText, Text subtitleText, int fadein, int stay, int fadeout) {
        title = Title.builder()
                .title(titleText)
                .subtitle(subtitleText)
                .fadeIn(fadein)
                .stay(stay)
                .fadeOut(fadeout)
                .build();
    }
    public ShowTitle(Text titleText, int fadein, int stay, int fadeout) {
        title = Title.builder()
                .title(titleText)
                .fadeIn(fadein)
                .stay(stay)
                .fadeOut(fadeout)
                .build();
    }

    public ShowTitle(Text titleText, int stay) {
        title = Title.builder()
                .title(titleText)
                .stay(stay)
                .build();
    }

    public ShowTitle(Text titleText) {
        title = Title.builder()
                .title(titleText)
                .build();
    }
    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        for (Entity e: st.targets){
            if (e instanceof Player){
                ((Player) e).sendTitle(title);
            }
        }
    }
}
