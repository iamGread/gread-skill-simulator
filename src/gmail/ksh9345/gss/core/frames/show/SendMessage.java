package gmail.ksh9345.gss.core.frames.show;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.channel.ChatTypeMessageReceiver;
import org.spongepowered.api.text.chat.ChatType;
import org.spongepowered.api.text.chat.ChatTypes;

/**
 * Created by ksh93 on 2017-01-21.
 */
public class SendMessage implements BaseFrame {
    public Text msg;
    public ChatType chattype;

    public SendMessage(Text msg) {
        this.msg = msg;
        this.chattype = ChatTypes.CHAT;
    }
    public SendMessage(Text msg, int mode) {
        this.msg = msg;
        switch (mode){
            default:
            case 0:
                this.chattype = ChatTypes.CHAT;
                break;
            case 1:
                this.chattype = ChatTypes.ACTION_BAR;
                break;
            case 2:
                this.chattype = ChatTypes.SYSTEM;
                break;
        }
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        for (Entity e: st.targets){
            if (e instanceof ChatTypeMessageReceiver){
                ((ChatTypeMessageReceiver) e).sendMessage(chattype, msg);
            }
        }
    }
}
