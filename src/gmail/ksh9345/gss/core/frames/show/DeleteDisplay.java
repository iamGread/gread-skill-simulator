package gmail.ksh9345.gss.core.frames.show;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.Entity;

/**
 * Created by ksh93 on 2017-02-09.
 */
public class DeleteDisplay implements BaseFrame{
    int code;

    public DeleteDisplay(int code) {
        this.code = code;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Entity e = st.belongEntitys.get(code);
        if(e != null){
            e.remove();
            st.belongEntitys.remove(e);
        }
    }
}
