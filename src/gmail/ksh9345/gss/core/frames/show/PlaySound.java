package gmail.ksh9345.gss.core.frames.show;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.effect.sound.SoundType;
import org.spongepowered.api.entity.Entity;

/**
 * Created by ksh93 on 2017-01-21.
 */
public class PlaySound implements BaseFrame {
    public enum PlayType{
        SKILL,
        CASTER,
        TARGETS,
    }
    PlayType pt;
    SoundType soundType;
    double volume;
    double pitch;

    public PlaySound(PlayType pt, SoundType st, double volume, double pitch) {
        this.pt = pt;
        this.soundType = st;
        this.volume = volume;
        this.pitch = pitch;
    }
    public PlaySound(PlayType pt, SoundType st, double volume) {
        this.pt = pt;
        this.soundType = st;
        this.volume = volume;
        this.pitch = 1.0;
    }
    public PlaySound(SoundType st, double volume, double fitch) {
        this.pt = PlayType.SKILL;
        this.soundType = st;
        this.volume = volume;
        this.pitch = fitch;
    }
    public PlaySound(SoundType st, double volume) {
        this.pt = PlayType.SKILL;
        this.soundType = st;
        this.volume = volume;
        this.pitch = 1.0;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        switch (pt){
            default:
            case SKILL:
                st.world.playSound(soundType, st.location.getPosition(), volume, pitch);
                break;
            case CASTER:
                st.world.playSound(soundType, st.caster.getLocation().getPosition(), volume, pitch);
                break;
            case TARGETS:
                for(Entity e:st.targets){
                    st.world.playSound(soundType, e.getLocation().getPosition(), volume, pitch);
                }
                break;
        }
    }
}
