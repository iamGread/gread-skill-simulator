package gmail.ksh9345.gss.core.frames.show;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.system.ArmorStandSystem;
import gmail.ksh9345.gss.core.system.CoordinateSystem;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.ArmorStand;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * Created by ksh93 on 2017-01-21.
 */
public class PlaceDisplay implements BaseFrame{
    public enum Mode{
        SKILL,
        CASTER,
        FIRST_TARGET,
    }
    public Mode md;
    public ItemType it;
    public int aCode;
    public CoordinateSystem cs;
    public Vector3d argpos;
    public Vector3d argrot;

    public PlaceDisplay(ItemType it, int aCode, CoordinateSystem cs, Vector3d argpos) {
        this.md = Mode.SKILL;
        this.it = it;
        this.aCode = aCode;
        this.cs = cs;
        this.argpos = argpos;
        this.argrot = null;
    }
    public PlaceDisplay(ItemType it, int aCode, CoordinateSystem cs, Vector3d argpos, Vector3d argrot) {
        this.md = Mode.SKILL;
        this.it = it;
        this.aCode = aCode;
        this.cs = cs;
        this.argpos = argpos;
        this.argrot = argrot;
    }
    public PlaceDisplay(Mode md, ItemType it, int aCode, CoordinateSystem cs, Vector3d argpos) {
        this.md = md;
        this.it = it;
        this.aCode = aCode;
        this.cs = cs;
        this.argpos = argpos;
        this.argrot = null;
    }
    public PlaceDisplay(Mode md, ItemType it, int aCode, CoordinateSystem cs, Vector3d argpos, Vector3d argrot) {
        this.md = md;
        this.it = it;
        this.aCode = aCode;
        this.cs = cs;
        this.argpos = argpos;
        this.argrot = argrot;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Entity codeTarget = st.belongEntitys.get(aCode);

        if (codeTarget != null){
            return;
        }
        //
        Location<World> temp;

        Vector3d rot;
        if (argrot != null){
            rot = argrot;
        }else {
            rot = Vector3d.ZERO;
        }
        switch (md){
            default:
            case SKILL:
                rot = st.rotation.add(rot);
                temp = st.world.getLocation(
                        cs.transform(st.location.getPosition(), rot, argpos)
                );
                break;
            case CASTER:
                rot = st.caster.getHeadRotation().add(rot);
                temp = st.world.getLocation(
                        cs.transform(st.caster.getLocation().getPosition(), rot, argpos)
                );
                break;
            case FIRST_TARGET:
                if (st.targets.size() < 1){
                    return;
                }
                Entity entity = st.targets.get(0);
                if (entity instanceof Living){
                    rot = ((Living) entity).getHeadRotation().add(rot);
                    temp = st.world.getLocation(
                            cs.transform(entity.getLocation().getPosition(), rot, argpos)
                    );
                }else {
                    rot = entity.getRotation().add(rot);
                    temp = st.world.getLocation(
                            cs.transform(entity.getLocation().getPosition(), rot, argpos)
                    );
                }
                break;
        }
        //==================

        codeTarget = temp.getExtent().createEntity(EntityTypes.ARMOR_STAND, ArmorStandSystem.armorStandPosition(temp.getPosition(), rot.getY(), rot.getX()));
        st.belongEntitys.put(aCode, codeTarget);
        //===================
        ArmorStand as = ((ArmorStand) codeTarget);

        final Vector3d finalRot = new Vector3d(rot.getX(), 0,0);
        final Vector3d bodyrot = new Vector3d(0, rot.getY(), 0);
        final World w = st.world;
        final Living c = st.caster;
        smt.synchronizeRequest(
                () -> {
                    as.setHelmet(ItemStack.of(it, 1));
                    as.offer(Keys.WALKING_SPEED, new Double(50));
                    as.offer(Keys.FLYING_SPEED, new Double(50));

                    //
                    as.offer(Keys.HAS_GRAVITY, false);
                    as.offer(Keys.INVISIBLE, true);
                    as.setRotation(bodyrot);
                    as.offer(Keys.HEAD_ROTATION, finalRot);
                    w.spawnEntity(as, Cause.source(c).build());
                }
        );
    }
}
