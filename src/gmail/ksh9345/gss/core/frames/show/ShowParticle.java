package gmail.ksh9345.gss.core.frames.show;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.system.CoordinateSystem;
import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleType;
import org.spongepowered.api.entity.Entity;

/**
 * Created by ksh93 on 2017-04-17.
 */
public class ShowParticle implements BaseFrame {
    public enum Mode {
        SKILL_POSITION,
        CASTER_POSITION,
        TARGETS_POSITION,

    }

    public Mode dt;
    public ParticleType pt;
    public CoordinateSystem lr;
    public Vector3d pos;
    public Vector3d velocity;
    public int quantity;

    public ShowParticle(Mode dt, ParticleType pt) {
        this.dt = dt;
        this.pt = pt;
        this.lr = CoordinateSystem.RELATIVE_CORDINATE;
        this.pos = Vector3d.ZERO;
        this.velocity = Vector3d.ZERO;
        this.quantity = 1;
    }
    public ShowParticle(Mode dt, ParticleType pt, int quantity) {
        this.dt = dt;
        this.pt = pt;
        this.lr = CoordinateSystem.RELATIVE_CORDINATE;
        this.pos = Vector3d.ZERO;
        this.velocity = Vector3d.ZERO;
        this.quantity = quantity;
    }
    public ShowParticle(Mode dt, ParticleType pt, CoordinateSystem lr, Vector3d pos, int quantity) {
        this.dt = dt;
        this.pt = pt;
        this.lr = lr;
        this.pos = pos;
        this.velocity = Vector3d.ZERO;
        this.quantity = quantity;
    }
    public ShowParticle(Mode dt, ParticleType pt, CoordinateSystem lr, Vector3d pos, Vector3d velocity, int quantity) {
        this.dt = dt;
        this.pt = pt;
        this.lr = lr;
        this.pos = pos;
        this.velocity = velocity;
        this.quantity = quantity;

    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Vector3d temp;
        switch (this.dt) {
            default:
            case SKILL_POSITION:
                temp = lr.transform(st.location.getPosition(), st.rotation, pos);
                st.caster.getWorld().spawnParticles(
                        ParticleEffect.builder()
                                .quantity(quantity)
                                .type(pt)
                                .velocity(velocity)
                                .build()
                        //
                        , temp);
                break;
            case CASTER_POSITION:
                temp = lr.transform(st.caster.getLocation().getPosition(), st.caster.getHeadRotation(), pos);
                st.caster.getWorld().spawnParticles(ParticleEffect.builder()
                                .quantity(quantity)
                                .type(pt)
                                .velocity(velocity)
                                .build()
                        //
                        , temp);
                break;
            case TARGETS_POSITION:
                for (Entity entity: st.targets){

                    temp = lr.transform(entity.getLocation().getPosition(), entity.getRotation(), pos);
                    entity.getWorld().spawnParticles(
                            ParticleEffect.builder()
                                    .quantity(quantity)
                                    .type(pt)
                                    .velocity(velocity)
                                    .build()
                            //
                            , temp);
                }
                break;
        }
    }
}
