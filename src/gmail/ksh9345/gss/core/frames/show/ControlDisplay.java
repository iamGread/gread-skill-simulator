package gmail.ksh9345.gss.core.frames.show;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.system.ArmorStandSystem;
import gmail.ksh9345.gss.core.system.CoordinateSystem;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.ArmorStand;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.util.rotation.Rotation;
import org.spongepowered.api.util.rotation.Rotations;

/**
 * Created by ksh93 on 2017-02-09.
 */
public class ControlDisplay implements BaseFrame{
    public enum Mode{
        SKILL,
        CASTER,
        FIRST_TARGET,
    }

    Mode md;
    int code;
    CoordinateSystem cs;
    Vector3d arg;
    Vector3d argrot;
    boolean istransfer = true;

    public ControlDisplay(int code, CoordinateSystem cs, Vector3d arg, Vector3d argrot) {
        this.md = Mode.SKILL;
        this.code = code;
        this.cs = cs;
        this.arg = arg;
        this.argrot = argrot;
    }
    public ControlDisplay(int code, CoordinateSystem cs, Vector3d arg) {
        this.md = Mode.SKILL;
        this.code = code;
        this.cs = cs;
        this.arg = arg;
        this.argrot = Vector3d.ZERO;
    }
    public ControlDisplay(int code, Vector3d argrot) {
        this.md = Mode.SKILL;
        this.code = code;
        this.cs = CoordinateSystem.RELATIVE_CORDINATE;
        this.arg = Vector3d.ZERO;
        this.argrot = argrot;
    }
    public ControlDisplay(int code) {
        this.md = Mode.SKILL;
        this.code = code;
        this.cs = CoordinateSystem.RELATIVE_CORDINATE;
        this.arg = Vector3d.ZERO;
        this.argrot = Vector3d.ZERO;
    }
    public ControlDisplay(Mode md, int code, CoordinateSystem cs, Vector3d arg, Vector3d argrot, boolean istransfer) {
        this.md = md;
        this.code = code;
        this.cs = cs;
        this.arg = arg;
        this.argrot = argrot;
        this.istransfer = istransfer;
    }
    public ControlDisplay(Mode md, int code, CoordinateSystem cs, Vector3d arg, Vector3d argrot) {
        this.md = md;
        this.code = code;
        this.cs = cs;
        this.arg = arg;
        this.argrot = argrot;
    }
    public ControlDisplay(Mode md, int code, CoordinateSystem cs, Vector3d arg) {
        this.md = md;
        this.code = code;
        this.cs = cs;
        this.arg = arg;
        this.argrot = Vector3d.ZERO;
    }
    public ControlDisplay(Mode md, int code, Vector3d argrot) {
        this.md = md;
        this.code = code;
        this.cs = CoordinateSystem.RELATIVE_CORDINATE;
        this.arg = Vector3d.ZERO;
        this.argrot = argrot;
    }
    public ControlDisplay(Mode md, int code) {
        this.md = md;
        this.code = code;
        this.cs = CoordinateSystem.RELATIVE_CORDINATE;
        this.arg = Vector3d.ZERO;
        this.argrot = Vector3d.ZERO;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Entity e = st.belongEntitys.get(code);
        if (!(e instanceof ArmorStand)){
            return;
        }
        ArmorStand as = (ArmorStand) e;

        final Vector3d pos;
        final Vector3d temp_rot;
        final Vector3d temp_body;
        switch (md){
            default:
            case SKILL:
                pos = cs.transform(st.location.getPosition(), st.rotation, arg);
                temp_rot = new Vector3d(
                        st.rotation.getX()  + arg.getX(),
                        0,
                        0
                );
                temp_body = new Vector3d(
                        0,
                        st.rotation.getY() + arg.getY(),
                        0
                );
                break;
            case CASTER:

                pos = cs.transform(st.caster.getLocation().getPosition(), st.caster.getHeadRotation(), arg);
                Vector3d ctemp = st.caster.getHeadRotation();
                temp_rot = new Vector3d(
                        ctemp.getX() + arg.getX(),
                        0,
                        0
                );
                temp_body = new Vector3d(
                        0,
                        ctemp.getY() + arg.getY(),
                        0
                );
                break;
            case FIRST_TARGET:
                if (st.targets.size() < 1){
                    return;
                }
                Entity temp = st.targets.get(0);
                if (temp instanceof Living){
                    pos = cs.transform(temp.getLocation().getPosition(), ((Living) temp).getHeadRotation(), arg);
                    Vector3d temp2 = ((Living) temp).getHeadRotation();
                    temp_rot = new Vector3d(
                            temp2.getX() + arg.getX(),
                            0,
                            0
                    );
                    temp_body = new Vector3d(
                            0,
                            temp2.getY() + arg.getY(),
                            0
                    );
                }else {
                    pos = cs.transform(temp.getLocation().getPosition(), temp.getRotation(), arg);
                    Vector3d temp2 = temp.getRotation();
                    temp_rot = new Vector3d(
                            temp2.getX() + arg.getX(),
                            0,
                            0
                    );
                    temp_body = new Vector3d(
                            0,
                            temp2.getY() + arg.getY(),
                            0
                    );
                }
                break;
        }
        //
        smt.synchronizeRequest(() -> {
            as.setRotation(temp_body);
            as.offer(Keys.HEAD_ROTATION, temp_rot);
            if (istransfer){
                as.transferToWorld(as.getWorld(), ArmorStandSystem.armorStandPosition(pos, temp_body.getY(), temp_rot.getX()));
            }else {
                as.setLocation(as.getWorld().getLocation(ArmorStandSystem.armorStandPosition(pos, temp_body.getY(), temp_rot.getX())));
            }

        });
    }
}
