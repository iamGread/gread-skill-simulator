package gmail.ksh9345.gss.core.frames.select;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;

/**
 * Created by ksh93 on 2017-01-18.
 */
public class SelectByName implements BaseFrame {
    public enum MatchingMode{
        EXACT,
        EXCEPT,
        REGEXP,
    }
    public enum ArgsMode{
        CONST,
        VARIABLE,
        BOTH,
    }
    public MatchingMode mm;
    public ArgsMode am;
    public String[] constargs;

    public SelectByName(MatchingMode mm, ArgsMode am, String[] constargs) {
        this.mm = mm;
        this.am = am;
        this.constargs = constargs;
    }
    //

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
//        List<Entity> replacement = new ArrayList<>();
//        List<String> args;
//        switch (this.am){
//            default:
//            case BOTH:
//                args = Arrays.asList(this.constargs);
//                args.addAll(Arrays.asList(st.args));
//                break;
//            case CONST:
//                args = Arrays.asList(this.constargs);
//                break;
//            case VARIABLE:
//                args = Arrays.asList(st.args);
//                break;
//        }
//        switch (this.mm){
//            default:
//            case EXACT:
//                for (String temp : args){
//                    for (Entity et:s.targets){
//                        Optional<Text> vt = et.get(Keys.DISPLAY_NAME);
//                        if (vt.isPresent()){
//                            if(vt.get().toPlain().equals(temp)){
//                                replacement.add(et);
//                            }
//                        }
//                    }
//                }
//                break;
//            case EXCEPT:
//                for (String temp : args){
//                    for (Entity et:s.targets){
//                        Optional<Text> vt = et.get(Keys.DISPLAY_NAME);
//                        if (vt.isPresent()){
//                            if(!vt.get().toPlain().equals(temp)){
//                                replacement.add(et);
//                            }
//                        }
//                    }
//                }
//                break;
//            case REGEXP:
//                for (String temp : args){
//                    for (Entity et:s.targets){
//                        Optional<Text> vt = et.get(Keys.DISPLAY_NAME);
//                        if (vt.isPresent()){
//                            if(!vt.get().toPlain().matches(temp)){
//                                replacement.add(et);
//                            }
//                        }
//                    }
//                }
//                break;
//        }
    }
}
