package gmail.ksh9345.gss.core.frames.select;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.EntityType;

import java.util.stream.Collectors;

/**
 * Created by ksh93 on 2017-01-18.
 */
public class ExceptEntityTypes implements BaseFrame {

    public EntityType[] et;

    public ExceptEntityTypes(EntityType et) {
        this.et = new EntityType[]{et};
    }

    public ExceptEntityTypes(EntityType[] ets) {
        this.et = ets;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        st.targets = st.targets.stream().filter(entity -> {
                    for (EntityType entityType : et) {
                        if (entity.getType().equals(entityType)) {
                            return false;
                        }
                    }
                    return true;
                }
        ).collect(Collectors.toList());
    }
}
