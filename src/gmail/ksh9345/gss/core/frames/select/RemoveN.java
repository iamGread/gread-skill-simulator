package gmail.ksh9345.gss.core.frames.select;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.Entity;

import java.util.List;

/**
 * Created by ksh93 on 2017-02-01.
 */
public class RemoveN implements BaseFrame {

    public enum Mode {
        First, Last;
    }

    public Mode pt;
    public int n;
    public RemoveN(int n) {
        this.pt = Mode.First;
        this.n = n;
    }
    public RemoveN(Mode pt, int n) {
        this.pt = pt;
        this.n = n;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        List<Entity> temp = null;
        switch (pt) {
            default:
            case First:
                try {
                    temp = st.targets.subList(n, st.targets.size());
                    st.targets = temp;
                } catch (Exception e) {
                }
                break;
            case Last:
                try {
                    temp = st.targets.subList(0, st.targets.size() - n);
                    st.targets = temp;
                } catch (Exception e) {
                }
                break;
        }
    }
}
