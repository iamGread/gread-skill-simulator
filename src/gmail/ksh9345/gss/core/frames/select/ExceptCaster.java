package gmail.ksh9345.gss.core.frames.select;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;

import java.util.stream.Collectors;

/**
 * Created by ksh93 on 2017-02-04.
 */
public class ExceptCaster implements BaseFrame {
    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        st.targets = st.targets.stream().filter(entity -> !entity.equals(st.caster)).collect(Collectors.toList());
    }
}
