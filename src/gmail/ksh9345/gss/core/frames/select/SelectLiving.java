package gmail.ksh9345.gss.core.frames.select;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by ksh93 on 2017-02-06.
 */
public class SelectLiving implements BaseFrame{
    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        st.targets = st.targets.stream().filter(entity -> {
            if (entity instanceof Living) {
                if (!entity.isRemoved()){
                    Optional<Double> opthp = entity.get(Keys.HEALTH);
                    if (opthp.isPresent()){
                        return opthp.get() > 0;
                    }
                }
            }
            return false;
        }).collect(Collectors.toList());

    }
}
