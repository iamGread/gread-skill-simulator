package gmail.ksh9345.gss.core.frames.select;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.Entity;

import java.util.HashSet;

/**
 * Created by ksh93 on 2017-02-06.
 */
public class RemoveRepeated implements BaseFrame {
    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        HashSet<Entity> temp = new HashSet<Entity>();
        temp.addAll(st.targets);
        st.targets.clear();
        st.targets.addAll(temp);
    }
}
