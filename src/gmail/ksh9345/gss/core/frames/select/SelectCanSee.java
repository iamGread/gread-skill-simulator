package gmail.ksh9345.gss.core.frames.select;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;

import java.util.stream.Collectors;

/**
 * Created by ksh93 on 2017-04-17.
 */
public class SelectCanSee implements BaseFrame{
    public enum Mode{
        CASTER,
        FIRST_TARGET
    }

    public SelectCanSee(Mode mode) {
        this.mode = mode;
    }

    public Mode mode;
    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        switch (mode){
            default:
            case CASTER:
                st.targets = st.targets.stream().filter(entity -> st.caster.canSee(entity)).collect(Collectors.toList());
                break;
            case FIRST_TARGET:
                Living lv;
                if(st.targets.size() < 2){
                    st.targets.clear();
                    return;
                }
                Entity e = st.targets.get(0);
                if(e instanceof Living){
                    lv = (Living) e;
                    st.targets.remove(0);
                    st.targets = st.targets.stream().filter(lv::canSee).collect(Collectors.toList());
                }else {
                    st.targets.clear();
                }
                break;
        }
    }
}
