package gmail.ksh9345.gss.core.frames.select;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;

import java.util.Random;

/**
 * Created by ksh93 on 2017-01-18.
 */
public class SelectN implements BaseFrame {
    public enum Mode {
        FIRST,
        LAST,
        RANDOM,
    }
    public int n;
    public Mode snt;

    public SelectN(int n) {
        this.n = n;
        this.snt = Mode.FIRST;
    }

    public SelectN(Mode snt, int n) {
        this.n = n;
        this.snt = snt;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        int len = st.targets.size();
        int temp;
        switch (this.snt){
            default:
            case FIRST:
                if (this.n > len){
                    st.targets = st.targets.subList(0, len);
                }else {
                    st.targets = st.targets.subList(0, this.n);
                }
                break;
            case LAST:
                if (len - this.n < 0){
                    st.targets = st.targets.subList(0 , len);
                }else {
                    st.targets = st.targets.subList(len - this.n , len);
                }
                break;
            case RANDOM:
                Random r = new Random();
                for (int i = 0; i < len - this.n; i++) {
                    temp = r.nextInt(len - i);
                    st.targets.remove(temp);
                }
                break;
        }
    }
}
