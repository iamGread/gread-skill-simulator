package gmail.ksh9345.gss.core.frames.act;

import gmail.ksh9345.gss.core.Errors.SkillNotFoundError;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.loader.Container;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Model;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.skill.SubSkill;

/**
 * Created by ksh93 on 2017-02-06.
 */
public class CastSkill implements BaseFrame{
    public String skillname;
    public boolean async;

    public CastSkill(String skillname, boolean async) {
        this.skillname = skillname.replace(" ", "_");
        this.async = async;
    }


    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Model sm = Container.Get(skillname);
        if (sm == null){
            st.setError(new SkillNotFoundError("[CallSkill] " + skillname + " is not found"));
            return;
        }
        //
        SubSkill temSubSkill = new SubSkill(sm, st.copyForSubskill(sm.length()));
        if (async){
            sk.requestAsync(temSubSkill);
        }else {
            sk.requestSync(temSubSkill);
        }
    }
}
