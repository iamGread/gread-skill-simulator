package gmail.ksh9345.gss.core.frames.act;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.system.CoordinateSystem;
import org.spongepowered.api.entity.Entity;

/**
 * Created by ksh93 on 2017-01-31.
 */
public class ActVelocity implements BaseFrame {
    CoordinateSystem lr;
    Vector3d velocity;

    public ActVelocity(CoordinateSystem lr, Vector3d velocity) {
        this.lr = lr;
        this.velocity = velocity;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        for (Entity e : st.targets){
            try {
                e.setVelocity(this.lr.transform(new Vector3d(0,0,0), e.getRotation(), this.velocity));
            } catch (Exception e1) {

            }
        }
    }
}
