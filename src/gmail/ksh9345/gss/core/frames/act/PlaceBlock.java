package gmail.ksh9345.gss.core.frames.act;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.system.CoordinateSystem;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;

/**
 * Created by ksh93 on 2017-01-20.
 */
public class PlaceBlock implements BaseFrame{
    public enum Mode{
        SKILL,
        CASTER,
        FIRST_TARGET,
    }

    //
    public Mode md;
    public int code;
    public BlockType bt;
    public CoordinateSystem cs;
    public Vector3d arg;
    //

    //new mode
    public PlaceBlock(Mode md, int code, BlockType bt, CoordinateSystem cs, Vector3d arg) {
        this.md = md;
        if (code < 0){
            code = -1;
        }
        this.code = code;
        this.bt = bt;
        this.cs = cs;
        this.arg = arg;
    }
    public PlaceBlock(Mode md, BlockType bt, CoordinateSystem cs, Vector3d arg) {
        this.md = md;
        this.code = -1;
        this.bt = bt;
        this.cs = cs;
        this.arg = arg;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Vector3d temp = null;
        switch (md){
            default:
            case SKILL:
                temp = cs.transform(st.location.getPosition(), st.rotation, arg);
                break;
            case CASTER:
                temp = (cs.transform(st.caster.getLocation().getPosition(), st.caster.getRotation(), arg));
                break;
            case FIRST_TARGET:
                if (st.targets.size() < 1){
                    return;
                }
                Entity e = st.targets.get(0);
                if (e instanceof Living){
                    temp = (cs.transform(e.getLocation().getPosition(), ((Living) e).getHeadRotation(), arg));
                }else {
                    temp = (cs.transform(e.getLocation().getPosition(), e.getRotation(), arg));
                }
                break;
        }
        //
        if (code < 0){
            st.placeBlock(temp, bt);
        }else {
            st.placeBlock(code, temp, bt);
        }
    }
}
