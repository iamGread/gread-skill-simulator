package gmail.ksh9345.gss.core.frames.act;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.mutable.Value;
import org.spongepowered.api.entity.Entity;

/**
 * Created by ksh93 on 2017-04-17.
 */
public class ChangeGameState implements BaseFrame {
    public enum GameState{
        GRAVITY,
        FLYING,
    }
    public enum Mode{
        CASTER,
        FIRST_TARGET,
        TARGETS
    }
    public GameState gameState;
    public Mode mode;
    public boolean to;

    public ChangeGameState(GameState gameState, boolean to) {
        this.gameState = gameState;
        this.mode = Mode.TARGETS;
        this.to = to;
    }
    public ChangeGameState(GameState gameState, Mode mode, boolean to) {
        this.gameState = gameState;
        this.mode = mode;
        this.to = to;
    }


    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Key<Value<Boolean>> temp;
        switch (gameState){
            default:
            case FLYING:
                temp = Keys.IS_FLYING;
                break;
            case GRAVITY:
                temp = Keys.HAS_GRAVITY;
                break;
        }
        switch (mode){
            case CASTER:
                try {
                    st.caster.offer(temp, to);
                }catch (Exception e1){ }
                break;
            case FIRST_TARGET:
                Entity e = st.targets.get(0);
                if (e != null){
                    try {
                        e.offer(temp, to);
                    }catch (Exception e1){ }
                }

                break;
            case TARGETS:
                for (Entity entity : st.targets) {
                    try {
                        entity.offer(temp, to);
                    }catch (Exception e1){ }
                }
                break;
        }
    }
}
