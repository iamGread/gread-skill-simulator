package gmail.ksh9345.gss.core.frames.act;

import com.flowpowered.math.vector.Vector3d;
import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.core.system.CoordinateSystem;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * Created by ksh93 on 2017-01-31.
 */
public class ActTeleport implements BaseFrame {
    public enum Mode {
        CASTER_TO_START_POINT,
        CASTER_TO_FIRST_TARGET,
        CASTER_TO_SKILL,
        FIRST_TARGET_TO_SECOND_TARGET,
        FIRST_TARGET_TO_SKILL,
        CASTER_WARP,
        FIRSTTARGET_WARP,
    }
    public Mode tptype;
    public CoordinateSystem lr;
    public Vector3d args;

    public ActTeleport(Mode tptype, CoordinateSystem lr, Vector3d args) {
        this.tptype = tptype;
        this.lr = lr;
        this.args = args;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Location<World> loc = null;
        Vector3d rot = null;
        Entity temp = null;
        switch (tptype){
            default:
            case CASTER_TO_FIRST_TARGET:
                if (st.targets.size() >= 1){
                    temp = st.targets.get(0);
                    if (temp instanceof Living){
                        loc = st.world.getLocation(lr.transform(temp.getLocation().getPosition(), ((Living) temp).getHeadRotation(), this.args));
                        rot = temp.getRotation();
                        st.caster.setHeadRotation(((Living) temp).getHeadRotation());
                    }else {
                        loc = st.world.getLocation(lr.transform(temp.getLocation().getPosition(), temp.getRotation(), this.args));
                        rot = temp.getRotation();
                        st.caster.setHeadRotation(temp.getRotation());
                    }
                    st.caster.setLocation(loc);
                }
                break;
            case CASTER_WARP:
                st.caster.setLocation(st.caster.getLocation().setPosition(args));
                break;
            case CASTER_TO_SKILL:
                temp = st.caster;
                rot = st.rotation;
                st.caster.setHeadRotation(rot);
                temp.setLocation(st.location);
                break;
            case FIRST_TARGET_TO_SKILL:
                if (st.targets.size() >= 1){
                    temp = st.targets.get(0);
                    rot = st.rotation;
                    if (temp instanceof Living){
                        ((Living) temp).setHeadRotation(rot);
                    }
                    temp.setLocation(st.location);
                }
                break;
            case FIRSTTARGET_WARP:
                break;
            case FIRST_TARGET_TO_SECOND_TARGET:
                if (st.targets.size() >= 2){
                    temp = st.targets.get(1);
                    loc = st.world.getLocation(lr.transform(temp.getLocation().getPosition(), temp.getRotation(), this.args));
                    rot = temp.getRotation();
                    Entity temp_teleportee = st.targets.get(0);
                    if (temp_teleportee instanceof Living){
                        ((Living) temp_teleportee).setHeadRotation(loc.getPosition().sub(st.caster.getLocation().getPosition().normalize()));
                    }
                    temp_teleportee.setLocation(loc);
                }
                break;
            case CASTER_TO_START_POINT:
                loc = st.world.getLocation(lr.transform(st.startPosition, st.startHeadRotation, args));
                rot = st.startRotation;
                st.caster.setHeadRotation(st.startHeadRotation);
                st.caster.setLocation(loc);
                break;
        }
    }
}
