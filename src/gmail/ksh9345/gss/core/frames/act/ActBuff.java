package gmail.ksh9345.gss.core.frames.act;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectType;
import org.spongepowered.api.entity.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by ksh93 on 2017-01-19.
 */
public class ActBuff implements BaseFrame {
    public PotionEffectType pet;
    public boolean ambience;
    public int amplifier;
    public int duration;
    public boolean particles;
    //
    public boolean clearbuff;
    public boolean nobuff;

    public ActBuff(PotionEffectType pet, boolean ambiance, int amplifier, int duration, boolean particles, boolean clearbuff, boolean nobuff) {

        this.pet = pet;
        this.ambience = ambiance;
        this.amplifier = amplifier;
        this.duration = duration;
        this.particles = particles;
        this.clearbuff = clearbuff;
        this.nobuff = nobuff;

    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        for (Entity e: st.targets){
            List<PotionEffect> temp1;
            Optional<List<PotionEffect>> temp2 = e.get(Keys.POTION_EFFECTS);
            //=======================================

            // clear buff 옵션
            if (clearbuff){
                temp1 = new ArrayList<>();
            }else if (temp2.isPresent()){
                temp1 = temp2.get();
            }else {
                temp1 = new ArrayList<>();
            }
            //=======================================
            // no buff 옵션
            if (!nobuff){
                temp1.add(PotionEffect.builder()
                        .ambience(ambience)
                        .amplifier(amplifier)
                        .duration(duration)
                        .particles(particles)
                        .potionType(pet)
                        .build());
            }
            //=======================================
            // 버프 적용
            smt.synchronizeRequest(() -> e.offer(Keys.POTION_EFFECTS, temp1));
        }
    }
}
