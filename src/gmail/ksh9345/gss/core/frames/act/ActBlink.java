package gmail.ksh9345.gss.core.frames.act;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.util.blockray.BlockRay;
import org.spongepowered.api.util.blockray.BlockRayHit;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;

/**
 * Created by ksh93 on 2017-02-05.
 */
public class ActBlink implements BaseFrame {
    public enum Mode{
        CASTER_TO_EYE,
        TARGET_FIRST_TO_EYE,
        CASTER_TO_SKILL,
        TARGET_FIRST_TO_SKILL,
    }
    Mode md;
    double distance;
    boolean allowAir;

    public ActBlink(Mode md, double distance, boolean allowAir) {
        this.md = md;
        this.distance = distance;
        this.allowAir = allowAir;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        Location<World> temp = null;
        BlockRay<World> br = null;
        Entity et = null;
        Optional<BlockRayHit<World>> hitOpt = null;
        //
        switch (md){
            default:
            case CASTER_TO_EYE:
                et = st.caster;
                br = BlockRay.from(et)
                        .distanceLimit(distance)
                        .skipFilter(BlockRay.continueAfterFilter(BlockRay.onlyAirFilter(), 1))
                        .build();
                //
                hitOpt = br.end();
                if (hitOpt.isPresent()) {
                    BlockRayHit<World> hit = hitOpt.get();
                    if (allowAir){
                        temp = hit.getLocation();
                    }else {
                        if (!hit.getLocation().getBlockType().equals(BlockTypes.AIR)){
                            temp = hit.getLocation();
                        }
                    }
                }
                break;
            case TARGET_FIRST_TO_EYE:
                if (st.targets.size() < 1){
                    return;
                }
                et = st.targets.get(0);
                br = BlockRay.from(et)
                        .distanceLimit(distance)
                        .skipFilter(BlockRay.continueAfterFilter(BlockRay.onlyAirFilter(), 1))
                        .build();
                //
                hitOpt = br.end();
                if (hitOpt.isPresent()) {
                    BlockRayHit<World> hit = hitOpt.get();
                    if (allowAir){
                        temp = hit.getLocation();
                    }else {
                        if (!hit.getLocation().getBlockType().equals(BlockTypes.AIR)){
                            temp = hit.getLocation();
                        }
                    }
                }
                break;
            case CASTER_TO_SKILL:
                et = st.caster;
                temp = st.location;
                break;
            case TARGET_FIRST_TO_SKILL:
                if (st.targets.size() < 1){
                    return;
                }
                et = st.targets.get(0);
                temp = st.location;
                break;
        }
        et.setLocationSafely(temp);
    }
}
