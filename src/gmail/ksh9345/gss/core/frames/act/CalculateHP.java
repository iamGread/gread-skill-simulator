package gmail.ksh9345.gss.core.frames.act;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.sponge.PEP;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.cause.entity.damage.source.DamageSources;

/**
 * Created by ksh93 on 2017-04-14.
 */
public class CalculateHP implements BaseFrame {
    public enum Mode{
        SET,
        SET_MAX,
        ADD_FROMCUR,
        SUB_FROMCUR,
        MUL_FROMCUR,
        DIV_FROMCUR,
        //
        ADD_FROMMAX,
        SUB_FROMMAX,
        MUL_FROMMAX,
        DIV_FROMMAX,
    }
    public Mode md;
    public double value;

    public CalculateHP(Mode md, double value) {
        this.md = md;
        this.value = value;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        for (Entity e: st.targets){
            smt.synchronizeRequest(
                () -> {
                    double hp = e.get(Keys.HEALTH).orElse(0d);
                    double maxhp = e.get(Keys.MAX_HEALTH).orElse(0d);
                    double tohp = 0d;
                    switch (md){
                        default:
                        case SET:
                            tohp = value;
                            break;
                        case SET_MAX:
                            tohp = maxhp;
                            break;
                            //
                        case ADD_FROMCUR:
                            tohp = hp + value;
                            break;
                        case SUB_FROMCUR:
                            tohp = hp - value;
                            break;
                        case MUL_FROMCUR:
                            tohp = hp * value;
                            break;
                        case DIV_FROMCUR:
                            tohp = hp / value;
                            break;
                            //
                        case ADD_FROMMAX:
                            tohp = maxhp + value;
                            break;
                        case SUB_FROMMAX:
                            tohp = maxhp - value;
                            break;
                        case MUL_FROMMAX:
                            tohp = maxhp * value;
                            break;
                        case DIV_FROMMAX:
                            tohp = maxhp / value;
                            break;

                    }
                    e.offer(Keys.HEALTH, tohp);
                }
            );
        }
    }
}
