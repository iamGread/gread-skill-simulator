package gmail.ksh9345.gss.core.frames.act;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;

/**
 * Created by ksh93 on 2017-01-19.
 */
public class ActDamage implements BaseFrame {
    public double damage;

    public ActDamage(double damage) {
        this.damage = damage;
    }


    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        for (Entity e: st.targets){
            smt.synchronizeRequest(() -> e.offer(Keys.HEALTH, e.get(Keys.HEALTH).get() - damage));

        }
    }
}
