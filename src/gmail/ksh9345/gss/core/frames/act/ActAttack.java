package gmail.ksh9345.gss.core.frames.act;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.sponge.PEP;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.cause.entity.damage.source.DamageSource;
import org.spongepowered.api.event.cause.entity.damage.source.DamageSources;

/**
 * Created by ksh93 on 2017-01-19.
 */
public class ActAttack implements BaseFrame {
    DamageSource ds;
    public double damage;

    public ActAttack(String damagesource, double damage) {
        switch (damagesource){
            default:
                this.ds = DamageSources.GENERIC;
            case "GENERIC":
                this.ds = DamageSources.GENERIC;
            case "DROWNING":
                this.ds = DamageSources.DROWNING;
            case "FALLING":
                this.ds = DamageSources.FALLING;
            case "FIRE_TICK":
                this.ds = DamageSources.FIRE_TICK;
            case "MAGIC":
                this.ds = DamageSources.MAGIC;
            case "MELTING":
                this.ds = DamageSources.MELTING;
            case "POISON":
                this.ds = DamageSources.POISON;
            case "STARVATION":
                this.ds = DamageSources.STARVATION;
            case "VOID":
                this.ds = DamageSources.VOID;
            case "WITHER":
                this.ds = DamageSources.WITHER;
        }
        this.damage = damage;
    }
    public ActAttack(double damage) {
        this.ds = DamageSources.GENERIC;
        this.damage = damage;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        for (Entity e: st.targets){
            final Entity temp = e;

            final Cause tempc = Cause.source(st.caster).owner(PEP.pluginContainer).build();
            smt.synchronizeRequest(
                    () -> temp.damage(damage, DamageSources.GENERIC, tempc)
            );
        }

    }
}
