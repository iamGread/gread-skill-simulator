package gmail.ksh9345.gss.core.frames.act;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import gmail.ksh9345.gss.sponge.PEP;
import org.spongepowered.api.command.CommandSource;

/**
 * Created by ksh93 on 2017-02-05.
 */
public class ActCommand implements BaseFrame{
    String command;

    public ActCommand(String command) {
        this.command = command;
    }

    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        if (st.caster instanceof CommandSource){
            PEP.commandManager.process((CommandSource) st.caster, command);
        }
    }
}
