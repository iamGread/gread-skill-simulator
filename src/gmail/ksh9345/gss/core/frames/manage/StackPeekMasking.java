package gmail.ksh9345.gss.core.frames.manage;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;
import org.spongepowered.api.entity.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ksh93 on 2017-02-06.
 */
public class StackPeekMasking implements BaseFrame{
    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        List<Entity> temp = new ArrayList<>();
        for (Entity e: st.targetStack.peek()){
            temp.add(e);
        }
        for (Entity e: st.targets){
            temp.remove(e);
        }
        st.targets = temp;
    }
}
