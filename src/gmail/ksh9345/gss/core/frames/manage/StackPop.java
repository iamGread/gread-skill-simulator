package gmail.ksh9345.gss.core.frames.manage;

import gmail.ksh9345.gss.core.frames.BaseFrame;
import gmail.ksh9345.gss.core.simulator.SimulatorListener;
import gmail.ksh9345.gss.core.simulator.SimulatorMultiTask;
import gmail.ksh9345.gss.core.simulator.SimulatorWorker;
import gmail.ksh9345.gss.core.skill.Skill;
import gmail.ksh9345.gss.core.skill.State;

/**
 * Created by ksh93 on 2017-02-01.
 */
public class StackPop implements BaseFrame {
    @Override
    public void Do(SimulatorMultiTask smt, SimulatorListener sl, SimulatorWorker sw, Skill sk, State st) {
        if (st.targetStack.size() > 0){
            st.targets = st.targetStack.pop();
        }
    }
}
