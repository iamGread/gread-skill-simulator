package gmail.ksh9345.gss.core.system;

import com.flowpowered.math.vector.Vector3d;

/**
 * Created by ksh93 on 2017-02-11.
 */
public class ArmorStandSystem {
    public static Vector3d armorStandPosition(Vector3d originpos, double theta, double phi) {
        // 목으로부터 블럭 중심까지 거리를 0.5 + 0.3
        double len = 0.275;
        theta = - (Math.PI/2 - Math.toRadians(theta));
        phi = - ( Math.PI/2 + Math.toRadians(phi));
        double y = len * Math.sin(phi);
        double xzlen = Math.abs(len * Math.cos(phi));
        double x = xzlen * Math.cos(theta);
        double z = xzlen * Math.sin(theta);

        return originpos.add(x, y - 1.4, z);
    }
}
