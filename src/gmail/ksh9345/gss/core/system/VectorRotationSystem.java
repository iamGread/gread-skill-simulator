package gmail.ksh9345.gss.core.system;

import com.flowpowered.math.vector.Vector3d;

/**
 * Created by ksh93 on 2017-02-11.
 */
public class VectorRotationSystem {
    public static Vector3d vectorToRotation(Vector3d originpos) {

        //=================================
        double xzlen = Math.sqrt(originpos.getX() * originpos.getX() + originpos.getZ() * originpos.getZ());
        double val_theta;
        double val_phi;


        if (originpos.getZ() > 0){
            val_theta = Math.atan(originpos.getX() / originpos.getZ());
        }else if (originpos.getZ() < 0){
            val_theta = Math.PI + Math.atan(originpos.getX() / originpos.getZ());
        }else {
            if (originpos.getX() > 0){
                val_theta = Math.PI/2;
            }else if (originpos.getX() < 0){
                val_theta = - Math.PI/2;
            }else {
                val_theta = 0;
            }
        }
        val_phi = Math.atan(originpos.getY()/xzlen);
        return new Vector3d(-Math.toDegrees(val_phi), Math.toDegrees(-val_theta), 0);
    }
    public static Vector3d rotationToNormalVector(Vector3d rot){
        double theta = Math.toRadians(rot.getY());
        double phi = Math.toRadians(rot.getX());
        //
        double xz = Math.cos(phi);
        //
        double x = xz * Math.sin(theta);
        double y = Math.sin(phi);
        double z = xz * Math.cos(theta);
        //
        return new Vector3d(-x, -y, z);
    }
}
