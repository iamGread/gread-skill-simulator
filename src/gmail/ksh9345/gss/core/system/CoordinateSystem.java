package gmail.ksh9345.gss.core.system;

import com.flowpowered.math.vector.Vector3d;

/**
 * Created by ksh93 on 2017-02-01.
 */
public enum CoordinateSystem {
    ABSOLUTE,
    RELATIVE_CORDINATE,
    RELATIVE_VIEWING,
    RELATIVE_VIEWING_WITHOUT_Y,
    RELATIVE_VIEWING_WITHOUT_HEIGHT
    ;


    public Vector3d transform(Vector3d origin, Vector3d rotation, Vector3d value) {
        if (value.equals(Vector3d.ZERO)){
            return origin;
        }
        switch (this){
            default:
            case ABSOLUTE:
                return value;
            case RELATIVE_CORDINATE:
                return origin.add(value);
            case RELATIVE_VIEWING:
                return origin.add(
                        VectorRotationSystem.rotationToNormalVector(
                                rotation.add(
                                        VectorRotationSystem.vectorToRotation(value)
                                )
                        ).mul(value.length()));
            case RELATIVE_VIEWING_WITHOUT_Y:
            case RELATIVE_VIEWING_WITHOUT_HEIGHT:
                return origin.add(
                        VectorRotationSystem.rotationToNormalVector(
                                new Vector3d(0, rotation.getY(), 0).add(
                                        VectorRotationSystem.vectorToRotation(value)
                                )
                        ).mul(value.length()));
        }
    }
}
